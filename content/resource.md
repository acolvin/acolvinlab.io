---
title: Links to External resources
subtitle: 
author: 
date: 2019-06-12 08:39:19.656341379 +0100 BST m=+0.037235663
toc: false
#weight: 120
tags: [resources]
layout: article
previous: ""
index: ""
next: ""
comments: false
---

The following are links to external resources that are necessary or useful.  They are split in internal resource such as my CA that is requires for my gitlab and external references that I will add to over time

## This sites source

[on gitlab.com](https://gitlab.com/acolvin/acolvin.gitlab.io)
[on my gitlab see below](https://git.acolvin.me.uk/andrew/andrew.pages.acolvin.me.uk)

## Internal Resources

Links to self hosted resources



### My Certificate Authourity 

The following link points directly to my CA.  Install this if you wish to visit my gitlab server 

[acolvinCA2.pem](http://www.acolvin.me.uk/acolvinCA2.pem)

The checksums are:

```
sha1:       746b8930ead25a4ff15dfdd9390ac43389958f97
sha256:     6c53b9f25673105f73b140784c70bfc0fcfd11189b4cdb36f5d51e121b5b0b31
sha512/256: bb55eeda03549f5e79e379dc23003b5c5083dfc3215965a1f1d8394c1dd639e7

```

### My Gitlab Server

My personal gitlab server where I keep my source and often mirror to gitlab.com repositories.  You will require my CA to access it.

[https://git.acolvin.me.uk](https://git.acolvin.me.uk/explore/projects)

it is also available through [https://gitlab.acolvin.me.uk](https://gitlab.acolvin.me.uk/explore/projects)
If I provide you with an account then the ssh is available through an ssh proxy.  To access it seamlessly add the following to your ssh config

```
Host git.acolvin.me.uk
    ProxyCommand ssh -q proxy.acolvin.me.uk nc -q0 git 22

```

If you are using windows and a tool such as putty please read how to add the proxy config and follow its instructions.  You need to proxy through the host `proxy.acolvin.me.uk` with proxy username `git`.  When you add your ssh public key to your gitlab account it will take about 10 mins for the key to propogate to the proxy server

## External Resources

[My Gitlab account](https://gitlab.com/acolvin)




