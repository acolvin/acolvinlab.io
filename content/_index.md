## What's Inside

This site is Andrew's place to publish current projects and articles and to reference different pieces of information that are of interest.  It is built using Hugo and deployed on [Gitlab Pages](http://andrew.pages.acolvin.me.uk) on his own [gitlab server](https://git.acolvin.me.uk) as well as replicated to his public [gitlab.com](https://gitlab.com/acolvin) account and [site](https://acolvin.gitlab.io/). 

You will find links to the different projects under in the projects menu and more details on me in the about link above.  Additional internal and external links can be found in the resources page.

The main projects currently underway are 

- the [Go Series]({{< ref "page/go/readme.md" >}}) which walks through all sorts of topics such as make, gitlab and not just `go`
- documenting the [ASAP]({{< ref "page/asap/asap-overview.md">}}) tool
  - just finishing the analysis section 
  - moving onto monitoring 


Read the hugo blogs (*[#hugo]({{< ref "tags/hugo" >}})*) to see how I have extended the Beautiful Hugo theme.

And now here are my blogs...
