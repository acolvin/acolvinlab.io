---
title: "Adding a Layout for Articles"
subtitle: "Current single.html Layout doesn't work for Series of Articles"
tags: ["hugo", "layout", "archetype"]
date: 2019-06-12
author: Andrew Colvin
categories: ["hugo"]
---

The current `_default/single.html` layout doesn't work wonderfully when we are presented with a series of articles that are linked such as my go series.  This is how I created an article archetype and layout to amend the current page layout to support series.

<!--more-->

## Define the Archetype
I will start by creating a new archetype to identify the requirements of the new page layout

```markdown

---
title: 
subtitle: 
author: 
date: {{ now }}
toc: true
tags: []
layout: article
previous: ""
index: ""
next: ""
---
```

This archetype defines the layout as article and defines the parameters previous, index and next to provide the author with the ability to link this article to a previous article in a series, link back to an index page and link to the next article in the series.  If these are not defined or left as `""` then the links will not be generated.  

This archetype file is created in the file located at `/archetype/article.md`
 

## Build a Partial for Previous/Index/Next

As we need a new pager I am going to create this in a partial as it could be useful in other layouts in the future.

Create a new file called `prevnext.html` in the directory `/layouts/partials/`

Now create the contents of this with 

```html
<div>
{{$i := .Params.index }}
{{$p := .Params.previous }}
{{$n := .Params.next }}
 <ul class="pager">
   {{if .Params.previous }}
     <li class="previous"><a href="{{ ref . $p}}">Previous</a></li>
   {{end}}
   {{if .Params.index }}
   <li><a href="{{ ref . $i}}">index</a></li>
   {{end}}
   {{if .Params.next }}
   <li class="next"><a href="{{ ref . $n}}">Next</a></li>
   {{end}}
 </ul>  
</div>
```

The `{{$i := ...` sets the variable to that of the ages parameter.  The reason for this is that I was getting errors using `.Params.*` within the `ref` function later.

The pager function is using the [bootstrap pager](https://www.w3schools.com/bootstrap/bootstrap_pager.asp) which hugo and the theme is based upon for its layout.  

The next part `{{if .Params.previous }}` decides whether to add this function based upon the parameter being set.  


## Add Article Layout 

The next step is to create the article layout for the page. This is a file in the following location `/layouts/page/article.html`  A file in this location will override any theme's ``page/article` layout

The article will be based on the standard `/themes/beautifulhugo/layouts/_default/single.html` so the first step is to copy this file into the location stated above

```
cp /themes/beautifulhugo/layouts/_default/single.html /layouts/page/article.html
```

The next step is to remove the standard Next/Previous functionality.  You can see it commented out below

```html
{{ define "main" }}
<div class="container">
  <div class="row">
   
    <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1">
      <article role="main" class="blog-post">
	     {{ partial "toc.html" . }}
        {{ .Content }}
      </article>
    </div>
    

    </div>
      <div class="row">
    <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
        
<!--
      <ul class="pager blog-pager">
        {{ if .PrevInSection }}
          <li class="previous">
            <a href="{{ .PrevInSection.Permalink }}" data-toggle="tooltip" data-placement="top" title="{{ .PrevInSection.Title }}">&larr; {{ i18n "previousPost" }}</a>
          </li>
        {{ end }}
        {{ if .NextInSection }}
          <li class="next">
            <a href="{{ .NextInSection.Permalink }}" data-toggle="tooltip" data-placement="top" title="{{ .NextInSection.Title }}">{{ i18n "nextPost" }} &rarr;</a>
          </li>
        {{ end }}
      </ul>
-->
      {{ if (.Params.comments) | or (and (or (not (isset .Params "comments")) (eq .Params.comments nil)) (.Site.Params.comments)) }}
        {{ if .Site.DisqusShortname }}
          <div class="disqus-comments">
            {{ template "_internal/disqus.html" . }}
          </div>
        {{ end }}
      {{ end }}

    </div>
  </div>
</div>
{{ end }}
```

To add the new partial for our Next/Previous we insert it just above the comments

```go-html-template
    <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
      {{partial "prevnext.html" . }}
<!--
      <ul class="pager blog-pager">

```

Hugo will now provide the page/article layout as part of the site.  To create a new page use:

    hugo new --kind article page/<name of page>.md

For example

```
andrew@host:~/git/andrew.pages.acolvin.me.uk> hugo new --kind article page/test-article.md
/home/andrew/git/andrew.pages.acolvin.me.uk/content/page/test-article.md created

```

This generates athe contents as 

```
---
title: 
subtitle: 
author: 
date: 2019-06-12 07:43:10.190431736 +0100 BST m=+0.044296340
toc: true
tags: []
layout: article
previous: ""
index: ""
next: ""
---

```

Ready to be edited and the article created


## Change Pages to use the Article Layout

We are now ready to change the go series articles from the standard layout to the new article layout.

First change the index page `page/go/readme.md` (I am only showing the front matter)

```
---
date: 2018-12-01
author: "Andrew Colvin"
toc: false
weight: 99
title: My Journey with Go
tags: ["go", "series", "gitlab", "docker", "mkdocs", "pages", "git"]
bigimg: [{src: "/img/DSCF1190.png"}]
layout: article
---
```

The only change is to set `layout: article` otherwise hugo would default to the default

Now the first article in the series needs to have its front matter set so that it references the index and the 2nd:

```
---
title: Go Blog Part 1 - Get Going...
author: Andrew Colvin
date: 2018-12-30
toc: true
weight: 100
tags: ["go", "series", "git"]
layout: article
#previous: "go_blog001.md"
index: "readme.md"
next: "go_blog002.md"
---

```

Now just set all of the other articles appropriately to add the front matter for the previous, next, index and layout.

## What's Next

The next step to be covered is to add a series tag and get hugo to auto create the next and previous in the series.  This will be worked through in a later post.

