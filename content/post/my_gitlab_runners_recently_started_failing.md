---
title: "Gitlab Runner Failure"
subtitle: "All of my Gitlab Runners Start to fail for every Pipeline"
author: "Andrew Colvin"
date: "2019-06-30T08:30:00"
tags: ["gitlab", "pipeline", "CI/CD"]
---

This article is a quick look into why all of my runners started to fail all on the same day and  how I fixed it.

<!--more-->

# {{% param "title" %}}
**{{< param subtitle >}}** \
by **{{< param author>}}** \
*{{< param date>}}*

Yesterday while adding content to my [Personal Hugo Site's Repository](https://git.acolvin.me.uk/andrew/andrew.pages.acolvin.me.uk) my CI/CD pipelines all started to fail whereas the day before this was not a problem.

{{< figure src="screenshot.png" >}}

What could have changed? For background my gitlab server runs on a VM that is based on openSUSE Tumbleweed which is a rolling release and is actually the Kubic transactional OS which auto updates each night. This is one avenue for investigation, but before we embark on this lets have a look at the errors

```
$ GIT_COMMIT_SHA=`git rev-parse --verify HEAD` GIT_COMMIT_SHA_SHORT=`git rev-parse --short HEAD` hugo
/bin/sh: eval: line 1: git: not found
/bin/sh: eval: line 1: git: not found
Building sites … ERROR 2019/06/29 10:38:16 Failed to read Git log: Git executable not found in $PATH

```

This is telling me that my docker image used to build this `registry.gitlab.com/pages/hugo:latest` no longer has git packaged in it when it always did previously! 

Looking at the docker images source repository [https://gitlab.com/pages/hugo](https://gitlab.com/pages/hugo) we can see that there was indeed a new build published. Looking at the commit tree there hasn't been any in master but there has in the registry branch! 

Digging deeper I find [this issue](https://gitlab.com/pages/hugo/issues/31#note_186360100) which tells me a rebuild trigger occured and now I am writing this article someone else has [posted an issue](https://gitlab.com/pages/hugo/issues/32) which wasn't there yesterday.

A quick fix for my was to add the install of git into the pages/hugo container each time by adding `apk add --no-cache git` to `.gitlab-ci.yml` in my pages project

```
test:
  script:
  - apk add --no-cache git
  - GIT_COMMIT_SHA=`git rev-parse --verify HEAD` GIT_COMMIT_SHA_SHORT=`git rev-parse --short HEAD` hugo
  except:
  - master

pages:
  script:
  - apk add --no-cache git
  - GIT_COMMIT_SHA=`git rev-parse --verify HEAD` GIT_COMMIT_SHA_SHORT=`git rev-parse --short HEAD` hugo
  artifacts:
    paths:
    - public
  only:
  - master
```

Unfortunately that isn't the only error I am seeing...

```
*** WARNING: Service runner-SRMXYYg5-project-4-concurrent-0-docker-0 probably didn't start properly.

Health check error:
ContainerStart: Error response from daemon: Cannot link to a non running container: /runner-SRMXYYg5-project-4-concurrent-0-docker-0 AS /runner-SRMXYYg5-project-4-concurrent-0-docker-0-wait-for-service/service (executor_docker.go:1321:0s)

Service container logs:
2019-06-29T17:15:33.211141951Z mount: permission denied (are you root?)
2019-06-29T17:15:33.211200994Z Could not mount /sys/kernel/security.
2019-06-29T17:15:33.211212302Z AppArmor detection and --privileged mode might break.
2019-06-29T17:15:33.212263264Z mount: permission denied (are you root?)

*********

```

This seems much more serious. Looking through the change log for Tumbleweed I see that we have inded also had an update to this with some big changes

- kernel version 5.1.7 to 5.1.10
- libvirt
- libcontainers-common

Let's have a look at my server

```
apparmor_status
...
   /usr/bin/dumb-init (10388) docker-default
   /usr/lib/gitlab-runner/gitlab-runner (10422) docker-default
   /usr/bin/dumb-init (10487) docker-default
   /usr/lib/gitlab-runner/gitlab-runner (10528) docker-default
   /usr/bin/dumb-init (10592) docker-default
   /usr/lib/gitlab-runner/gitlab-runner (10628) docker-default
   /usr/bin/dumb-init (10694) docker-default
   /usr/lib/gitlab-runner/gitlab-runner (10728) docker-default
   /usr/bin/dumb-init (10796) docker-default
   /usr/lib/gitlab-runner/gitlab-runner (10841) docker-default
   /usr/bin/dumb-init (10904) docker-default
   /usr/lib/gitlab-runner/gitlab-runner (10938) docker-default
...
```

We are clearly inside apparmor protection which probably is blocking our access to /sys/kernel/security.  

```
deny /sys/kernel/security/** rwklx,

```



One way around this is to set the runner so that it is using privileged mode. This isn't very intelligent but let's do this as all of my runners are used internally only and it is only the VM that is at risk. 

Open the runner configuration file `config/config.yaml` and set `privileged=true` in `runners.docker`

```
[[runners]]
  name = "runner 1 - go"
  url = "https://gitlab.acolvin.me.uk/"
  token = "xxxxxxxxxxxxxxxxxxxxx"
  executor = "docker"
  environment = ["GIT_SSL_NO_VERIFY=1"]
  [runners.docker]
    tls_verify = false
    image = "golang:latest"
    privileged = true
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache"]
    shm_size = 0
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
```

After restarting my runners (I did this in them all) my pipelines started working. This really is only a workaround a I need to revisit this to fix the apparmor profile for docker in docker.





