---
title: "Understanding the Statistical Behaviour of Computer Systems"
subtitle: "When is a Mean not a Mean?"
tags: ["mean", "performance", "statistics", "lognormal"]
date: 2019-06-14
original_date: 2013-07-25
categories: ["Maths"]
author: Andrew Colvin
bigimg: [{src: "post/ansible-backup-blog-1/ansible-backup-blog-1.svg"}]
katex: true
markup: mmark
---

When is a mean not a mean?  When you are trying to decide the average response time of a computer function! What will be discuss is the statistical behaviour of systems and how to determine the important measure to judge what the system performance really is.




# Understanding the Statistical Behaviour of Computer Systems
**{{< param subtitle >}}**\
by **{{< param author>}}**\
Originally posted **{{< param original_date>}}** 


## What is an Average?

To understand what the question is really asking we have to ask “what
are you trying to obtain from some arbitrary number that represents all
points in the distribution?”. Generally, this is the Expected Value.

There are different types of average that should be consider when
determining how a system is behaving. These averages are:

  - Arithmetic Mean – colloquially called average or mean, it is the sum
    of the values divided by the number of values.

  - Geometric Mean – the product of the values raised to the fractional
    power of the number of values.

  - Mode – the most common value present in the set.

  - Median – the middle value present in the set; also known as the
    50<sup>th</sup> percentile.

  - Harmonic Mean – is a form of arithmetic mean but is constructed to
    remove the influence of outliers. It is calculated by calculating
    the arithmetic mean of the inverse values and then inverting the
    result.

The easiest way to explain the difference is to look at an example set
and work out the different means and the implications of each. The
following data set will be used for this example:

|||||||
|-----------|----|----|----|----|------|
| Value     | 5  | 10 | 15 | 20 | 2000 |
| Frequency | 20 | 50 | 20 | 9  | 1    |


The mean of this set is (20x5+10x50+15x20+20x9+2000)/100=3080/100=30.8

The median of the set (or 50<sup>th</sup> percentile) = 10

The modal value =10

The geometric mean = 10.6

The Harmonic Mean = 100/(20/5+50/10+20/15+9/20+1/2000)=10.8

As can be seen from this example all the different averages are very
close to 10 except the mean. Looking at the 95<sup>th</sup> and
99<sup>th</sup> percentiles which are 20 and 2000 respectively, it is
clear that the arithmetic mean is even outside the 95<sup>th</sup>
percentile.

{{< figure src="images/media/chart.png" >}}

Plotting these data as a bar chart clearly shows that the distribution
does not fit the Normal Distribution Graph (see the next section).

You may at this point think that the data is contrived (which is true as
it was for illustration purposes) but system data looks very much more
like this graph than a normal distribution. See the graph taken from a
real system at the end of the next section and read the log normal
distribution section.

## Normal Distribution

Most people understand a Normal Distribution (Bell Curve) as it is
simple to comprehend. The frequency distribution curve looks like a bell
(hence bell curve).

{{<figure src="images/media/image1.png" >}}

There are some special properties of this curve:

1.  The mean, mode and median coincide and it is easy to calculate each
    by just calculating the arithmetic mean (hence forth the mean)

2.  The curve is symmetric about this mean.

The 95<sup>th</sup> Percentile is easy to calculate and is the mean plus
two standard deviations.

Unfortunately, computer systems (especially multi-tier) when you plot
their response frequency graphs do not match this bell graph. The
picture below demonstrates this and has come from a real system:

![](images/media/image2.png)

The red plot is the frequency distribution and the blue line is the
cumulative frequency from 0 to 100 percent.

What can be seen from this graph is that there is a high frequency at
the lower response times and a tailing off of a fewer number at higher
response times. This is clearly not normally distributed and so the mean
does not carry the same significance as it does in a normally
distributed variate.

## Log Normal Distribution

Computer systems tend to be distributed after what is known as a log
normal distribution. The following graph from wikipedia shows some
examples of log-normal frequency graphs:

![](images/media/image3.png)

These graphs can have different shapes being tall and thin or short and
fat. However, they all have a characteristic in common: they all have a
long tail asymptotically tending towards zero.

Computer system responses match these graphs reasonably closely (see
additional discussion points below). Therefore it is necessary to
understand the characteristic (mathematical) differences between this
type of distribution and the simple Normal Distribution.

### Relationship

From the name you may have guessed that a log normal is related to a
normal distribution by some form of mathematical expression. There is
actually a very simple relationship:

  - A log-normal distribution is a continuous probability distribution
    of a random variable whose logarithm is normally distributed.

So what does this imply to the averages, standard deviations and other
properties. Taking the logarithm of each point would and plotting this
distribution would produce a normal frequency distribution graph and
these would then follow the simple statistical rules understood from the
normal distribution. A normal distribution is defined by the properties
σ and μ and is written  \\(N({\mu},{\sigma})\\).

  - $${\mu}$$ is the mean of the log of the values

  - $${\sigma}$$ is the standard deviation of the log of the values

###   

### Averages of Log Normal Distributions

Unlike a normally distributed variate a log normally distributed variate
does not have a coincidental set of averages. In general 

$$
\text{mode} < \text{median} < \text{mean}.
$$

![](./images/media/image4.png)

From the graph above it is clear that the skew, σ (standard deviation of
the log of the points), produces widely different mean and mode; whereas
the median remains the same. 

The median is a key value for log-normal
distributions in that it is equivalent to the normal-distribution's mean
by a simple formulae 

$$
\text{median} = e^{\mu}
$$ 

ie., the median is natural number, e, raised by the mean of the normal distributed variate
(log x). This is actually equivalent to the geometric mean for a log
normal distribution. The median is, of course, also the 50<sup>th</sup>
percentile by definition.

### Additional Discussion Points 

Computer systems are actually made up of many different variates. These
are then combined by both additive and sometimes multiplicative impacts
on each other, providing us with distributions that are not perfect
log-normal distributions and can become multi-modal depending on the
varying paths. If all of these paths could be split these distributions
would become more log-normal, however, this is almost impossible in a
computer system. These graphs are still skewed and like log-normal
graphs and therefore the same arguments exists against using the
arithmetic mean.

The following data is taken from a real system and is composed of over
70,000 samples.

![](./images/media/image5.png)

The averages for this sample of data are:
- Mean = 918, 
- Median = 879, 
- Geometric Mean = 832, 
- Harmonic Mean = 774. 

Clearly the range of the averages is some 150
which is about a 20% increase from the lower to the upper value.

The following distribution is from the same system but for a different
function

![](./images/media/image6.png)

This distribution is clearly not like any distribution discussed above.
However through a closer look it appears that this has 3 distributions
added together (different paths through the code)

![](./images/media/image7.png) 

Mean = 1377, Median = 1327, Geometric Mean = 1184, Harmonic Mean = 915.

## Conclusion

The arithmetic mean clearly should be avoided when looking at “average”
response times from a system which does not provide high confidence that
the distribution is normally distributed for the following reasons:

  - It can naturally be far to the right by having a high value of σ
    (skewness)

  - It is clearly no longer the “middle” value (or Expected Value) as
    this is represented by the geometric mean for this distribution type
    (median)

  - Given only a small sample size (ie much less than an infinite
    number, which any sample size is of course) the discrete values,
    although statistically within a log-normal pattern, will have
    “outliers” thereby skewing the mean far away from the real value
    let alone a responsible value for the “average”

As seen from above it is important to also understand the shape of the
distribution because if you have a multi-modal distribution the
“average” again becomes difficult to determine. The median may be
considered as a better choice than the arithmetic mean. This median is
not the same as the geometric mean in this case. However, clearly none
of these averages are significant as there are really three different
underlying functions that are coalesced by the architecture of the
solution.

## Reference

See
[<span class="underline">http://en.wikipedia.org/wiki/Log-normal\_distribution</span>](http://en.wikipedia.org/wiki/Log-normal_distribution)
for a detailed mathematical description of log normal distribution
(where I borrowed a couple of these pictures).

Download the original [Libre Office Document](understanding-lognormal.odt)
