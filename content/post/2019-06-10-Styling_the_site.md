﻿---
title: "Changing the BeautifulHugo theme's style"
subtitle: "Using custom header to change the style"
tags: ["hugo", "css"]
date: 2019-06-10
categories: ["hugo"]
---

This article describes how I changed the style of the Gitlab BeautifulHugo theme for Hugo so that it is coloured the way I want it to be.  The steps are to find out where a style is actually applied, changing the css to test my restyling and then pulling these changes out of the base template so that a new version of the theme will not change my restyling.

<!--more-->

## Locating a Style

To discover which css file and property that is setting the style I will use firefox's debugger tool (available when you push `F12`.

Load the browser, open the debugger and browse to the site.  Now click the element we wish to inspect 

As you can see the debugger shows the following

{{<figure src="debugger.png" >}}


```css
a:hover, a:focus {
    color: #0085a1;
}

```

This block is loaded from file `main.css` line 27

The `main.css` file can be found in `theme/BeautifulHugo/static/css/main.css`

We can now change this to style the link as needed.

## Testing the Change

Save the change to the `main.css` and the site should automatically update in your browser if you are already running from the local hugo directory.  If not then from the hugo site root test the change with `hugo serve` and browse to `http://localhost:1313/...` as specified.

You will now see that the style has changed.

## Extracting the Changes out of the Theme

Unfortunately we are making changes to the default theme files and any them update would undo all of our changes.  The answer has to be to load the styles after main.css is loaded.  

If we look through the `partials` in `themes/BeautifulHugo/layouts/partials` we see there is a file called `head_custom.html` with contents

```
<!--
If you want to include any custom html just before </head>, put it in /layouts/head_custom.html
Do not put anything in this file - it's only here so that hugo won't throw an error if /layouts/head_custom.html doesn't exist.
-->
```

looking at the bottom of `head.html` we see that the custom head file is loaded after the rest of the `css`

```
  <link rel="stylesheet" href="{{ "css/main.css" | absURL }}" />
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" />
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" />
  <link rel="stylesheet" href="{{ "css/pygment_highlights.css" | absURL }}" />
  <link rel="stylesheet" href="{{ "css/highlight.min.css" | absURL }}" />
{{- partial "head_custom.html" . }}
{{ template "_internal/google_analytics_async.html" . }}
</head>
```

So reading this we can create a `/layouts/partials/head_custom.html` file and get it to load our css.  We can also use the same call with the hugo templating `{{}}` inside the link to ensure we generate the correct href.  I am going to read our required css from a file called `css/overrides.css`

```
<link rel="stylesheet" href="{{ "css/overrides.css" | absURL }}" />

```

The css file needs to be create under the `/static` directory as `/static/css/overrides.css`

Add your styling changes, for example:

```
p a {
  /* text-decoration: underline */
  color: #f34f6f;
}

a:hover,
a:focus {
  color: #9b4ff3;
}

a {
  color: #f34f6f;
}

.navbar-custom {
  background: #f3ea4f;
  
}
```

