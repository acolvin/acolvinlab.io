---
title: "Hugo Extension"
subtitle: "Adding a Table Of Contents to Beautiful Hugo  Theme"
tags: ["hugo", "toc"]
date: 2019-06-08
categories: ["hugo"]
---
The Beautiful Hugo theme modified by the gitlab team does not as standard support table of content production in the articles that it produces.  This is a blog of how I added it as an option to the `single` page 

<!--more-->

## Table of Content Partial

The first thing needed is to create a table of contents partial. Create a file called `beautifulhugo/layout/partials/toc.html`.  This is going to hold our code to generate the Table of Contents.

Hugo provides a mechanism to generate the details of the Table of Contents 
`{{ .TableOfContents}}`.  This has to be wrapped appropriately so that it can be controlled from the front matter of the pages and to also only display if the article is long enough.

```
{{ if and (gt .WordCount 400 ) (.Params.toc) }}
   {{.TableOfContents}}
{{ end }}
```

Now we need to add a little layout control to ensure that the Table of Contents doesn't waste the full page width.  To do this we add a div around the `{{.TableOfContents}}`

```
{{ if and (gt .WordCount 400 ) (.Params.toc) }}
  <div class="col-lg-6 col-lg-offset-0 col-md-10 col-md-offset-0">
 
    {{.TableOfContents}}

</div>
{{ end }}
```
### Adding the Table of Contents into the Page

I added the changes into the `_default/single.html`.  I know this is not the correct place and that I should have created a copy and edit that but that is for me to fix later.

I first started by adjusting the margins of the content section

```
{{ define "main" }}
<div class="container">
  <div class="row">
   
    <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1">
      <article role="main" class="blog-post">
```

I change `<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">` because I find the margins are just wasteful.  This isn't actually necessary for the TableOfContents

Now add the `toc` partial before the content

```
      <article role="main" class="blog-post">
        {{ partial "toc.html" . }}
        {{ .Content }}
      </article>
```

To display a table of content in an article add `toc: true` to the front matter of an article
which will render as the example 

{{<figure src="withtoc.png">}}

without the `toc: true` in the front matter the article will render as 

{{<figure src="withouttoc.png">}}

