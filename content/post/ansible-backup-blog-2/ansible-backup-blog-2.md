---
title: "Ansible Backup Part Two"
subtitle: "Creating an Ansible Script to Cross Backup a set of Servers"
tags: ["ansible", "borg", "bash"]
date: 2019-06-14
original_date: 2018-07-28
categories: ["ansible"]
author: Andrew Colvin
bigimg: [{src: "post/ansible-backup-blog-2/ansible-backup-blog-2.svg"}]
---


In the [previous part]({{< relref "ansible-backup-blog-1.md" >}}) we created an ansible back up script that is capable of creating a back up on each server in the group called cluster and also would cross backup each server onto each other.  This seems sensible when there a small number of servers but the number of backups made rises by the square of the number of servers.  For 2 servers that means 4 backups, for 3 servers we have 9 backups.  Twenty servers rapidly becomes 400 backups whereas 100 servers would be 10,000 backups.  Clearly this is not sustainable for anything other than a few servers.

What I am going to look at in this part is providing a list of servers that are the backup target servers so that I can reduce our backup volume to a multiplication of the number of these backup target servers.  

<!--more-->

# Ansible Backup Part Two
**{{< param subtitle >}}** \
by **{{< param author>}}** \
Originally posted **{{< param original_date>}}** 

The backup servers will be made addressable across any server interface that is required to communicate to the backup destinations and not necessarily the first interface ansible comes across by the host name.
 
Additionally, The use of ansible loop constructs will replace the outdated `with_items` construct and some string and list manipulation within ansible. 

## Backup Destination and Backup Hosts

The `backup_destinations` variable existed in part one but had to match
the hosts in the cluster group. What is needed is that it can contain any hosts
by name and importantly not just ones that exist in the run group or any
context of ansible. 

```ansible
vars:
- backup_destinations: [ 'host2.nas.acolvin.me.uk', 'host3.nas.acolvin.me.uk']
```

The backup hosts will be calculated from the cluster group using jinja2.
This is added into a variable called `run_hosts_string`

```ansible
- run_hosts_string: "{% set comma = joiner(',') %}{% for host in groups['cluster'] -%}{{comma()}}{{hostvars[host]['ansible_hostname']}}{%- endfor %}"*
```

This command generates a comma separated string of all the short host
names of the hosts defined in the cluster group

Now that this string has been created it needs to be turned into a list.
This is completed with a `set_fact` task:

```ansible
  tasks:
    - set_fact: run_hosts="{{run_hosts_string.split(',')}}"
      run_once: true
```

The script has now created a list of the short names of the backup hosts
and a list of the backup destination servers. The script relies on the
fact that each host is able to access the backup destination servers
using ssh keys.

I have also added an extra environment variable:
`BORG_RELOCATED_REPO_ACCESS_IS_OK: "yes"` which allows the
repository to be accessed using different host names. This is needed as
the script in Part One accessed the repositories through the shortname
and now I am accessing it across a different network designed for data
transfers (nas.a…. subdomain). Without this environment variable
Borgbackup will stop and ask for confirmation which will never be
answered by the ansible script.

## BORG Commands

### Repository Initialisation

This command has been changed to create the repository for run host
against each backup_destination. It has to be assumed that this has to
be done across SSH for all repositories as there is no guarantee that
any back up destination is local to the cluster group. The scriptlet has
replaced the with_items in the task with a simple loop: and the
{{item}} has been added as the ssh destination of the borg command. The
repository name has also been changed to that of the value of
`ansible_hostname`.

```ansible
- name: init repositories
  shell: borg list {{item}}:/backup/{{ansible_hostname}}; if [ $? == 2 ] ; then `borg init -e repokey {{item}}:/backup/{{ansible_hostname}}` ; echo -n "REPO_MISSING" ; fi 
  register: repoexists
  changed_when: "'REPO_MISSING' in repoexists.stdout"
  loop: "{{backup_destinations}}"
  
```

### Local Backups

I was of two minds whether to retain the local backups section. Given
that there is no guarantee of there being any and we could create all of
the backups using the remote section.  However, it provides a little bit of
performance boost to retain it.

```ansible
- name: local backup
shell: >
  borg list /backup/{{ansible_hostname}} | grep -q `date +%d%m%Y:%H` && echo -n BACKEDUP || borg create --verbose --exclude-caches --exclude-from /etc/borgmatic/excludes.txt /backup/{{ansible_hostname}}::root.`date +%d%m%Y:%H%M` /
when: item[1]==ansible_hostname and
item[0].split('.')[0]==ansible_hostname
register: run_borg
changed_when: run_borg.stdout != 'BACKEDUP'
#     with_items: "{{ backup_destinations }}"
loop: "{{ backup_destinations | product(run_hosts) | list }}"

```

The with_items (commented out) has been replaced by a more complex loop
definition. This construct creates a list of lists `[[backup destination , run host]]`. Each item is the pair `[backup destination, run host]`. To be local the run host must be the same as the current
ansible host. Additionally, the test must also ensure that the backup
destination is the same host. I rely on the fact that my servers all
have the same host and different domain for each interface. To do this
test I need to break down the backup host to the short name. This is
completed by splitting the name at the ‘.’ character and taking the
first entry; I cannot rely on ansible `hostvars` as the backup destination
may not be an ansible host. If all three values are equal then I can
complete a local backup:

```ansible
when: item[1]==ansible_hostname and item[0].split('.')[0]==ansible_hostname

```

### Remote Backups

Remote backups are all completed across ssh. The task uses the same loop
construct as the local backup but now ensures that the run_host is the
same as the ansible host (having the opposite test would provide an
additional set which may be of use for pulling backups). The script also
checks that the destination server is not the current run host (ie it
really is remote). If the last part of the check is removed this task
would backup all hosts including those that are local but would
communicate across ssh.

```ansible
- name: remote backup
shell: >
    borg list {{item[0]}}:/backup/{{ansible_hostname}} | grep -q `date +%d%m%Y:%H` && echo -n BACKEDUP || borg create --verbose --exclude-caches --exclude-from /etc/borgmatic/excludes.txt {{item[0]}}:/backup/{{ansible_hostname}}::root.`date +%d%m%Y:%H%M` /
when: item[1]==ansible_hostname and
item[0].split('.')[0]!=ansible_hostname
#            when: item != ansible_hostname
register: run_borg
changed_when: run_borg.stdout != 'BACKEDUP'
loop: "{{ backup_destinations | product(run_hosts) | list }}"

```

The borg command now uses `{{item[0]}}` for the ssh host name (the full
name of the server and hence allowing control of the route out of the
server) and the ansible_hostname as the repo name

## Example Run

```ansible
PLAY [cluster]
***************************************************************************************************

TASK [Gathering Facts]
***************************************************************************************************
ok: [host3.acolvin.me.uk]
ok: [host2.acolvin.me.uk]

TASK [set_fact]
***************************************************************************************************
ok: [host2.acolvin.me.uk]

TASK [zypper]
***************************************************************************************************
ok: [host3.acolvin.me.uk] => (item=['borgbackup', 'borgmatic'])
ok: [host2.acolvin.me.uk] => (item=['borgbackup', 'borgmatic'])

TASK [copy]
***************************************************************************************************
ok: [host3.acolvin.me.uk]
ok: [host2.acolvin.me.uk]

TASK [init repositories]
***************************************************************************************************
ok: [host3.acolvin.me.uk] => (item=host2.nas.acolvin.me.uk)
ok: [host3.acolvin.me.uk] => (item=host3.nas.acolvin.me.uk)
ok: [host2.acolvin.me.uk] => (item=host2.nas.acolvin.me.uk)
ok: [host2.acolvin.me.uk] => (item=host3.nas.acolvin.me.uk)
changed: [host3.acolvin.me.uk] => (item=host)
changed: [host2.acolvin.me.uk] => (item=host)

TASK [local backup]
***************************************************************************************************
changed: [host3.acolvin.me.uk] => (item=['host3.nas.acolvin.me.uk','host3'])
changed: [host2.acolvin.me.uk] => (item=['host2.nas.acolvin.me.uk','host2'])

TASK [remote backup]
***************************************************************************************************
changed: [host3.acolvin.me.uk] => (item=['host2.nas.acolvin.me.uk','host3'])
changed: [host2.acolvin.me.uk] => (item=['host3.nas.acolvin.me.uk','host2'])
changed: [host3.acolvin.me.uk] => (item=['host', 'host3'])
changed: [host2.acolvin.me.uk] => (item=['host', 'host2'])

PLAY RECAP
***************************************************************************************************
host2.acolvin.me.uk       : ok=7   changed=3   unreachable=0   failed=0 
host3.acolvin.me.uk       : ok=6   changed=3   unreachable=0   failed=0

```

The Cluster Group is `[host2.acolvin.me.uk, host3.acolvin.me.uk]`

The Backup Servers are now `host2.nas.acolvin.me.uk, host3.nas.acolvin.me.uk` and an 
additional one called `host` which is not in the ansible inventory list

The script recognised the pre-existence of the repositories on
`host2.nas.acolvin.me.uk` and `host3.nas.acolvin.me.uk` but created new
repositories on `host`

New local backups were made for `host2` and `host3`

New remote backups were created for `host3` on `host2.nas` and `host` and for
`host2` on `host3.nas` and `host`

Note that because I walk through the product of the two arrays of hosts
there are many skipped combinations. I have suppressed the output of
these with the ansible config property `DISPLAY_SKIPPED_HOSTS=False`

## Conclusion

Part two has given us the fine control of what servers will be
backed up and also which servers they will back up onto. The script has
utilised the loop characteristics and also some more complex list
construction within ansible.
