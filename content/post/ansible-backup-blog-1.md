---
title: "Ansible Backup Part One"
subtitle: "Creating an Ansible Script to Cross Backup a set of Servers"
tags: ["ansible", "borg", "bash"]
date: 2019-06-13
original_date: 2018-07-25
categories: ["ansible"]
author: Andrew Colvin
bigimg: [{src: "post/ansible-backup-blog-1/ansible-backup-blog-1.svg"}]
---

I have a cluster of servers all running opensuse tumbleweed and I want
a central script that runs and creates back ups of every machine onto
all the machine (including the local machine).  This provides backup
redundancy.  The blog describes how using Ansible and Borg Backup delivers the solution to make it deliverable from a single location.

<!--more-->


# Ansible Backup Part One
**Creating an Ansible Script to Cross Backup a set of Servers** \
by **{{< param author>}}**\
Originally posted **{{< param original_date>}}** 

## Introduction

I have a cluster of servers all running opensuse tumbleweed and I wanted
a central script that could be run that would back up every machine onto
every machine (including the local machine) to provide backup
redundancy.

This provides me a level of security over having a central backup
service in that I can lose n-1 servers of the n devices and be able to
restore all others. I also didn’t want to replicate a backup between
servers because if the backup repository is corrupt then I will
replicate that corruption.

I decided to use borg backup for the backup application
(<https://www.borgbackup.org/>) and ansible as the controlling script.
For information I could have just implemented borgmatic on every machine
that will run on a schedule and do what is necessary but again this left
me with the dilemma of implementing and managing the borgmatic configs
on each server.

I created a device (logical volume) on each server and mounted this at 
`/backup`. The filesystem used was `btrfs` allowing me to snapshot the
backup repositories if required and stream the snapshot across the
network to another server for central snapshot storage (not in the scope
of this blog).

Note that all servers have ssh access to each other using ssh keys.

## Backup Naming 

The repositories for each host will be created under `/backup/<short
host name>`

Each backup will be created in the corresponding repository and named
using the following scheme: 
```
root.`date +%d%m%Y:%H%M`
```

This will create a backup called `root.ddmmyyyy:HHMM` and will relate to the date and time the backup was created. Note that the word `root` was placed at the front to show that it relates to a backup from `/`. Note that `/backup` will be excluded from the backup along with the other directories and files as required.

## Ansible Requirements

The following are the requirements that the ansible playbook needs to execute'

1. Install the borg software if it isn't installed

2. Copy the exclude file to each server

3. Create the repositories if they do not exist using `repokey` encryption 

4. Create the local machine backups if there isn’t a backup within the current clock hour

5. Create backups of each machine on the other server if there isn’t a backup within the current clock hour

## Ansible Script

```ansible

# cluster is defined as the set of hosts to be backed up
- hosts: cluster

# do the backup as root as we are backing up whole servers
  become: yes

# backup_destinations is the list of servers that remote backups will be created on
  vars: 
    - backup_destinations: [ 'host2', 'host3' ]

# this encrypted file holds the repository password
  vars_files:
    - borg-key.yaml

# create this environment variable so the password does not have to be added to all borg commands
  environment:
    BORG_PASSPHRASE: "{{ borg_password }}"

# The actions to be undertaken
  tasks:

# Install the software if required. Zypper is the opensuse command for software management 
# (similar to yum). There is an ansible zypper module
    - zypper: name={{item}} state=present
      with_items:
        - "borgbackup"
        - "borgmatic"

# Copy the source exclude.txt file to each server 
    - copy: 
        src: borgexclude.txt
        dest: /etc/borgmatic/excludes.txt
        owner: root
        group: root
        mode: u=rw,g=r

#initialise the repositories. This uses shell functionality. See the below section for explanation

    - name: init repositories
      shell: borg list /backup/{{item}}; if [ $? == 2 ] ; then `borg init -e repokey /backup/{{item}}` ; echo -n "REPO_MISSING" ; fi 
      register: repoexists
      changed_when: "'REPO_MISSING' in repoexists.stdout"
      with_items: "{{ backup_destinations }}"

# create all local backups
    - name: local backup
    shell: >
           borg list /backup/{{ansible_hostname}} | grep -q `date +%d%m%Y:%H`
           && echo -n BACKEDUP || borg create --verbose --exclude-caches --exclude-from /etc/borgmatic/excludes.txt /backup/{{ansible_hostname}}::root.`date +%d%m%Y:%H%M` /
#   when: item == ansible_hostname
    register: run_borg
    changed_when: run_borg.stdout != 'BACKEDUP'
#   with_items: "{{ backup_destinations }}"

# Do the remote backups
    - name: remote backup
      shell: >
             borg list {{item}}:/backup/{{ansible_hostname}} | grep -q `date +%d%m%Y:%H`
             && echo -n BACKEDUP || borg create --verbose --exclude-caches --exclude-from /etc/borgmatic/excludes.txt {{item}}:/backup/{{ansible_hostname}}::root.`date +%d%m%Y:%H%M` /
      when: item != ansible_hostname
      register: run_borg
      changed_when: run_borg.stdout != 'BACKEDUP'
      with_items: "{{ backup_destinations }}"

```

### Creating Repositories

If you try to create a repository using borg and one exists it will
throw an error. Therefore we need to determine if the repository exists
(I have assumed that the device is already mounted at `/backup`).

To complete the test I use the list command and check the return code.
This will return error code 2 if the location is not a borg repository:
*`borg list /backup/{{item}};`*

This same shell call then tests this value for 2 and then initialises
the repository: 
```
if [ $? == 2 ] ; then `borg init -e repokey /backup/{{item}}` ; 

```

The last part echoes out the string `REPO_MISSING` for later testing and
ends the if: `echo -n "REPO_MISSING" ; fi`

The result is then registered into a variable `repoexists`. Each command
is set to have changed if the string `REPO_MISSING` is found in this
variable: `changed_when: "'REPO_MISSING' in repoexists.stdout”`.

To create all the repositories for all hosts we loop over the list of
hosts: `with_items: "{{ backup_destinations }}"`

Note that this requires the `backup_destinations` list to be a superset
of the cluster host list. An alternative would be to have a list of
servers to back up and read them at run time.

### Running the Backup

To create the backup on the local server using borg the following
command is used 

```
borg create --verbose --exclude-caches --exclude-from /etc/borgmatic/excludes.txt /backup/{{ansible_hostname}}::root.`date +%d%m%Y:%H%M` 
```
However, this alone would not provide the idempotency required (run
once per clock hour). To achieve this the script uses a similar trick developed in the initialisation phase. 


- get list of existing backups in the repo, 
- grep for the value of `date +%d%m%Y:%HH` (ie ignoring minutes), 
- if this exists (&&) then echo the BACKEDUP string; 
  - otherwise run the backup, 
- the changed flag is then set when the value is not BACKEDUP.

To do the remote back ups we iterate this over the `backup_destinations`
variable adding the host value (`{{item}}`) to the borg commands. We also
add the **when** constraint to exclude another local backup over ssh

## Running the Playbook

```
host:~/ansible # ansible-playbook  --vault-id @prompt backup.yaml
   
Vault password (default):    
  
PLAY [cluster]
*****************************************************************************************
  
TASK [Gathering Facts]
*****************************************************************************************  
ok: [host3.acolvin.me.uk]  
ok: [host2.acolvin.me.uk]  
  
TASK [zypper]
*****************************************************************************************  
ok: [host3.acolvin.me.uk] => (item=['borgbackup', 'borgmatic'])  
ok: [host2.acolvin.me.uk] => (item=['borgbackup', 'borgmatic'])  
  
TASK [copy]
*****************************************************************************************  
ok: [host2.acolvin.me.uk]  
ok: [host3.acolvin.me.uk]  
  
TASK [init repositories]
*****************************************************************************************  
ok: [host3.acolvin.me.uk] => (item=host2)  
ok: [host2.acolvin.me.uk] => (item=host2)  
ok: [host3.acolvin.me.uk] => (item=host3)  
ok: [host2.acolvin.me.uk] => (item=host3)  
  
TASK [local backup]
*****************************************************************************************  
changed: [host3.acolvin.me.uk]  
changed: [host2.acolvin.me.uk]  
  
TASK [remote backup]
*****************************************************************************************  
skipping: [host2.acolvin.me.uk] => (item=host2)    
changed: [host3.acolvin.me.uk] => (item=host2)  
skipping: [host3.acolvin.me.uk] => (item=host3)    
changed: [host2.acolvin.me.uk] => (item=host3)  
  
PLAY RECAP
*****************************************************************************************  
host2.acolvin.me.uk        : ok=6    changed=2    unreachable=0
   failed=0      
host3.acolvin.me.uk        : ok=6    changed=2    unreachable=0
   failed=0  

```
Note that the password for the borg repositories are maintained in an
ansible vault. The command line prompts the user to enter the vault
password. The `borg-key.yaml` contains: 
```
borg_password: xxxx
```

## Conclusion

The script was a good introduction to get an understanding of writing simple playbooks  for ansible.  It exposed a few issue that I had to grasp of such as managing idempotency with scripts in ansible.  

The solution will only work for a small number of servers as the back up volume grows with the square of the number of servers.  In  [part two]({{<relref "ansible-backup-blog-2.md">}}) I will look at controlling the backup servers to a different list to manage the volume.
