---
title: "Further Hugo Addons"
subtitle: "Adding Mermaid and Reveal.js to the Hugo Theme"
tags: ["hugo", "mermaid", "revealjs"]
date: 2019-06-21
author: "Andrew Colvin"
toc: true
---

This short article wall detail how I added both mermaid graphics to my gitlab derived BeautifulHugo Them that I have already styled in previous articles. I will look at mermaid first as it is the simplest and then move on to reveal.js. This was heavily driven by the work from the [hugo docbook theme](https://github.com/vjeantet/hugo-theme-docdock).
<html><p>&nbsp;</p></html>

<!--more-->


# {{% param "title" %}}
**{{< param subtitle >}}** \
by **{{< param author>}}** \

This short article wall detail how I added both mermaid graphics to my gitlab derived BeautifulHugo Them that I have already styled in previous articles. I will look at mermaid first as it is the simplest and then move on to reveal.js. This was heavily driven by the work from the [hugo docbook theme](https://github.com/vjeantet/hugo-theme-docdock).

## Adding Mermaid 

Mermaid is a client side graphics drawing javascript library. It isn't as full featured as [plantUML](plantuml.com) or [ditaa](http://ditaa.sourceforge.net/) which would require an additional step to compile the images as part of the build process. It is built into gitlab and there are requests for it to be enabled directly in github as well. 

 Read more about [mermaid](https://mermaidjs.github.io/)

To start go a grab the latest version of the mermaid files. There are multiple cdn where the file can be collected but it is also available from the github realease folder of the project [https://github.com/knsv/mermaid/releases/tag/7.0.0](https://github.com/knsv/mermaid/releases/tag/7.0.0) although this isn't the absolute latest. The minimum files required are:

- mermaid.js
- mermaid.css

Optionally I have added the two additional themes `mermaid.dark.css` and `mermaid.forest.css`.  These are all placed in a folder called mermaid. I need to add them to my site so I add them into the directory `static/mermaid`.

Although this will ensure they are copied to the as static resource during the hugo build process they will not be loaded because no page resource will load them. Fortunately we can use the similar concept of overloading the `head_custom.html` we explored in [a previous article]({{<ref "post/2019-06-10-styling_the_site.md">}}). However, this time I will make use of the `footer_custom.html` file.

Create the file using `touch layouts/partials/footer_custom.html` and add the following with your favourite text editor

```html
<link href="{{"mermaid/mermaid.css" | relURL}}" rel="stylesheet" />
<script src="{{"mermaid/mermaid.js" | relURL}}"></script>

```
The static content is now loaded and ready. What is needed now is a shortcode that will allow hugo to pass the graph definition to the mermaid.js scripts when the page is loaded.  The content needs to be enclosed in start and end shortcode tags (note I added extra spaces between `{{` and `<` to stop it rendering

```hugo
{{ < mermaid >}}

{{ < /mermaid >}}
```
This shortcode needs to create a `div` with class `mermaid` and pass the content to inside the `mermaid` tags into this div.  Create the file `layout/shortcodes/mermaid.html` with the following content. It additionally provides an optional align tag to align the generated image left, centre or right

```html
<div class="mermaid" align="{{ if .Get "align" }}{{ .Get "align" }}{{ else }}center{{ end }}">{{ safeHTML .Inner }}</div>

```

The mermaid tags now generate 

```
{{% mermaid %}}{{% /mermaid %}}
```

Time to see it in action


```
{{ < mermaid > }}
graph LR
A --> B
{{ < /mermaid > }}
```
translates to
```
{{% mermaid %}}
graph LR
A --> B
{{% /mermaid %}}
```
and generates

{{< mermaid >}}
graph LR
A --> B
{{< /mermaid >}}

{{% notice warning "fa-exclamation-triangle" %}}
If the diagram isn't drawn then please press stop in your browser window. This is an issue with the 
  embedded slides so try not to mix mermaid and embedded slides on the same page until I get a resolution.

> The issue should now be fixed - [see this issue](https://git.acolvin.me.uk/andrew/andrew.pages.acolvin.me.uk/issues/1)
{{% /notice %}}

<!--
<html><div class="alert alert-warning">
  <strong>&#9888; Warning!</strong> If the diagram isn't drawn then please press stop in your browser window. This is an issue with the 
  embedded slides so try not to mix mermaid and embedded slides until I get a resolution.
</div></html>
-->

## Adding `reveal.js`

This JavaScript library provides the additional capability to generate slide shows as part of the site. This will be created as shortcode to embed the presentation as shown below or as a site themed slide show (without the bottom nav) as shown in [this link]({{<ref "slide/revealjs" >}})

{{<revealjs theme="sky" progress="true" controls="true">}}

## 1

slide 1

---

## 2 

slide 2

___

### 2.1

slide 2.1

---

## 3

slide 3

{{</revealjs>}}

For information I took out 10 pixels from the top nav bar depth so that it provided additional slide space and didn't compromise the navbar. 

### Install the `revealjs` Library

Download the library from [https://github.com/hakimel/reveal.js/releases](https://github.com/hakimel/reveal.js/releases) and copy the distribution folder (with a rename) to `static/revealjs`.

### Create Shortcode 

The next step in the process is to create our revealjs shortcode. This is a complex task and as stated above I am utilising the work from the docbook theme. Copy the shortcode from this theme:

``` 
wget 'https://raw.githubusercontent.com/vjeantet/hugo-theme-docdock/master/layouts/shortcodes/revealjs.html` > layouts/reveal.js

```

The `iframe` in the shortcode has a `src` listed of `none.html` which is just required to be empty. Create the empty file in `static` with `touch static/none.html`.

The `shortcode` processes `---` (dashes) as next horizontal slide and `___` (underscore) as next vertical slide. It recognises markdown.  There are a number of optional arguments that can be passed to the shortcode:

  transition: "concave"
  controls: true
  progress: true
  history: true
  center: true

### Create Slide Archetype and Page Kind

What is needed is a new page type so that the slide show can be full page but with the navbar.  If I want full page then just clicking the show full screen link under the embedded will provide this.

First we need to create that slide content type. Create the directory `layouts/slide` and copy the shortcode `revealjs.html` file into this location renaming the copy as `slide.html`.

Now edit the file and make the following changes:

  Remove the `iframe`\
  Change `embedded` from `true` to `false` \
  Add this to the top to add the navbar
  ``` 
  <html>
     <!--<head><meta charset="utf-8"></head>-->
  {{ partial "head.html" . }}
  <body>
  {{ partial "nav.html" . }}
```

That is all that is needed for the `slide` article kind and slide decks can now be created using 

```
hugo new --kind=slide post/<name>.md
```

However, what we are missing is an archetype that can create a templated document. A page archetype was created in a previous article so the process is understood. Create the file `archetypes/slide.md` with the following content as top matter:

```
---
title: "Slide title"
type: "slide"
theme: "moon"
revealOptions:
  transition: 'concave'
  controls: true
  progress: true
  history: true
  center: true
---
```

Additionally add some template text to remind us how to build the slides:

```
# Slide 1

___

## Slide 1.1

- Turn off alarm
- Get out of bed

___

## Slide 1.2

- Eat eggs
- Drink coffee

---

# Slide 2

___

## Slide 2.1

- Eat spaghetti
- Drink wine

___

## Slide 2.2

- Get in bed
- Count sheep

```

### Adding Slide Attributes

The Hugo markdown processing interferes with the html comment solution of allying attributes to slides. you can get aroound this by enclosing the element definition as specified in the revealjs documentation with `<html></html>` tags.  For example to make each of the bullets only appear when next slode is requested use:

```
* theme      <html><!-- .element: class="fragment" --></html>
* transition <html><!-- .element: class="fragment" --></html>
* controls   <html><!-- .element: class="fragment" --></html>
* progress   <html><!-- .element: class="fragment" --></html>
* history    <html><!-- .element: class="fragment" --></html>
* center     <html><!-- .element: class="fragment" --></html>
```

## Conclusion

The mermaid library was fairly easy to add and provided no issues getting it to work.  The reveal library took some time to understand and work out how it operated and I can only give my kudos to the authors of the docbook theme who did most of the leg work for me. 

I have already created a nice mermaid diagram [in the ASAP Project Page]({{<ref "page/asap/asap-overview.md" >}}) to show how the different agents of the ASAP suite of tools relate to each other.


