---
title: "Page Changeable Logo"
subtitle: "Let a Page Set its own Logo"
tags: ["hugo"]
date: 2019-07-17
categories: ["hugo"]
logo: "post/logo/logo.png"
author: "Andrew Colvin"
---
by {{< param author>}}  \
\

The current Beautiful Hugo theme provides a small elliptical logo image which is static across the site.  This  is generally OK except when it isn't such as for a series when you may want a logo for the series. The article delves into this logo change as seen on this post above (you will have to read more now to see it :-D )

<!--more-->



## Look Up 

See this page has a different logo - OK now to spill the beans

## Front Matter

This is achieved using front matter `logo: <path from content>` so for this page it is `logo: "post/logo/logo.png"`.  If you try it nothing will happen so there is more magic somewhere.  This is just how we will use it when finished.

## Navbar

The logo is part of the Navigation Bar. Its code can be found at `themes/beautifulhugo/layouts/partials/nav.html`. Looking at the code we can identify the logo code 

```
    <div class="avatar-container">
      <div class="avatar-img-border">

      
      
        {{ if isset .Site.Params "logo" }}
          <a title="{{ .Site.Title }}" href="{{ "" | absLangURL }}">
            <img class="avatar-img" src="{{ .Site.Params.logo | absURL }}" alt="{{ .Site.Title }}" />
          </a>
        {{ end }}
        
        
      </div>
    </div>
```

This Hugo template segment test to see if the Site Parameter `logo` is set and if it is adds this logo image to th navbar using the css class `avatar-img`

The change is therefore easy instead of checking if the site logo is set we can test if a page logo is set and display this and if not revert to the site logo. 

My `logo` variable can be access through `.Params "logo"` so we test if this is set and display this and if not we run the the site logo code.

```
    <div class="avatar-container">
      <div class="avatar-img-border">
      
<!-- I have added this so I can change the small logo using front matter "logo: url"  -->
        {{ if isset .Params "logo" }}  
          <a title="{{ .Site.Title }}" href="{{ "" | absLangURL }}">
            <img class="avatar-img" src="{{ .Params.logo | absURL }}" alt="{{ .Site.Title }}" />
          </a>
<!-- else added here-->
        {{ else if isset .Site.Params "logo" }}
          <a title="{{ .Site.Title }}" href="{{ "" | absLangURL }}">
            <img class="avatar-img" src="{{ .Site.Params.logo | absURL }}" alt="{{ .Site.Title }}" />
          </a>
        {{ end }}

        
      </div>
    </div>
```

## Easy Peasy

Yep that is it. Change your logo with ease. A small addition could be to allow the link location to be set, e.g., back to the index of the series.
