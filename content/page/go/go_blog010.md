---
title: Go Blog Part 10 - Saving and Restoring Data using a File as a Backing Store
author: Andrew Colvin
date: 2019-06-01
toc: true
weight: 200
tags: ["go", "series", "json"]
layout: article
previous: "go_blog009.md"
index: "readme.md"
#next: "go_blog011.md"
---


by Andrew Colvin, June 2019

In this artice I will be returning to Go and developing a mechanism to save and load the Readings to and from a file. The article will start writing records and then look at reading records. After this we will discuss handling of improper data read into the program and how we handle this.


## Outputting Data

We will start by looking at writing to our backing store. This will be completed in 2 stages
-  Stream JSON objects from our set
-  Write the stream to a file

### Streaming JSON Objects

Currently the VGSet object has the capability to produce JSON output.  However, this produces a single array of Readings

```json
[
  {
    "Reading": 5,
    "Date": "2019-01-25T06:59:00Z",
    "Comment": "test",
    "Food": "",
    "Exercise": "",
    "Fasting": false,
    "RealFasting": false,
    "Preprandial": false,
    "ID": "id01"
  },
  {
    "Reading": 5,
    "Date": "2019-01-25T07:59:00Z",
    "Comment": "test 2",
    "Food": "",
    "Exercise": "",
    "Fasting": false,
    "RealFasting": false,
    "Preprandial": false,
    "ID": "id02"
  },
  {
    "Reading": 5,
    "Date": "2019-01-25T08:59:00Z",
    "Comment": "test 3",
    "Food": "",
    "Exercise": "",
    "Fasting": false,
    "RealFasting": false,
    "Preprandial": false,
    "ID": "1GFRt5ojh3H5ObUAZ4cDDcDDgWg"
  }
]

```

A stream of JSON objects would look like 

```json 
{
    "Reading": 5,
    "Date": "2019-01-25T06:59:00Z",
    "Comment": "test",
    "Food": "",
    "Exercise": "",
    "Fasting": false,
    "RealFasting": false,
    "Preprandial": false,
    "ID": "id01"
}{
    "Reading": 5,
    "Date": "2019-01-25T07:59:00Z",
    "Comment": "test 2",
    "Food": "",
    "Exercise": "",
    "Fasting": false,
    "RealFasting": false,
    "Preprandial": false,
    "ID": "id02"
}{
    "Reading": 5,
    "Date": "2019-01-25T08:59:00Z",
    "Comment": "test 3",
    "Food": "",
    "Exercise": "",
    "Fasting": false,
    "RealFasting": false,
    "Preprandial": false,
    "ID": "1GFRt5ojh3H5ObUAZ4cDDcDDgWg"
}

```

and generally without the pretty printing ie 

```json
{"Reading":5,"Date":"2019-01-25T06:59:00Z","Comment":"test","Food":"","Exercise":"","Fasting":false,"RealFasting":false,"Preprandial":false,"ID":"id01"}{"Reading":5,"Date":"2019-01-25T07:59:00Z","Comment":"test 2","Food":"","Exercise":"","Fasting":false,"RealFasting":false,"Preprandial":false,"ID":"id02"}{"Reading":5,"Date":"2019-01-25T08:59:00Z","Comment":"test 3","Food":"","Exercise":"","Fasting":false,"RealFasting":false,"Preprandial":false,"ID":"1GFRt5ojh3H5ObUAZ4cDDcDDgWg"}


```

Obviously we can still use the Readings ToJSON call to produce each individual object so that makes life a little simpler.  Therefore we can just iterate through the set and get each readings representation.

```go
    it:=getInstance().bgset.Iterator()
    for it.Next() {
        v:=it.Value().(*BGReading)
        vv, err :=v.ToJSON()

```

So this is my iterator and `vv` contains each readings JSON representation so now we have to decide how we stream it (and what does it mean).  

To stream the data means to write the data out to `io` byte by byte.  Go provides a simple interface for this `io.Writer` 

>  Writer is the interface that wraps the basic Write method.
>
> Write writes len(p) bytes from p to the underlying data stream. It returns the number of bytes written from p (0 <= n <= len(p)) and any error encountered that caused the write to stop early. Write must return a non-nil error if it returns n < len(p). Write must not modify the slice data, even temporarily.
>
> Implementations must not retain p.
>
> ```go
> type Writer interface {
>        Write(p []byte) (n int, err error)
> }
```




This is just what is needed so our function can take a type that implements the `Writer` interface and we can write our data to it through this method.

```go hl_lines="9"
func StreamJSON(w io.Writer) {

    it:=getInstance().bgset.Iterator()
    for it.Next() {
        v:=it.Value().(*BGReading)
        vv, err :=v.ToJSON()
        
        // TODO deal with error writing and transcoding
        if err==nil {w.Write(vv)}
    }
    w.Write([]byte("\n"))
}

```

The write is assuming that it does not error.  If it does we will get a partial stream.  It is a design decision on what to report if we get an error either from the marshalling to JSON or the write to the Writer.  As an example the write could fail because the disk is full or a network connection has been closed so all we can do is report the error back and let the layer above work out what to do.  At present I am going to ignore them and cross my fingers.

### Testing our Streaming Code

We currently have a unit test script that test our `BGSet` type and its functions.  If you remember this is the `BGSet_test.go` file and inside this we have a function called `TestBGSet(...)`.  

We can add a few lines of code to this to create a `Writer` to pass to our `BGSet` and look at the streamed output.  For now I am going to just create a `Writer` which outputs to `stdout` and stream to this:

```go hl_lines="7 8 9"
    a0,_:=ToJSON()
    t.Logf("%s\n",string(a0))
    
    if Size() !=4 {
       t.Errorf("set does not contain 4 items %d", Size())
    }
    w := bufio.NewWriter(os.Stdout)
    StreamJSON(w)
    w.Flush()

```

I am using the buffered io package and creating a Writer with NewWriter wrapping the Standard out file.  Because we have wrapped it with a buffered Writer we need to ensure it is flushed at the end. 

Although this isn't a proper test case we can see the output of the stream which is what we are trying to do

```
andrew@host:~/go/src/gitlab.acolvin.me.uk/go/bloodglucose/blood/pkg/blood> go test
{"Reading":5,"Date":"2018-12-01T09:00:00Z","Comment":"test","Food":"","Exercise":"","Fasting":false,"RealFasting":false,"Preprandial":false,"ID":"1LvJsL2Kkqc8xOaZ8cCC6whG6DQ"}{"Reading":7.8,"Date":"2018-12-01T10:00:00Z","Comment":"test1","Food":"","Exercise":"","Fasting":false,"RealFasting":false,"Preprandial":false,"ID":"1LvJsKH2lJXXcYk7ZmhFJilJYjZ"}{"Reading":6.8,"Date":"2018-12-01T11:00:00Z","Comment":"test2","Food":"","Exercise":"","Fasting":false,"RealFasting":false,"Preprandial":false,"ID":"1LvJsIp10g0svFloDvUUjSDtIcb"}{"Reading":4.8,"Date":"2018-12-01T12:00:00Z","Comment":"test3","Food":"","Exercise":"","Fasting":false,"RealFasting":false,"Preprandial":false,"ID":"1LvJsLcBPPrhDfY7EewXiUvotCb"}
PASS
ok      gitlab.acolvin.me.uk/go/bloodglucose/blood/pkg/blood    0.003s

``` 

Although it is difficult to see from output as shown I will add newlines for clarity

```
andrew@host:~/go/src/gitlab.acolvin.me.uk/go/bloodglucose/blood/pkg/blood> go test
{"Reading":5,"Date":"2018-12-01T09:00:00Z","Comment":"test","Food":"","Exercise":"","Fasting":false,"RealFasting":false,"Preprandial":false,"ID":"1LvJsL2Kkqc8xOaZ8cCC6whG6DQ"}
{"Reading":7.8,"Date":"2018-12-01T10:00:00Z","Comment":"test1","Food":"","Exercise":"","Fasting":false,"RealFasting":false,"Preprandial":false,"ID":"1LvJsKH2lJXXcYk7ZmhFJilJYjZ"}
{"Reading":6.8,"Date":"2018-12-01T11:00:00Z","Comment":"test2","Food":"","Exercise":"","Fasting":false,"RealFasting":false,"Preprandial":false,"ID":"1LvJsIp10g0svFloDvUUjSDtIcb"}
{"Reading":4.8,"Date":"2018-12-01T12:00:00Z","Comment":"test3","Food":"","Exercise":"","Fasting":false,"RealFasting":false,"Preprandial":false,"ID":"1LvJsLcBPPrhDfY7EewXiUvotCb"}
PASS
ok      gitlab.acolvin.me.uk/go/bloodglucose/blood/pkg/blood    0.003s


```

### Saving the Readings from the Rest Service

This section will look at saving the state of the `bloodrest` service created in previous articles when it is shutdown (this will allow a later step that reads the file and load the readings on initialisation).

The default file location will be the users home directory and called `bloodrest.json`.  A different file location can be set using the command line flags `-f` or `--file`.

Any existing file will be overwritten when the server is stopped with the interrupt signal.

#### Creating the Filename

The work needed for this has already been discussed in the previous article for creating the rest service.  This is a minor change to the command path and environment variable and is shown here


```go
    flag.StringVar(&filename,"file","","file backing store: default $HOME/bloodrest.json")
    flag.StringVar(&filename,"f","","file backing store: default $HOME/bloodrest.json (shorthand)")


```

Add the above before `flag.Parse()`.  Now create the string holding the filename

```go hl_lines="2 10"
    if filename == "" {
        //home,yes = os.UserHomeDir() // use this for go 1.12 instead of the if
        if runtime.GOOS == "windows" {
          home,yes = os.LookupEnv("%userprofile%")
        } else {
          home,yes = os.LookupEnv("HOME")
        }
        
        if yes {
            filename = home+string(os.PathSeparator)+"bloodrest.json"
        } else {
            filename = "bloodrest.json"
        }
        
    }

```

In the above snippet we are doing a few interesting things:
- using `runtime.GOOS` to determine the platform and either looking up `$HOME` or `%userprofile%` for windows.
- This can be replaced with os.UserHomeDir() if compiling with Go 1.12.1 or above
- using `os.PathSeparator` instead of `"/"` incase we are on windows.

#### Writing the File

To write to the file identified above we need to 

- Create the file and open it for writing
- Deal with an error if it cannot be opened for writing
- Stream the readings into the file
- Synchronise the file to disk
- Close the file

We will start by creating a separate function that does most of the work called `writeFile` in the `BloodRest.go` file.

To create and open a file we will use `os.Create`

>
>   `func Create`
>
>   `func Create(name string) (*File, error)`
>
> Create creates the named file with mode 0666 (before umask), truncating it if it already exists. If successful, methods on the returned File can be used for I/O; the associated file descriptor has mode O_RDWR. If there is  an error, it will be of type *PathError. 

Once we have created and opened our folder and finished with it we need to close the file.  I will use the defer on the close function so that it is automatically closed when the function exits no matter what path is taken through the function.  This only needs to be done when the create is successful. 


 
```go 
func writeFile(filename string){
    f,err:=os.Create(filename)
    if err==nil {
       defer f.Close()

```

Now the records can be streamed into the file and this file synchronised to disk.

```go
       blood.StreamJSON(f)
       f.Sync()
    } else {
        log.Fatal(err)
    }  
}

```

_Note we are not handling errors from the Stream process_.

#### In Operation

So now that the write function is in place we need to execute this write.  This is achieved by adding the write function after the channel `idleConnsClosed` is closed


```go hl_lines="5 6"
	<-idleConnsClosed
    
    log.Print("Blood Rest Server Shutdown")
    
    log.Print("Streaming Readings to ", filename)
    writeFile(filename)

```

The service is now built using `make`. Here is the output at the end of the service execution

```
./bloodrest-amd64
^C2019/05/30 14:18:54 Interrupt Received
2019/05/30 14:18:54 Blood Rest Server Shutdown
2019/05/30 14:18:54 Streaming Readings to /home/andrew/bloodrest.json


```

Here is the contents of `/home/andrew/bloodrest.json` with newlines added for clarity

```json
andrew@host:~/go/src/gitlab.acolvin.me.uk/go/bloodglucose/blood/bin> cat /home/andrew/bloodrest.json
{"Reading":5,"Date":"2019-01-25T06:59:00Z","Comment":"test","Food":"","Exercise":"","Fasting":false,"RealFasting":false,"Preprandial":false,"ID":"id01"}
{"Reading":5,"Date":"2019-01-25T07:59:00Z","Comment":"test 2","Food":"","Exercise":"","Fasting":false,"RealFasting":false,"Preprandial":false,"ID":"id02"}
{"Reading":5,"Date":"2019-01-25T08:59:00Z","Comment":"test 3","Food":"","Exercise":"","Fasting":false,"RealFasting":false,"Preprandial":false,"ID":"1GFRt5ojh3H5ObUAZ4cDDcDDgWg"}


```

## Reading the File

The Rest service is configured to stream its records to a file on shutdown. What is going to be discussed in this section is the code required to read the specified file on start up. 

The process to be followed will allow the program to load the records as it reads through the file without having to completely load the whole file and can continue past records with errors.  To accomplish this the process has to readh to the end of the record, process the string and unmarshall this as a JSON record and assume it is a BGReading. To make this easier so that the program doesn't have to parse the file for any of these character combinations `}<EOF>` or `}{` or `}<any combination of white space>{` where `<white space>` is space, carriage return, newline, tab and `}` is the closing tab, I decided to make each record written end with `\n` so that the program can read a line and process this as a record. 

Changing the write process is a simple change to the BGSet.go file

```go
func StreamJSON(w io.Writer) {

    it:=getInstance().bgset.Iterator()
    for it.Next() {
        v:=it.Value().(*BGReading)
        vv, err :=v.ToJSON()
        if err==nil {
            w.Write(vv)
```
Here are the changes
```go
            w.Write([]byte("\n")) // add this here
        }
    }
    //w.Write([]byte("\n")) // don't need the blank now
```

Now it is just a case of read the file line by line and process each line


Open the file

```go
    f, err := os.Open(filename);
    if err !=nil {
       log.Print(err);   
    } else { 
```

Create a scanner to read the file line by line

```go    
        var i int  
        var bg *blood.BGReading 
        scanner := bufio.NewScanner(f)
        for scanner.Scan() {
            bg=blood.NewBGReading()
            line:=[]byte(scanner.Text())
```

If the line is not blank then process as a JSON record 

```go
            if len(line)!=0 {
                err:=bg.FromJSON(line)
                if err !=nil && err!=io.EOF { 
```

Print the error for a failed conversion and do nothing if we are at end of file

```go
                    log.Print(err)
                } else if err == io.EOF {
```

Add record that has been converted

```go
                } else {
                    i++
                    blood.AddReading(*bg)
                }
            }
```

Close the file and log the number of records read and successful processed


```go
        }
        f.Close()
        log.Printf("%v messages read and converted",i)
    }

```

## Conclusion

And... That's all folks...

Simple wasn't it. We have read the stream of records with the data presented as one record per line. We can write out our records recorded in the rest service as it is closed down and recover on start up. We now have a simple persistance layer.  During the process we relooked at command line parameters and environment  variables and then investigated simple file IO operations.
