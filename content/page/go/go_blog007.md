---
title: Go Blog Part 7 - Publishing the Blogs on Gitlab Pages
authors: Andrew Colvin
date: April 2019
toc: true
weight: 170
tags: ["go", "series", "gitlab", "docker", "mkdocs", "pages", "git"]
layout: article
previous: "go_blog006.md"
index: "readme.md"
next: "go_blog008.md"
---

# Publishing Blogs to Gitlab Pages

## Installing Gitlab Pages

Gitlab Pages is a hosting solution for sites delivered through a gitlab repository. As I have been writing these articles and making them available through my gitlab which is not the best way to presention for the documents, I decided to turn on Gitlab Pages on my gitlab-ce installation and show how to set it up including a runner to do the build and deploy to pages

The basic Gitlab Pages is built into gitlab-ce so switching it on and configuring the network to access it is all that is needed initially

To make it available change the `gitlab.rb` configuration file and add the following properties


```

pages_external_url 'http://pages.acolvin.me.uk:88'
gitlab_pages['enable'] = true
gitlab_pages['inplace_chroot'] = true


```

The first line tells gitlab how to expose pages to the outside world.  I have an apache server acting as a proxy server in front of this which will translate port 88 and any TLS 

The second line turns it on and the third is required because I am running gitlab inside a docker container so it cannot do bind mounts.

Restart the container to make the changes active.

### Reconfigure Docker Gitlab Image 

At this point the gitlab process will fail to start.  This is caused by port 88 not being exposed to the container.  Therefore the script that creates the gitlab container needs to be amended to attach port 88

Add the following variable to the gitlab create script

```
PAGESIP=192.168.1.19
PAGESPORT=88
```

To allow us to direct which IP the container will connect to `0.0.0.0` means all.  Now change the doscker run command to attach port 88

```
docker run --detach \
        --hostname $HOSTNAME \
        --publish $PORTSSL:$PORTSSL --publish $BINDIP:$PORTHTTP:$PORTHTTP --publish $BINDIP:$PORTSSH:$PORTSSH --publish $PAGESIP:$PAGESPORT:88\
        --name $NAME \
        --restart always \
        --volume $CONFIG:/etc/gitlab/ \
        --volume $LOGS:/var/log/gitlab \
        --volume $DATA:/var/opt/gitlab \
        gitlab/gitlab-ce:latest



```

Now stop and delete the existing container and run the create script

```
docker stop gitlab-80
docker rm gitlab-80

```



## How Pages are deployed

Pages (in this configuration) deploys your site based on the repository name.  My repository for these articles is `go/bloodglucose/blood` and therefore the first part of the repo will create the host name `go.pages.acolvin.me.uk` with the uri being `bloodglucose/blood`.  Therefore the complete Pages Site for our repository is `http://go.pages.acolvin.me.uk:88/bloodglucose/blood/` 

Note that we defined `pages...` and not `go.pages...` so how are we going to resolve the dns for `go.pages...`?

The answer to this is to create a wildcard DNS entry for `*.pages.acolvin.e.uk`

The internal response to my nameserver lookup

```
# dig '*.pages.acolvin.me.uk'

; <<>> DiG 9.11.2 <<>> *.pages.acolvin.me.uk
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 37071
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 1, ADDITIONAL: 2

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;*.pages.acolvin.me.uk.         IN      A

;; ANSWER SECTION:
*.pages.acolvin.me.uk.  86400   IN      A       192.168.1.5

;; AUTHORITY SECTION:
acolvin.me.uk.          86400   IN      NS      ns.acolvin.me.uk.

;; ADDITIONAL SECTION:
ns.acolvin.me.uk.       86400   IN      A       192.168.1.239

;; Query time: 0 msec
;; SERVER: 192.168.1.239#53(192.168.1.239)
;; WHEN: Mon Apr 15 18:50:57 BST 2019
;; MSG SIZE  rcvd: 99


```


Here is the external query through google's nameservice

```
# dig @8.8.8.8 '*.pages.acolvin.me.uk' 

; <<>> DiG 9.11.2 <<>> @8.8.8.8 *.pages.acolvin.me.uk
; (1 server found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 7025
;; flags: qr rd ra; QUERY: 1, ANSWER: 2, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 512
;; QUESTION SECTION:
;*.pages.acolvin.me.uk.         IN      A

;; ANSWER SECTION:
*.pages.acolvin.me.uk.  14399   IN      CNAME   www.acolvin.me.uk.
www.acolvin.me.uk.      14399   IN      A       87.75.97.236

;; Query time: 50 msec
;; SERVER: 8.8.8.8#53(8.8.8.8)
;; WHEN: Mon Apr 15 18:55:04 BST 2019
;; MSG SIZE  rcvd: 84


```


### Proxy Setup

You will have noticed that the dns requests returned IPs that are not that exposed by the container.  These are the internal/external IPs of the proxy servers.

This isn't necessarily necessary if you are happy to expose the gitlab service directly but I prefer to have a proxy so I can make my exposure inside and outside my environment act the same.

To accomplish this proxy setup with apache I created a virtual host file on my proxy

```
<VirtualHost *:80>
 DocumentRoot /srv/www/gitlab
 ServerName pages.acolvin.me.uk
 ServerAlias *.pages.acolvin.me.uk
 ServerAdmin andrew@acolvin.me.uk
 <Proxy "*">
  AllowOverride None
  Require all granted
 </Proxy>

 ProxyPreserveHost On
 ProxyPass "/" "http://192.168.1.19:88/"
 ProxyPassReverse "/" "http://192.168.1.19:88/"


</VirtualHost>


```


## Building the Site

At this point we have our Gitlab Pages installed and running but we haven't deployed our articles to our pages site.  What do we need to do to do this:

1. Generate our static site
2. Deploy the site using continuous build and deploy processes
3. visit the site and confirm it works

### Generate the Static Site 

We could use pandoc to generate the HTML for each of our markdown articles but this leaves us little control and doesnt provide facilities to handle different devices or table of contents etc. 

To get around this I decided to make use of [MkDocs](https://www.mkdocs.org/) which has details instructions to help you deploy a local version.

It is easy to build a site from our markdown with MkDocs.  Just copy our markdown documents into a `docs` directory, create a control YAML document and run `mkdocs build`.  The full site will be delivered in the `site` directory.

The site can be tested using `mkdocs serve` and browse to location shown on the screen.

My control document (called `mkdocs.yml`) is pretty simple

```
site_name: Andrew GOes on a Journey
#theme: readthedocs
#theme: bootstrap4
#theme: cyborg
#theme: windmill
#theme: gitbook
theme: material
repo_url: https://gitlab.acolvin.me.uk/go/bloodglucose/blood/
repo_name: https://gitlab.acolvin.me.uk/go/bloodglucose/blood/

nav:
        #        - Home: index.md
        - Home:  README.md
        - Blog 1: go_blog1.md
        - Blog 2: go_blog2.md
        - Blog 2.1: go_blog2.1.md
        - Blog 3: go_blog3.md
        - Blog 4: go_blog4.md
        - Blog 5: go_blog5.md
        - Blog 6: go_blog6.md
        - Blog 7: go_blog7.md

```


The theme I am using in my site is called material.  I set the site name and set the navigation list (if this isn't done then mkdocs picks the files up alphabetically).

## Automating the Pages Build and Deploy Process 

This will be achieved using the Gitlab CI/CD process.  The `.gitlab-ci.yml` file we created in previous articales we have a documentation stage which currently generates `PDF` files from our markdown articles.  To add a Pages job to this stage we will create a task called `pages` This is a special job and tells gitlab runner to deploy your exported artefacts to the gitlab pages.

However we first need to build the site then export the generated files. This will be achieved in a base alpine docker image where we will 

* install MkDocs and our theme 
* link our code into the correct place in the tree 
* run mkdocs build
* move site to public
* export public

Of course we only need to run this job when any of the blog files are changed, the mkdoc.yml file is changed or the README.md is changed as this is acting as our home page

```yaml

pages:
  image: python:alpine
  stage: document
  script:
  - pip install mkdocs
  - pip install mkdocs-windmill
  - pip install mkdocs-material
  - mkdir $CI_PROJECT_DIR/docs
  - cp *.md $CI_PROJECT_DIR/docs
  - ln -s $CI_PROJECT_DIR/Pictures $CI_PROJECT_DIR/docs/Pictures
  - cd $CI_PROJECT_DIR
  - mkdocs build
  - mv site public
  artifacts:
    paths:
    - public
  only:
     changes: 
        - "*blog*.md"
        - "README.md"
        - "mkdocs.yml"

```

This task will not be picked up by our project runners as it does not carry the tags we set but will be picked up by a shared runner that I have set up following the steps in the previous article but as the site admin so this runner is available for all repositories.

Now when any of the files are uploaded gitlab will trigger the build job and deploy the site.  The output of the job follows

```bash
Running with gitlab-runner 11.8.0 (4745a6f3)
  on shared SRMXYYg5
Using Docker executor with image python:alpine ...
Pulling docker image python:alpine ...
Using docker image sha256:96c5c39abbb6766b2d15c4722fa3ac8b80e8f5f90462694acca05921081ece92 for python:alpine ...
Running on runner-SRMXYYg5-project-1-concurrent-0 via f4ab565f4efe...
Fetching changes...
Removing docs/
Removing public/
HEAD is now at 32e7530 change author to authors for mkdocs as a test
From https://git.acolvin.me.uk/go/bloodglucose/blood
   32e7530..215c2ba  master     -> origin/master
Checking out 215c2bad as master...
Skipping Git submodules setup
$ mkdir -p $GOPATH/src/$(dirname $REPO_NAME)
$ ln -svf $CI_PROJECT_DIR $GOPATH/src/$REPO_NAME
'/src/gitlab.acolvin.me.uk/go/bloodglucose/blood' -> '/builds/go/bloodglucose/blood'
$ cd $GOPATH/src/$REPO_NAME

```

The runner is set up with the before_script completed (ie go paths etc.).  The following section is the start of our job

```bash
$ pip install mkdocs
Collecting mkdocs
  Downloading https://files.pythonhosted.org/packages/db/f9/b0179afee0db21943120ea606eb68bda1257b96420df74b775280eb5850b/mkdocs-1.0.4-py2.py3-none-any.whl (1.2MB)
Collecting Jinja2>=2.7.1 (from mkdocs)
  Downloading https://files.pythonhosted.org/packages/1d/e7/fd8b501e7a6dfe492a433deb7b9d833d39ca74916fa8bc63dd1a4947a671/Jinja2-2.10.1-py2.py3-none-any.whl (124kB)
Collecting PyYAML>=3.10 (from mkdocs)
  Downloading https://files.pythonhosted.org/packages/9f/2c/9417b5c774792634834e730932745bc09a7d36754ca00acf1ccd1ac2594d/PyYAML-5.1.tar.gz (274kB)
Collecting click>=3.3 (from mkdocs)
  Downloading https://files.pythonhosted.org/packages/fa/37/45185cb5abbc30d7257104c434fe0b07e5a195a6847506c074527aa599ec/Click-7.0-py2.py3-none-any.whl (81kB)
Collecting Markdown>=2.3.1 (from mkdocs)
  Downloading https://files.pythonhosted.org/packages/f5/e4/d8c18f2555add57ff21bf25af36d827145896a07607486cc79a2aea641af/Markdown-3.1-py2.py3-none-any.whl (87kB)
Collecting tornado>=5.0 (from mkdocs)
  Downloading https://files.pythonhosted.org/packages/03/3f/5f89d99fca3c0100c8cede4f53f660b126d39e0d6a1e943e95cc3ed386fb/tornado-6.0.2.tar.gz (481kB)
Collecting livereload>=2.5.1 (from mkdocs)
  Downloading https://files.pythonhosted.org/packages/ae/dd/f518bb99d84a3d26f45c281030b4d74b8cbe23bf8ad311b42e413aa33e51/livereload-2.6.0-py2.py3-none-any.whl
Collecting MarkupSafe>=0.23 (from Jinja2>=2.7.1->mkdocs)
  Downloading https://files.pythonhosted.org/packages/b9/2e/64db92e53b86efccfaea71321f597fa2e1b2bd3853d8ce658568f7a13094/MarkupSafe-1.1.1.tar.gz
Requirement already satisfied: setuptools>=36 in /usr/local/lib/python3.7/site-packages (from Markdown>=2.3.1->mkdocs) (41.0.0)
Collecting six (from livereload>=2.5.1->mkdocs)
  Downloading https://files.pythonhosted.org/packages/73/fb/00a976f728d0d1fecfe898238ce23f502a721c0ac0ecfedb80e0d88c64e9/six-1.12.0-py2.py3-none-any.whl
Building wheels for collected packages: PyYAML, tornado, MarkupSafe
  Building wheel for PyYAML (setup.py): started
  Building wheel for PyYAML (setup.py): finished with status 'done'
  Stored in directory: /root/.cache/pip/wheels/ad/56/bc/1522f864feb2a358ea6f1a92b4798d69ac783a28e80567a18b
  Building wheel for tornado (setup.py): started
  Building wheel for tornado (setup.py): finished with status 'done'
  Stored in directory: /root/.cache/pip/wheels/61/7e/7a/5e02e60dc329aef32ecf70e0425319ee7e2198c3a7cf98b4a2
  Building wheel for MarkupSafe (setup.py): started
  Building wheel for MarkupSafe (setup.py): finished with status 'done'
  Stored in directory: /root/.cache/pip/wheels/f2/aa/04/0edf07a1b8a5f5f1aed7580fffb69ce8972edc16a505916a77
Successfully built PyYAML tornado MarkupSafe
Installing collected packages: MarkupSafe, Jinja2, PyYAML, click, Markdown, tornado, six, livereload, mkdocs
Successfully installed Jinja2-2.10.1 Markdown-3.1 MarkupSafe-1.1.1 PyYAML-5.1 click-7.0 livereload-2.6.0 mkdocs-1.0.4 six-1.12.0 tornado-6.0.2

```

MkDocs is now installed in the container

```bash
$ pip install mkdocs-windmill
Collecting mkdocs-windmill
  Downloading https://files.pythonhosted.org/packages/4e/8c/35c7e7cf5774264eb6264ebacdbabd52eef6a46a5dda1ab32e156b11922d/mkdocs-windmill-1.0.0.tar.gz (977kB)
Requirement already satisfied: mkdocs in /usr/local/lib/python3.7/site-packages (from mkdocs-windmill) (1.0.4)
Requirement already satisfied: livereload>=2.5.1 in /usr/local/lib/python3.7/site-packages (from mkdocs->mkdocs-windmill) (2.6.0)
Requirement already satisfied: tornado>=5.0 in /usr/local/lib/python3.7/site-packages (from mkdocs->mkdocs-windmill) (6.0.2)
Requirement already satisfied: Markdown>=2.3.1 in /usr/local/lib/python3.7/site-packages (from mkdocs->mkdocs-windmill) (3.1)
Requirement already satisfied: PyYAML>=3.10 in /usr/local/lib/python3.7/site-packages (from mkdocs->mkdocs-windmill) (5.1)
Requirement already satisfied: click>=3.3 in /usr/local/lib/python3.7/site-packages (from mkdocs->mkdocs-windmill) (7.0)
Requirement already satisfied: Jinja2>=2.7.1 in /usr/local/lib/python3.7/site-packages (from mkdocs->mkdocs-windmill) (2.10.1)
Requirement already satisfied: six in /usr/local/lib/python3.7/site-packages (from livereload>=2.5.1->mkdocs->mkdocs-windmill) (1.12.0)
Requirement already satisfied: setuptools>=36 in /usr/local/lib/python3.7/site-packages (from Markdown>=2.3.1->mkdocs->mkdocs-windmill) (41.0.0)
Requirement already satisfied: MarkupSafe>=0.23 in /usr/local/lib/python3.7/site-packages (from Jinja2>=2.7.1->mkdocs->mkdocs-windmill) (1.1.1)
Building wheels for collected packages: mkdocs-windmill
  Building wheel for mkdocs-windmill (setup.py): started
  Building wheel for mkdocs-windmill (setup.py): finished with status 'done'
  Stored in directory: /root/.cache/pip/wheels/de/db/b0/82a099bb9b910d38da2fbbd79039022641266de7e2c79b3f28
Successfully built mkdocs-windmill
Installing collected packages: mkdocs-windmill
Successfully installed mkdocs-windmill-1.0.0
$ pip install mkdocs-material
Collecting mkdocs-material
  Downloading https://files.pythonhosted.org/packages/2a/5d/18974b95352f16217e670f3b7ea68fab633d70842f24bea9e6b2abd78d42/mkdocs_material-4.1.1-py2.py3-none-any.whl (706kB)
Collecting pymdown-extensions>=4.11 (from mkdocs-material)
  Downloading https://files.pythonhosted.org/packages/0e/9a/7a16db7ecc84cd5a12b8fa443a5ebb390baa896548701acbe94f55bed82e/pymdown_extensions-6.0-py2.py3-none-any.whl (218kB)
Collecting Pygments>=2.2 (from mkdocs-material)
  Downloading https://files.pythonhosted.org/packages/13/e5/6d710c9cf96c31ac82657bcfb441df328b22df8564d58d0c4cd62612674c/Pygments-2.3.1-py2.py3-none-any.whl (849kB)
Requirement already satisfied: mkdocs>=1 in /usr/local/lib/python3.7/site-packages (from mkdocs-material) (1.0.4)
Requirement already satisfied: Markdown>=3.0.1 in /usr/local/lib/python3.7/site-packages (from pymdown-extensions>=4.11->mkdocs-material) (3.1)
Requirement already satisfied: livereload>=2.5.1 in /usr/local/lib/python3.7/site-packages (from mkdocs>=1->mkdocs-material) (2.6.0)
Requirement already satisfied: tornado>=5.0 in /usr/local/lib/python3.7/site-packages (from mkdocs>=1->mkdocs-material) (6.0.2)
Requirement already satisfied: PyYAML>=3.10 in /usr/local/lib/python3.7/site-packages (from mkdocs>=1->mkdocs-material) (5.1)
Requirement already satisfied: click>=3.3 in /usr/local/lib/python3.7/site-packages (from mkdocs>=1->mkdocs-material) (7.0)
Requirement already satisfied: Jinja2>=2.7.1 in /usr/local/lib/python3.7/site-packages (from mkdocs>=1->mkdocs-material) (2.10.1)
Requirement already satisfied: setuptools>=36 in /usr/local/lib/python3.7/site-packages (from Markdown>=3.0.1->pymdown-extensions>=4.11->mkdocs-material) (41.0.0)
Requirement already satisfied: six in /usr/local/lib/python3.7/site-packages (from livereload>=2.5.1->mkdocs>=1->mkdocs-material) (1.12.0)
Requirement already satisfied: MarkupSafe>=0.23 in /usr/local/lib/python3.7/site-packages (from Jinja2>=2.7.1->mkdocs>=1->mkdocs-material) (1.1.1)
Installing collected packages: pymdown-extensions, Pygments, mkdocs-material
Successfully installed Pygments-2.3.1 mkdocs-material-4.1.1 pymdown-extensions-6.0

```

The themes are installed in the container.  Now move the files that are going to be used for the site into the docs folder either explicitly or using a link.

```bash
$ mkdir $CI_PROJECT_DIR/docs
$ cp *.md $CI_PROJECT_DIR/docs
$ ln -s $CI_PROJECT_DIR/Pictures $CI_PROJECT_DIR/docs/Pictures
$ cd $CI_PROJECT_DIR

```

Now build the site and move it to the public folder.  At this point the public folder is exposed and the pages deploy job takes over to deploy the site to Gitlab Pages.

```bash
$ mkdocs build
INFO    -  Cleaning site directory 
INFO    -  Building documentation to directory: /builds/go/bloodglucose/blood/site 
$ mv site public
Uploading artifacts...
public: found 91 matching files                    
Uploading artifacts to coordinator... ok            id=143 responseStatus=201 Created token=uwYd_5Mg
Job succeeded


```

## Example Pages

![](pages1.png)

![](pages2.png)

## Conclusion

Producing our site from our mark down articles has been extremely easy and if you look at the mkdoc.yml file you will see that I have tried many different themes making the final site look and behave differently.  

This approach quickly allows anyone to generate nicely published sites.

You can have a look around the site at [`http://go.pages.acolvin.me.uk/bloodglucose/blood/`](http://go.pages.acolvin.me.uk/bloodglucose/blood/).

If there is a need to speed up the production process I could easily have created my own docker image with mkdocs and the themes I required installed so that I didn't have to build it each time.

Note that as I push this to [gitlab.com](https://gitlab.com/acolvin/bloodrest) as a mirror and the build job is using a standard global shared runner gitlab's runners will pick this up and create the site and deploy it at [https://acolvin.gitlab.io/bloodrest/](https://acolvin.gitlab.io/bloodrest/) as well.


