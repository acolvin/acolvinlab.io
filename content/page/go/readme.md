---
date: 2018-12-01
author: "Andrew Colvin"
toc: false
weight: 99
title: My Journey with Go
tags: ["go", "series", "gitlab", "docker", "mkdocs", "pages", "git"]
bigimg: [{src: "img/DSCF1190.png"}]
layout: article
comments: false
next: "go_blog001.md"
---

# Blogs of my Go Journey
by {{< param author >}}

This series of blogs documents my journey into learning about and using GO from my perspective - someone who knows many programming languages and has focused on Java for the many years.

The Blog is structured into parts due to its size and the end of each blog is tagged in the repository so that you can view the state at the point in time.

All the code for these blogs can be found in the gitlab repository at https://git.acolvin.me.uk/go/bloodglucose/blood and also mirrored at https://gitlab.com/acolvin/bloodrest

## [1.]({{<ref "go_blog001.md">}}) The Journey Begins

- Understanding Go and the pitfalls I fell into as I went. 
- Struggles with conversion from class based language to functional language
- Sorting out initial and default values.

## [2.]({{<ref "go_blog002.md">}}) GOing futher...
- Extending the program 
- init functions
- playing with sets 
- Playing with unique IDs
- testing capabilities

## [2+.]({{<ref "go_blog002_1.md">}}) Squashing those pesky bugs
- Debugging

## [3.]({{<ref "go_blog003.md">}}) Depart for a REST
- Building REST
- Envirtonment variables and command line flags
- Goroutines (Multithreading)
- Channels
- Mutual Exclusivity


## [4.]({{<ref "go_blog004.md">}}) Building on both sides of the void
- Make
- Cross compiling
- building into docker

## [5.]({{<ref "go_blog005.md">}}) Jenkins GO and flow through that pipe

- Jenkins Pipelining without docker
- Added Jenkinsfile

## [6.]({{<ref "go_blog006.md">}}) Gitlab follows down the same pipe.

- Installation of your own gitlab server
- Building the pipeline
- Installing runners to drive the pump
- Collecting the outputs from the end of the pipe


## [7.]({{<ref "go_blog007.md">}}) Converting the Blogs 

- Building a static site with mkdocs
- Installing Gitlab pages
- CI/CD for mkdocs and gitlab pages

## [8.]({{<ref "go_blog008.md">}}) Supporting Large Files with Git

- GIT-LFS installation and turning it on
- Tracking amd untracking files
- Using file locks
- Removing GIT-LFS 

## [9.]({{<ref "go_blog009.md">}}) Deploying to Github Pages from Gitlab CI/CD

- Limiting to branches
- Mkdocs gh-pages
- Access control and limiting execution

## [10.]({{<ref "go_blog010.md">}}) JSON Streaming and File IO

- Return to Go
- Write Interface
- Stream the set
- Adding File IO to the Rest Service
- Stream the Set of Readings to a File on Service Closure
