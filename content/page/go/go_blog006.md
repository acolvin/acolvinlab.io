---

title: Go Blog Part 6 - Gitlab Pipelining and Go
author: Andrew Colvin
date: February 2019
toc: true
weight: 160
tags: ["go", "series", "gitlab", "docker", "pipeline"]
layout: article
previous: "go_blog005.md"
index: "readme.md"
next: "go_blog007.md"
---

# Gitlab Pipelining

In the previous blog I looked at using Jenkins to create a CI pipeline. In this article I am going to investigate using the capabilities of the built in Gitlab Pipeline functionality. 

This article will show how to easily install gitlab community edition as a docker container and a gitlab runner which is required to execute jobs on pipelines.  The reason to show how to install gitlab is so that you as the reader can easily try out Gitlab's facilities as well as the fact I needed to upgrade my dead end Gitlab installation which was on an end of line platform that couldn't be upgraded

After this we will look into creating and defining the pipeline to compile our Go program and then add an additional job that will generate PDF documents from the markdown blog post and make these available for download.

Before all of this we need to ask...

##  What is a Pipeline?

Gitlab's Pipeline is a collection of jobs that are executed either concurrently or linearly to fulfil a task. Gitlab will show you a dashboard of your projects Pipelines and their progress.  It is accessed by clicking the CI/CD link on the sidebar

![](pipeline-dashboard.png)


When you select a pipeline you can see its details


![](pipelinedetail.png)

Now you can click on a job (for example Build Compile) and review what happened

![](pipelineoutput.png)

Pipelines can be defined as simply or complex as required and full detail can be found in [the Gitlab documentation](https://docs.gitlab.com/ee/ci/pipelines.html)

## Installing Gitlab

I decided to build a new virtual server (to run on my farm) and then to install Gitlab as a docker container.  This has a few complexities in that I needed to retain access to my existing end of life Gitlab installation and also to be able to stand up test teardown until I was happy with the build.

### New Virtual Machine - openSUSE Kubic

As the majority of my servers are built using openSUSE I decided to use the `kubic` platform.  `Kubic` is openSUSE's kubernetes (k8s) certified platform which can be installed as a microOS or a full k8s admin/node configuration.

`Kubic` has an interesting concept.  It is a read only root filesystem which is update (on a rolling basis) as a single transaction and can therefore be rolled back if any upgrade fails.  It is configure using `cloud-init` .  Being read-only provides additional security in that many of the important areas of the system can not be changed except within a transaction so providing visibility of all changes made to the base system.  This reduces my system's vulnerability. 

As standard `kubic` uses `CRI-O` as its default containerisation platform.  This is the new k8s `containerd` process but for familiarity of the reader I swapped it our for docker which meant I wouldn't be treading new ground integrating Gitlab and `cri-o` in `cri-o` which is an additional unknown.

If you wish to read more about CRI-O click [here](https://cri-o.io/)
Documentation of `kubic` can be found [here](https://kubic.opensuse.org/) 

### Installing Gitlab in Docker

Because I wanted to be able to install the gitlab instance and then tear it down I decided to script its configuration and creation using bash (I suppose as it is a Go blog I should have written as in Go - but that will need to be for another time).

I started with some Variables that can be changed to suit my needs

```
# Ports that gitlab will connect to
PORTSSL=443
PORTHTTP=80
PORTSSH=22

# host network adapter to bind to
BINDIP=0.0.0.0

# internal name of the container
HOSTNAME=git.acolvin.me.uk

# where it will sotre its data on the  host
CONFIG=/srv/gitlab/config
LOGS=/srv/gitlab/logs
DATA=/srv/gitlab/data

# name of the container (this allows me to run multiple versions)
NAME=gitlab-$PORTHTTP

# external url tells gitlab how it is seen externally - leave it blank to default to hostname port 80

EXT_URL=\"https://git.acolvin.me.uk\"

if [[ ! -z "${param// }" ]]
then
  URL=\"http://$HOSTNAME:$PORTHTTP\"
else
  URL=$EXT_URL
fi

echo ext_url is $URL

```

Note that if you use https then Gitlab will not listen on port 80 unless you tell it not to do TLS (required when using SSL termination in a reverse proxy)

Create the directories needed 

```
mkdir -p $CONFIG
mkdir -p $LOGS
mkdir -p $DATA


```

Check our docker process isn't already started

```
d=$(docker ps | grep $NAME | wc -l)

if [ ! $d == 1 ] 
then


```

Start the container with our properties

```
docker run --detach \
        --hostname $HOSTNAME \
        --publish $PORTSSL:$PORTSSL --publish $PORTHTTP:$PORTHTTP --publish $PORTSSH:$PORTSSH \
        --name $NAME \
        --restart always \
        --volume $CONFIG:/etc/gitlab/ \
        --volume $LOGS:/var/log/gitlab \
        --volume $DATA:/var/opt/gitlab \
        gitlab/gitlab-ce:latest

# this should really be inspecting the health status of the container
echo waiting a minute for the container to start 
sleep 60 


```

Set the external URL and restart the container.  I could have passed this as an env variable to gitlab but I would then need to remember that every time. 

```
cd $CONFIG

docker stop $NAME
sed -i.backup "/# external_url /a external_url `echo $URL`" gitlab.rb
docker start $NAME

fi

```

Because I am using TLS offloading I had to set the following additional parameters in the config file which I did by hand when I got to the stage I wanted to do https

```
nginx['listen_port'] = 80
nginx['listen_https'] = false


```

Note that each time the docker container is restarted it runs its reconfigure process.  

Finally I set the email server settings as the docker image does not provide an `MTA`.  These setting will not be shown in this document


Nothing else is required to install Gitlab-CE.  When you first browse tot he URL you will need to set the `root` password and create yourself an account.  

### Push the Code

I then created the group and subgroup `go` and `bloodglucose` and then added the project `blood` in this subgroup.  Gitlab allows the admin to export projects from one Gitlab server and import it into another.  However, my original server was too far behind to do this so I had to transfer this manually.

Some of my repos have many branches that I want to keep so I had to follow a fairly interesting method to clone the repositories

```
mkdir blood-glucose
git clone --mirror git@gitlab.acolvin.me.uk:go/bloodglucose/blood.git blood-glucose/.git
cd blood-glucose
git config --unset core.bare
git config receive.denyCurrentBranch updateInstead
git checkout master

```

Now in this folder follwo the instructions provided by the Gitlab application about how to change the origin and push the git repository you have just extracted up to your new repository

```
git remote rename origin originalgitlab
git remote add origin git@git.acolvin.me.uk:go/bloodglucose/blood.git
git push -u origin --all
git push -u origin --tags

```

We now have a fully populated git repo uploaded to Gitlab

Pipeline processing has already started on your commit and is showing that it has failed (note this is from a different project because I have cleared this in the Go project)

![](failedcommit.png)

The reason is that by default Gitlab will try its Auto DevOps process that will run a set of default behaviours.  This process is shown below

![](autodevops.png)

Drilling into the failure issue shows the job timed out as there was no `runners` capable of running the pipeline.  This will be because we haven't installed any runners.

### Install a Runner

I am going to install a runner process in a docker container.  Gitlab has the capabilities if you join it to a k8s platform to do all of this for itself but where is the fun in that

Editing the script I add a few more variables

```
# Only ever create this number of Runners
MAXRUNNERS=6

# The location to save the runners data
RUNNERBASE=/srv/gitlab-runner

``` 

Get the number of runners installed and add one

```
n=$(docker ps -a | grep gitlab-runner | wc -l)

n=$(($n + 1))

echo creating runner number $n

```

Check we haven't reached the MAXRUNNERS allowed

```
if [ $n -gt $MAXRUNNERS ]
then
        echo "reached allowed number of runners ($MAXRUNNERS)"
        exit
fi


```

Create the Directory structure for runners and copy the our root CA certificate for use by the runners.  This is only needed because I use an internal CA

```
mkdir -p $RUNNERBASE/$n/config

if [ ! -f $RUNNERBASE/certs/ca.crt ]
then
mkdir -p $RUNNERBASE/certs
cp <path>/A_Colvin_CA_version_2.crt $RUNNERBASE/certs/ca.crt
fi


```

Now start the runner container.  Note that the docker.sock is to allow our container to start new containers (docker in docker)

```
docker run -d --name gitlab-runner-$n --restart always \
   -v $RUNNERBASE/$n/config:/etc/gitlab-runner \
   -v /var/run/docker.sock:/var/run/docker.sock \
   gitlab/gitlab-runner:latest

```

Finally copy the cert into the container in (I used the docker command for this although I could have copied it in the correct place in the folder directly)

```
# just a few seconds to ensure volumes mounted
sleep 5

# this strange docker copy does subfolders and all contents as 
# docker copy will not create intermediate folders normally
docker cp $RUNNERBASE/certs/. gitlab-runner-$n:/etc/gitlab-runner/certs

# now restart
docker restart gitlab-runner-$n

```

Running this script will create a new runner each time  up to the maximum.  

## Create the Pipeline

Gitlab's default Pipeline definition file (similar to a Jenkinsfile) is called .gitlab-ci.yml (you guessed it a YAML file) so let's get started.

On the project page there is a button "Set up CI/CD" and an editor page will open offering some templates.  

![](gitlabci.png)

Select `Go`

```
# This file is a template, and might need editing before it works on your project.
image: golang:latest

variables:
  # Please edit to your GitLab project
  REPO_NAME: gitlab.com/namespace/project

# The problem is that to be able to use go get, one needs to put
# the repository in the $GOPATH. So for example if your gitlab domain
# is gitlab.com, and that your repository is namespace/project, and
# the default GOPATH being /go, then you'd need to have your
# repository in /go/src/gitlab.com/namespace/project
# Thus, making a symbolic link corrects this.
before_script:
  - mkdir -p $GOPATH/src/$(dirname $REPO_NAME)
  - ln -svf $CI_PROJECT_DIR $GOPATH/src/$REPO_NAME
  - cd $GOPATH/src/$REPO_NAME

stages:
    - test
    - build

format:
    stage: test
    script:
      - go fmt $(go list ./... | grep -v /vendor/)
      - go vet $(go list ./... | grep -v /vendor/)
      - go test -race $(go list ./... | grep -v /vendor/)

compile:
    stage: build
    script:
      - go build -race -ldflags "-extldflags '-static'" -o $CI_PROJECT_DIR/mybinary
    artifacts:
      paths:
        - mybinary



```

Umm an explanation is in order:

1.  image: tells the runner what docker image to use to run the jobs in the pipeline
2.  The before_script will run before each job.  In this case it will create the directory structure for the go  src and link the cloned repo into the go path (if you remember we had to do this in the jenkinsfile in the previous article).
3.  Stages defines the stages of the build. Note that a job can belong to one stage only and a stage may have multiple jobs
4.  Jobs format and compile with the script to run in each job
5.  Artifacts defines the output of a job which is carried to the next or output

### Only a few tweaks

...  that is all that is needed

Point it at the project repo

```
variables:
  # Please edit to your GitLab project
  REPO_NAME: gitlab.acolvin.me.uk/go/bloodglucose/blood

```

No change to this part

```
# The problem is that to be able to use go get, one needs to put
# the repository in the $GOPATH. So for example if your gitlab domain
# is gitlab.com, and that your repository is namespace/project, and
# the default GOPATH being /go, then you'd need to have your
# repository in /go/src/gitlab.com/namespace/project
# Thus, making a symbolic link corrects this.
before_script:
  - mkdir -p $GOPATH/src/$(dirname $REPO_NAME)
  - ln -svf $CI_PROJECT_DIR $GOPATH/src/$REPO_NAME
  - cd $GOPATH/src/$REPO_NAME

```

I have added a documentation stage.  It is only creating PDFs at present but another job in this stage could generate go doc

```

stages:
    - test
    - build
    - document

```

This format job has its image defined as golang:latest.  This is different from the template as I wanted to be able to define the image needed per job.  This job is a part of the test stage and will execute before build and document.  

The extra section `only: changes: "**/*.go" means this job is only run if there are changes to the go files.  Any other file can be checked in and this will not trigger.`This `only changes` wasn't available in my old Gitlab installation and hence the upgrade to utilise the feature.   

```
format:
    image: golang:latest
    stage: test
    tags: 
      - go
    script:
      - go get ./...
      - go fmt $(go list ./... | grep -v /vendor/)
      - go vet $(go list ./... | grep -v /vendor/)
      - go test -race $(go list ./... | grep -v /vendor/)
    only:
      changes:
        - "**/*.go"
```

The compile job (build stage) again uses the golang:latest image building the bloodrest executable as a static.  You will notice I left the `-race` flag to detect race conditions in the code such as unsafe concurrent variable access.

Again this job is set to only execute on changes to Go files.  There is an additional artifacts declaration which tells the pipeline to select the files matching the paths declaration and to additionally expire them after an hour.  By default the timeout is set to a month on the base install

```
compile:
    image: golang:latest
    stage: build
    tags: 
      - go
    script:
      - go get ./...
      - go build -race -ldflags "-extldflags '-static'" -o $CI_PROJECT_DIR/bloodrest $GOPATH/src/$REPO_NAME/cmd/bloodrest/bloodrest.go
    only:
      changes:
        - "**/*.go"
    artifacts:
      paths:
        - bloodrest
      expire_in: 1 hour

```

This final job, `document`, runs in the document stage and could just as easily been defined in the `build` stage.  It uses a container built with pandoc and a full tex/latex installation (over 2GB in size!).  It selects all markdown documents in the root of the repo and runs pandoc on each to produce a PDF version of the markdown and then exports these PDF files.  This job is set to run always with `when: always`.  Note that it only compiles the blog markdown files (`*blog*.md`) but outputs as artifacts all PDF files

We could easily define a `go doc` job to generate API documentation    

```
document:
    image: ntwrkguru/pandoc-gitlab-ci:latest
    stage: document
    tags:
      - md
    script:
      - ln -svf $CI_PROJECT_DIR /source
      - find . -name "*.md" -exec pandoc  -f markdown_github -t latex {} -o {}.pdf \;
    only:
      changes: 
        - "*blog*.md"
    when: always
    artifacts:
      paths:
        - "*.pdf"
      expire_in: 1 hour
          
```

### Pipeline Still Failing 

At this stage once we have checked in our `.gitlab-ci.yml` we would expect to see our pipelines execute correctly.  However, they still complain that there is no runners assigned.  Why when we installed some runners?

Runners have to be assigned to a project.  There are several options:

- Shared Runners are across project and created by the gitlab admin
- Group Runners are defined against the group, in our case `go` or `bloodglucose` and assigned by the group owner or gitlab admin
- Assigned to the project by the project maintainer.  It is this option we will follow

This assignment is done in the project ``settings ci/cd` screen
![](settings-ci.png)

Expand runners to see the options

![](runners_config.png)

Here we can see the options explained above.  What we are going to do is assign one of our runners to this project.

This registration can be done in a single call passing through the assignment details when the container is run or register afterwards.  We are going to do the latter as we have already created our runner containers and they are running.

To achieve this we need to run the `gitlab-runner register` command and using the values specified in the instructions

```
> docker exec -it gitlab-runner-2 /bin/bash
root@b534587fef22:/# gitlab-runner register
Runtime platform                                    arch=amd64 os=linux pid=31 revision=4745a6f3 version=11.8.0
Running in system-mode.                            
                                                   
Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com/):
https://git.acolvin.me.uk/
Please enter the gitlab-ci token for this runner:
pfqysQYz3BWG3zc_KwZz
Please enter the gitlab-ci description for this runner:
[b534587fef22]: extra runner
Please enter the gitlab-ci tags for this runner (comma separated):
go
Registering runner... succeeded                     runner=pfqysQYz
Please enter the executor: kubernetes, ssh, virtualbox, docker+machine, docker-ssh+machine, docker, docker-ssh, parallels, shell:
docker
Please enter the default Docker image (e.g. ruby:2.1):
golang:latest
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded! 
root@b534587fef22:/# exit

``` 

That is it we have registered our runner against the project and the runner page shows the runner defined

![](runner_assign.png)

#### Tags

If you look back at the pipeline you will see tag definitions `tags: go` and `tags: md` This means that the runner must offer this tag to be able to execute the job.  In the registration we said this runner provides the support for tags `go` and therefore is able to process all of the jobs except document

#### Defining another Runner
 
It is necessary to register another runner to process our markdown.  Note that this doesn't need to be against a different container as a single gitlab runner container can offer any number of runners.

Execute the registration command  again and this time set the tag to `md` and use the docker image `ntwrkguru/pandoc-gitlab-ci:latest`

Almost there...  During this process I discovered a bug in the runner config process around CA certificates.  If you remember I added my root CA to the runner when we created them but it seems that this process only makes the gitlab-runner command work with this CA certificate.  The underlying git still fails as the provided CA isn't recognised as the CA isn't installed against the containers OS.  This can be fixed by copying the CA into the correct place for the container and running `update-ca-certificates` or just telling git to ignore SSL verification

The latter can be done from the runners configuration file.  This file is found on the host at the location of the runner config volume assigned during container creation `/srv/gitlab-runner/1/config` (note I am looking at runner 1 as runner 2 was only set up for the previous section and runner 1 is the fully configured runner).

```
/srv/gitlab-runner/1/config # cat config.toml 
concurrent = 1
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "runner 1 - go"
  url = "https://gitlab.acolvin.me.uk/"
  token = "8_x1pd-uHk8cN5chxiF3"
  executor = "docker"
  environment = ["GIT_SSL_NO_VERIFY=1"]
  [runners.docker]
    tls_verify = false
    image = "golang:latest"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache"]
    shm_size = 0
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]

[[runners]]
  name = "runner-1-md"
  url = "https://gitlab.acolvin.me.uk/"
  token = "79RSpMnQm18iEvQoaysx"
  environment = ["GIT_SSL_NO_VERIFY=1"]
  executor = "docker"
  [runners.docker]
    tls_verify = false
    image = "ntwrkguru/pandoc-gitlab-ci"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache"]
    shm_size = 0
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]


```


We can see in this config that we have both runners defined.  I added the line `environment = ["GIT_SSL_NO_VERIFY=1"]` to make git ignore SSL verify errors.  The runner will pick up these changes transparently.

### Example Pipeline Outputs

![](pipe_ex_dash.png)

#### Full Manual Pipeline Execution

Full execution with all stages and jobs
![](pipe_ex1_full.png)

#### Just the Documentation

This pipeline picked up just a change to the markdown and shows just the single stage and job.
![](pipe_ex2_docs.png)


##  Conclusion

Gitlab's pipeline process is very powerful and requires no complex integration between different components.  In fact a large part of this article was more about gitlab installation and configuration rather than the pipeline.  I only scratched the surface of the pipeline capabilities [full details are available](https://docs.gitlab.com/ee/ci/yaml/README.html)

I only touch the surface of creating pipelines and there abilities.  Gitlab can be set up with full CD capabilities and understands the concepts of environments as well as able to deploy to serverless environments as well as Functions as a Service using knative and k8s

