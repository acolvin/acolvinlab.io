---
title: Go Blog Part 2.1 - Debugging
author: "Andrew Colvin"
date: 2019-01-12
toc: true
weight: 115
tags: ["go", "series"]
layout: article
previous: "go_blog002.md"
index: "readme.md"
next: "go_blog003.md"
---

# Debugging GO
by {{< param author >}}

This article is a short addition to my go blog series but forms an important part of the journey as I expect it will be needed very soon.  I was posed a question: 

 > How do you debug a library or program written in Go?

We are going to explore this

## Use an IDE
Up until now I have avoided using an IDE as they do lots of things behind the scenes and I wouldn't really fully understand the detail of just how it all works.  

There is a [GoEclipse Plugin](https://github.com/GoClipse/goclipse/blob/latest/documentation/Features.md#features) with many features including a fully function interface to the gnu debugger

**However, I am a glutton for punishment ...**

## Using GDB

The **G**NU **D**e**b**ugger (gdb) is the gnu suites debugger.  It is very powerful and can run a program directly or attach to a running program.

It has modules that allows it to be extended to understand the symbol structure etc for different languages that are loaded as necessary.  Luckily Go comes with just one of these modules when you installed it.

Before we can debug our compiled program a debug version has to be created that contains the symbols (luckily go doesn't strip these by default) but also make sure all optimisations are turned off.  If we do not do this then variables that are defined and used once are likely to remain in a register and their inspection in the debugger would be unavailable. 

To accomplish no compile time optimisation pass the gcc flags -N and -l

    go build -gcflags='-N -l'

If our build process created an executable then a simple `gdb <exe>` would start gdb and away we would go

However, the code we have produced to date is just a library and we may need to use a debugger on a library when we find test scripts fail to Pass our tests.  Again the Go community thought of this and gave us `go test -c`.  This compiles the test suite into an executable but doesn't run it.  If we debug this we will find optimisations get in  the way again so it is better to build the test executable with `go test -c -gcflags='-N -l'`.  The executable is compiled to `<package>.test`

### Start the Debugger

We run our debugger on the test executable `gdb blood.test` (remember our package is called blood).  This should load the code, its symbols and be ready for us to set breakpoints etc.

```
~/go/src/blood_glucose/blood/pkg/blood> gdb blood.test 
GNU gdb (GDB; openSUSE Tumbleweed) 8.2
Copyright (C) 2018 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
Type "show copying" and "show warranty" for details.
This GDB was configured as "x86_64-suse-linux".
Type "show configuration" for configuration details.
For bug reporting instructions, please see:
<http://bugs.opensuse.org/>.
Find the GDB manual and other documentation resources online at:
    <http://www.gnu.org/software/gdb/documentation/>.

For help, type "help".
Type "apropos word" to search for commands related to "word"...
Reading symbols from blood.test...done.
warning: File "/usr/share/go/1.11/src/runtime/runtime-gdb.py" auto-loading has been declined by your `auto-load safe-path' set to "$debugdir:$datadir/auto-load".
To enable execution of this file add
        add-auto-load-safe-path /usr/share/go/1.11/src/runtime/runtime-gdb.py
line to your configuration file "/home/andrew/.gdbinit".
To completely disable this security protection add
        set auto-load safe-path /
line to your configuration file "/home/andrew/.gdbinit".
For more information about this security protection see the
"Auto-loading safe path" section in the GDB manual.  E.g., run from the shell:
        info "(gdb)Auto-loading safe path"
(gdb) 

```

So it is loaded but that is a lot of output just to start up!  Oh we have a warning

```
warning: File "/usr/share/go/1.11/src/runtime/runtime-gdb.py" auto-loading has been declined by your `auto-load safe-path' set to "$debugdir:$datadir/auto-load".
To enable execution of this file add
        add-auto-load-safe-path /usr/share/go/1.11/src/runtime/runtime-gdb.py
line to your configuration file "/home/andrew/.gdbinit".
To completely disable this security protection add
        set auto-load safe-path /
line to your configuration file "/home/andrew/.gdbinit".
For more information about this security protection see the
"Auto-loading safe path" section in the GDB manual.  E.g., run from the shell:
        info "(gdb)Auto-loading safe path"
```

This is basically saying I am not loading dynamic python code that was shipped with go to support gdb as it is not in its "safe locations".  It then tells us how to fix it.  I will take the first option of adding it to my local user

```
echo "add-auto-load-safe-path /usr/share/go/1.11/src/runtime/runtime-gdb.py" > /home/andrew/.gdbinit
```

```
gdb blood.test 
GNU gdb (GDB; openSUSE Tumbleweed) 8.2
Copyright (C) 2018 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
Type "show copying" and "show warranty" for details.
This GDB was configured as "x86_64-suse-linux".
Type "show configuration" for configuration details.
For bug reporting instructions, please see:
<http://bugs.opensuse.org/>.
Find the GDB manual and other documentation resources online at:
    <http://www.gnu.org/software/gdb/documentation/>.

For help, type "help".
Type "apropos word" to search for commands related to "word"...
Reading symbols from blood.test...done.
Loading Go Runtime support.
(gdb) 

```

As you can see the Go Runtime Support is now loaded

### Set a Breakpoint 

We can set a breakpoint using filename and line number 

```
(gdb) b BGReading_test.go:19
Breakpoint 1 at 0x52cb20: file /home/andrew/go/src/blood_glucose/blood/pkg/blood/BGReading_test.go, line 19.
(gdb) 

```

Now execute the program up to our breakpoint

```
(gdb) run
Starting program: /home/andrew/go/src/blood_glucose/blood/pkg/blood/blood.test 
[New LWP 30818]
[New LWP 30819]
[New LWP 30820]
[New LWP 30821]

Thread 1 "blood.test" hit Breakpoint 1, blood_glucose/blood/pkg/blood.TestConvertMMOL (t=0xc0000ae100) at /home/andrew/go/src/blood_glucose/blood/pkg/blood/BGReading_test.go:19
19      func TestConvertMMOL(t *testing.T) {
(gdb) 

```

Let's have a look at the source

```
(gdb) l
14            {3.3, 60.0},
15            {5.0,90.0},
16        }
17
18
19      func TestConvertMMOL(t *testing.T) {
20        for _,c :=range cases {
21            got := ConvertMMOL2MGDL(c.mmol)
22            diff := got - c.mgdl
23            if diff < 0 {

```

If we wanted to set the breakpoint by function name then we would need to provide the full path from the go src.  Looking above at the breakpoint definition this is `blood_glucose/blood/pkg/blood.TestConvertMMOL` 

By default we are only showing 10 lines with the list command.  To show more set the value of listsize to the required amount 

```
(gdb) set listsize 30
(gdb) l 19
4       import "time"
5       //import "fmt"
6
7
8       type bgcase struct {
9           mmol, mgdl float64
10      }
11
12      var cases = []bgcase {
13            {0.0,0.0},
14            {3.3, 60.0},
15            {5.0,90.0},
16        }
17
18
19      func TestConvertMMOL(t *testing.T) {
20        for _,c :=range cases {
21            got := ConvertMMOL2MGDL(c.mmol)
22            diff := got - c.mgdl
23            if diff < 0 {
24                diff *=-1
25            }
26            if diff > 1.0 {
27                t.Errorf("ConvertMMOL2MGDL(%f) == %f, want %f", c.mmol, got, c.mgdl)
28            }
29        }
30      }
31
32      func TestConvertMG(t *testing.T) {
33        for _,c :=range cases {
(gdb) 

```

Hitting `list` again will show the next 30 lines

```
(gdb) l
34            got := ConvertMGDL2MMOL(c.mgdl)
35            diff := got - c.mmol
36            if diff < 0 {
37                diff *=-1
38            }
39            if diff > 0.1 {
40                t.Errorf("ConvertMGDL2MMOL(%f) == %f, want %f", c.mgdl, got, c.mmol)
41            }
42        }
43      }
44
45      func TestSetReading(t *testing.T) {
46          var bg1 BGReading = BGReading{}
47          t.Logf("bg1.reading = %f \n",bg1.SetBGReading(MMOL,5.0))
48          //t.Logf("Reading is set to %f on struct %p\n", bg1.Reading, bg1)
49          var bg2 BGReading = BGReading{}
50          t.Logf("bg2.reading = %f \n",bg2.SetBGReading(MGDL,90.0))
51          //t.Logf("Reading is set to %f on struct %p\n", bg2.Reading, bg2)
52          diff:=bg2.Reading - bg1.Reading
53          if diff < 0 {
54              diff=diff*-1.0
55          }
56          if diff>0.1 {
57              t.Errorf("setting failed %f!=%f",bg1.Reading ,bg2.Reading )
58          }
59      }
60
61      //func TestSetReadingandDate(t *testing.T) {
62      //    d:=time.Now()
63      //    var bg1 *BGReading = &BGReading{}
(gdb) 

```

The program is stopped at line 19 waiting for us to continue.  Let's step the program on.  There are two options: 
- step --- Steps the program one line entering function calls
- next --- Executes a line stepping over any function calls

This is the code we are walking through

```
19      func TestConvertMMOL(t *testing.T) {
20        for _,c :=range cases {
21            got := ConvertMMOL2MGDL(c.mmol)
22            diff := got - c.mgdl
23            if diff < 0 {
24                diff *=-1
25            }
26            if diff > 1.0 {
27                t.Errorf("ConvertMMOL2MGDL(%f) == %f, want %f", c.mmol, got, c.mgdl)
28            }
29        }
30      }

```

Let's step one line

```
(gdb) s
20        for _,c :=range cases {
(gdb) 
```

This runs the lines and sets variable `c` to the value of the next item in the cases.

```
(gdb) p c
$2 = {mmol = 3.2547977566226305e-317, mgdl = 7.4109846876186982e-323}
(gdb) 
```

or  to get information about all local variables

```
(gdb) info locals
c = {mmol = 3.2547977566226305e-317, mgdl = 7.4109846876186982e-323}
(gdb)

```

Stepping again 

```
(gdb) s
21            got := ConvertMMOL2MGDL(c.mmol)
(gdb) p c.mmol
$3 = 0
(gdb) info locals
diff = 2.4561599086804433e-317
got = 4.0742333448429788e-312
c = {mmol = 0, mgdl = 0}
(gdb) 

```

And again, a few times, as we are stepping we go into the Convert function

```
(gdb) s
blood_glucose/blood/pkg/blood.ConvertMMOL2MGDL (mmol=0, ~r1=7.4109846876186982e-323) at /home/andrew/go/src/blood_glucose/blood/pkg/blood/BGReading.go:66
66      func ConvertMMOL2MGDL(mmol float64) float64 {
(gdb) s
67          return mmol*18.016
(gdb) 
blood_glucose/blood/pkg/blood.TestConvertMMOL (t=0xc0000ae100) at /home/andrew/go/src/blood_glucose/blood/pkg/blood/BGReading_test.go:22
22            diff := got - c.mgdl
(gdb)
23            if diff < 0 {

```

Our local values are now


```
(gdb) info locals
diff = 0
got = 0
c = {mmol = 0, mgdl = 0}
(gdb) 

```

To step out of a function we use the `fin` command which will finish this function.  With the test framework this will take us to the end.

```

(gdb) fin
Run till exit from #0  blood_glucose/blood/pkg/blood.TestConvertMMOL (t=0xc0000ae100) at /home/andrew/go/src/blood_glucose/blood/pkg/blood/BGReading_test.go:26
testing.tRunner (fn={void (testing.T *)} 0xc0000467d8, t=0xc0000ae100) at /usr/lib64/go/1.11/src/testing/testing.go:830
830             t.finished = true
(gdb) n
831     }
(gdb) n
PASS
[LWP 30821 exited]
[LWP 30820 exited]
[LWP 30819 exited]
[LWP 30818 exited]
[Inferior 1 (process 30814) exited normally]

```


In this case this is not very helpful.  This occurred as each test runs on its own execution thread in the test framework and can be run concurrently or sequentially.  If we had added breakpoints in the other functions we would have stopped.

To view the different execution commands for gdb this page is a great read [gdb command explanation](https://sourceware.org/gdb/current/onlinedocs/gdb/Continuing-and-Stepping.html#Continuing-and-Stepping) 


## Delve
[Delve](https://github.com/go-delve/delve) is a software package written in Go that wraps gdb and makes for a much more friendly experience debugging Go programs than using gdb.  This software as stated is written in Go and as we have a function Go environment we can download and build it 

```
go get -u github.com/go-delve/delve/cmd/dlv

```

This will install and build delve placing the `dlv` command go bin directory (`~/go/bin`) .  To make this available in my shell I will add this directory to my Path `PATH=$PATH:~/go/bin`.  We can now run `dlv` to ensure it is available

```
~/go/src/blood_glucose/blood> dlv
Delve is a source level debugger for Go programs.

Delve enables you to interact with your program by controlling the execution of the process,
evaluating variables, and providing information of thread / goroutine state, CPU register state and more.

The goal of this tool is to provide a simple yet powerful interface for debugging Go programs.

Pass flags to the program you are debugging using `--`, for example:

`dlv exec ./hello -- server --config conf/config.toml`

Usage:
  dlv [command]

Available Commands:
  attach      Attach to running process and begin debugging.
  connect     Connect to a headless debug server.
  core        Examine a core dump.
  debug       Compile and begin debugging main package in current directory, or the package specified.
  exec        Execute a precompiled binary, and begin a debug session.
  help        Help about any command
  run         Deprecated command. Use 'debug' instead.
  test        Compile test binary and begin debugging program.
  trace       Compile and begin tracing program.
  version     Prints version.

Flags:
      --accept-multiclient   Allows a headless server to accept multiple client connections. Note that the server API is not reentrant and clients will have to coordinate.
      --api-version int      Selects API version when headless. (default 1)
      --backend string       Backend selection:
        default         Uses lldb on macOS, native everywhere else.
        native          Native backend.
        lldb            Uses lldb-server or debugserver.
        rr              Uses mozilla rr (https://github.com/mozilla/rr).
 (default "default")
      --build-flags string   Build flags, to be passed to the compiler.
      --headless             Run debug server only, in headless mode.
      --init string          Init file, executed by the terminal client.
  -l, --listen string        Debugging server listen address. (default "localhost:0")
      --log                  Enable debugging server logging.
      --log-output string    Comma separated list of components that should produce debug output, possible values:
        debugger        Log debugger commands
        gdbwire         Log connection to gdbserial backend
        lldbout         Copy output from debugserver/lldb to standard output
        debuglineerr    Log recoverable errors reading .debug_line
        rpc             Log all RPC messages
        fncall          Log function call protocol
        minidump        Log minidump loading
Defaults to "debugger" when logging is enabled with --log.
      --wd string            Working directory for running the program. (default ".")

Use "dlv [command] --help" for more information about a command.


```


To run our test script through the debugger we use

```

~/go/src/blood_glucose/blood/pkg/blood> dlv test
Type 'help' for list of commands.
(dlv) 


```

Adding a breakpoint to the same function as before use

```
(dlv) b TestConvertMMOL
Breakpoint 1 set at 0x56e26b for blood_glucose/blood/pkg/blood.TestConvertMMOL() ./BGReading_test.go:19
(dlv)
```

This is much easier than adding the full path as we did with `gdb`
Let us now continue the execution up to the breakpoint we have just created

```
(dlv) c
> blood_glucose/blood/pkg/blood.TestConvertMMOL() ./BGReading_test.go:19 (hits goroutine(5):1 total:1) (PC: 0x56e26b)
    14:       {3.3, 60.0},
    15:       {5.0,90.0},
    16:   }
    17:
    18:
=>  19: func TestConvertMMOL(t *testing.T) {
    20:   for _,c :=range cases {
    21:       got := ConvertMMOL2MGDL(c.mmol)
    22:       diff := got - c.mgdl
    23:       if diff < 0 {
    24:           diff *=-1
(dlv) 

```

Stepping through

```
(dlv) s
> blood_glucose/blood/pkg/blood.TestConvertMMOL() ./BGReading_test.go:20 (PC: 0x56e282)
    15:       {5.0,90.0},
    16:   }
    17:
    18:
    19: func TestConvertMMOL(t *testing.T) {
=>  20:   for _,c :=range cases {
    21:       got := ConvertMMOL2MGDL(c.mmol)
    22:       diff := got - c.mgdl
    23:       if diff < 0 {
    24:           diff *=-1
    25:       }
(dlv) s
> blood_glucose/blood/pkg/blood.TestConvertMMOL() ./BGReading_test.go:21 (PC: 0x56e30d)
    16:   }
    17:
    18:
    19: func TestConvertMMOL(t *testing.T) {
    20:   for _,c :=range cases {
=>  21:       got := ConvertMMOL2MGDL(c.mmol)
    22:       diff := got - c.mgdl
    23:       if diff < 0 {
    24:           diff *=-1
    25:       }
    26:       if diff > 1.0 {

```

The `locals` command prints out the local variables to the function

```

(dlv) locals
c = blood_glucose/blood/pkg/blood.bgcase {mmol: 0, mgdl: 0}
got = 0.0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000040742333752
(dlv)

```

We also have other commands which you can view their description by using the command `help` but it provides `next, step-out, continue, threads` among others.

### Integrating Delve into your Editors

Delve has a number of integrations to make it available in editors such as `Atom`, `VIM`, `VS Code` so if you do not, or are uncomfortable with, the command line then you can use these integrations.  Follow the link on the Delve Github site.

### A direct Delve Gui

Someone on Github has created and made available a [GUI](https://github.com/aarzilli/gdlv) that wraps delve further into a simple GUI

![gldv](gdlv.png)

## Conclusion

There are various approaches to debugging Go programs with delve being a nice compromise between a full blown IDE and integration into an editor for simple 

Hope this helps you set up your development toolset and accelerates your Go experience
