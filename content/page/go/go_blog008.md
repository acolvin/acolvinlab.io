---
title: Go Blog Part 8 - Large File Handling with Git
authors: Andrew Colvin
date: April 2019
toc: true
weight: 180
tags: ["go", "series", "gitlab",  "git"]
layout: article
previous: "go_blog007.md"
index: "readme.md"
next: "go_blog009.md"
---


# Large File Handling with Git

## Introduction

Git is a distributed version control system.  This provides  a great resource for support for writing code and the complete management of that code with its ability to clone, branch and merge the code so that changes can be built in parallel and brought together to build releases or to revert the changes to go to a previous state.  

This is good but what about binary files.  For example, writing these blogs, which are being completed using mark down, as you can see in the referenced repository, the screenshots, pictures, PDF formatted files, ODT files etc. are all binary format so cannot be version controlled in the same way as source code;  when a picture is updated a complete replacement copy is added to the repository.  

These additional copies are not necessarily an issue for non-distributed version controlled systems but with a distributed version all copies are downloaded on clone and fetch causing a large amount of network, storage and time to accomplish the cloning as well as any push and merge.

Recognising this issue the community have developed `git-lfs` as an extension to git to allow it to handle large file storage; its reference site is [here](https://git-lfs.github.com/).  It also explains in detail how it works; put simply it creates a filter against an object or groups of objects which git them hands off to during its operation.  This allows the repository to hold just a pointer to the files in some off-repository site which it downloads and uploads appropriate versions through the lifecycle.

## Enable LFS Features on Gitlab Community Edition

Before I enable git-lfs on our repository it makes sense to ensure that my private gitlab-ce server is able to support the git-lfs extension.  

Gitlab has LFS support built in and even enabled in the repository by default.  However, I want to ensure that the large files are stored in the location exposed to my server outside the docker container running gitlab so that I can externally back up my data each night.  The change to the configuration required is  simply

```ruby
### Git LFS 
gitlab_rails['lfs_enabled'] = true 
gitlab_rails['lfs_storage_path'] = "/var/lfs" 
gitlab_rails['lfs_object_store_enabled'] = true

```

I now have to import my filesystem into the docker container running my gitlab server.  I therefore change the gitlab docker definition script to add the additional import

```shell
#!/bin/bash
...
LFS=/srv/gitlab/lfs

...
docker run --detach \
         --hostname $HOSTNAME \
         --publish $PORTSSL:$PORTSSL --publish $BINDIP:$PORTHTTP:$PORTHTTP  
         --publish $BINDIP:$PORTSSH:$PORTSSH --publish $PAGESIP:$PAGESPORT:88 \        
          --name $NAME \         
          --restart always \         
          --volume $CONFIG:/etc/gitlab/ \         
          --volume $LOGS:/var/log/gitlab \         
          --volume $DATA:/var/opt/gitlab \         
          --volume $LFS:/var/lfs \         
          gitlab/gitlab-ce:latest
...

```

Gitlab has many more options to support Large File Support including remote object stores in public clouds and other options. For instructions see [here](https://docs.gitlab.com/ee/workflow/lfs/lfs_administration.html#storing-lfs-objects-in-remote-object-storage)

> Note that the public Gitlab and Github offerings both have LFS enabled.


## Configuring our Client

### GIT-LFS installation

To utilise LFS within the local repository on your desktop it is necessary to install the git-lfs extension and then turn it on in the repository.

Installing the extension on the desktop depends very much on the client you are using.  For example git tower and github desktop have it built in so there is nothing to do.  For my Linux desktop using git on the command line (seeking hero status -- bought up with command line so just as comfortable with it as I am with a GUI). I can either use the package manager or compile it ourselves.  This turns out to be easy as it is written in `go`. 

First thing to do is download the code so execute the following from our go src directory

```
go get github.com/git-lfs/git-lfs

```

Change directory into the source directory and execute `make`.  All things we have covered in previous articles.  

Details of other platforms are [here](https://github.com/git-lfs/git-lfs/wiki/Installation)

### Enabling GIT-LFS in the Repository

Now that we have `git-lfs` running on the server as well as `git-lfs` installed on the desktop we need to turn it on.  There are two options:

- _for all my repositories:_ run `git lfs install`
- _for only a single repository:_ run `git lfs install --local` from inside a repository

### Using LFS

In these next sections we are going to set up a new repository, add binary files, configure LFS, add more files and then show the ability of LFS to perform repository based file level locking in case your workflow requires single editor access at a time.

#### Set up new test repo

We start by creating a new repository in the gitlab server (you can use the public gitlab or github).

![](test-repo.png)

Now clone this to my file system

```shell
andrew@host:~/git> git clone git@git.acolvin.me.uk:andrew/test-lfs.git
Cloning into 'test-lfs'...
warning: You appear to have cloned an empty repository.

andrew@host:~/git> cd test-lfs

andrew@host:~/git/test-lfs> touch README.md

andrew@host:~/git/test-lfs> git add README.md

andrew@host:~/git/test-lfs> git commit -m "add README"
[master (root-commit) 116ce42] add README
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 README.md

andrew@host:~/git/test-lfs> git push -u origin master
Enumerating objects: 3, done.
Counting objects: 100% (3/3), done.
Writing objects: 100% (3/3), 217 bytes | 217.00 KiB/s, done.
Total 3 (delta 0), reused 0 (delta 0)
To git.acolvin.me.uk:andrew/test-lfs.git
 * [new branch]      master -> master
Branch 'master' set up to track remote branch 'master' from 'origin'.


```



#### Add Files

Now add some files into the repository: both binary and non-binary

```shell 
andrew@host:~/git/test-lfs> cp ~/go/src/gitlab.acolvin.me.uk/go/bloodglucose/blood/go_blog4.* .

andrew@host:~/git/test-lfs> ls
go_blog4.md  go_blog4.odt  go_blog4.pdf  README.md

andrew@host:~/git/test-lfs> git add .
andrew@host:~/git/test-lfs> git commit -m "Add binary and text files"
[master e02ab3a] Add binary and text files
 3 files changed, 1099 insertions(+)
 create mode 100644 go_blog4.md
 create mode 100644 go_blog4.odt
 create mode 100644 go_blog4.pdf

andrew@host:~/git/test-lfs> git push
Enumerating objects: 6, done.
Counting objects: 100% (6/6), done.
Delta compression using up to 8 threads
Compressing objects: 100% (5/5), done.
Writing objects: 100% (5/5), 217.26 KiB | 24.14 MiB/s, done.
Total 5 (delta 0), reused 0 (delta 0)
To git.acolvin.me.uk:andrew/test-lfs.git
   116ce42..e02ab3a  master -> master


```

![](test-repo2.png)

#### Turn on LFS

Let us turn on LFS using the local command 

```shell
andrew@host:~/git/test-lfs> git lfs ls-files

andrew@host:~/git/test-lfs> git lfs install --local
Updated git hooks.
Git LFS initialized.

andrew@host:~/git/test-lfs> git lfs ls-files

```

The `git lfs ls-files` command shows the files that are under the control of the LFS lifecycle.  Note that even after the initiation no files are taken under its wing.

To do this we need to tell `git` which files to track

#### Track Files

Let us start by tracking the `pdf` files

```shell
andrew@host:~/git/test-lfs> git lfs track *.pdf
Tracking "go_blog4.pdf"
andrew@host:~/git/test-lfs> git push
Everything up-to-date
andrew@host:~/git/test-lfs> git lfs ls-files
andrew@host:~/git/test-lfs> 


```

Oh it is tracking but it doesn't list the file!  Let us add a new PDF and see what happens

```shell
andrew@host:~/git/test-lfs> cp ~/go/src/gitlab.acolvin.me.uk/go/bloodglucose/blood/go_blog3.pdf .
andrew@host:~/git/test-lfs> ls
go_blog3.pdf  go_blog4.md  go_blog4.odt  go_blog4.pdf  README.md
andrew@host:~/git/test-lfs> git status
On branch master
Your branch is up to date with 'origin/master'.

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

        modified:   go_blog4.pdf

Untracked files:
  (use "git add <file>..." to include in what will be committed)

        .gitattributes
        go_blog3.pdf

no changes added to commit (use "git add" and/or "git commit -a")
andrew@host:~/git/test-lfs> 


```

Hold on we have a `.gitattributes` file!  What is this?

```
andrew@host:~/git/test-lfs> more .gitattributes 
go_blog4.pdf filter=lfs diff=lfs merge=lfs -text
andrew@host:~/git/test-lfs>

```

It shows our tracking information.  Hold on again, didn't we say `*.pdf` not just a single file - well yes we did but the shell globbing intercepted the `*.pdf` so let's reapply the command escaping the globbing

```shell
andrew@host:~/git/test-lfs> git lfs track '*.pdf'
Tracking "*.pdf"
andrew@host:~/git/test-lfs> more .gitattributes 
go_blog4.pdf filter=lfs diff=lfs merge=lfs -text
*.pdf filter=lfs diff=lfs merge=lfs -text

``` 

Now we can commit the changes and push them to Gitlab

```shell
andrew@host:~/git/test-lfs> git add .
andrew@host:~/git/test-lfs> git commit -m "lfs tracking"
[master 24db550] lfs tracking
 3 files changed, 5 insertions(+)
 create mode 100644 .gitattributes
 create mode 100644 go_blog3.pdf
 rewrite go_blog4.pdf (100%)
andrew@host:~/git/test-lfs> git push
Locking support detected on remote "origin". Consider enabling it with:
  $ git config lfs.https://git.acolvin.me.uk/andrew/test-lfs.git/info/lfs.locksverify true
Uploading LFS objects: 100% (2/2), 406 KB | 0 B/s, done                                                                                                                                                                                               
Enumerating objects: 7, done.
Counting objects: 100% (7/7), done.
Delta compression using up to 8 threads
Compressing objects: 100% (5/5), done.
Writing objects: 100% (5/5), 686 bytes | 686.00 KiB/s, done.
Total 5 (delta 0), reused 0 (delta 0)
To git.acolvin.me.uk:andrew/test-lfs.git
   e02ab3a..24db550  master -> master
andrew@host:~/git/test-lfs> git lfs ls-files
bb893bd217 * go_blog3.pdf
69e316d68f * go_blog4.pdf
andrew@host:~/git/test-lfs> 

```

The `git lfs ls-files` now shows that both pdf files are under the git-lfs workflow.  Gitlab now shows that our PDF files are stored in the LFS storage.

![](test-repo3.png)

>
Note that I am using a very recent version of git and git-lfs as well as gitlab so the migration to lfs is transparent.  Older versions may require you to run the `git lfs migrate import --fixup` command to move files into lfs support if they were already in the repository.

 

#### Make Lockable

`PDF` files are generally not editable but what about the `ODT` file.  This is a opendocument file (similar to `docx` for you Microsoftees) and of course multiple people can edit this.  As `ODT` is binary format (actually internally it is a zipped `XML` format and there is a flat unzipped format that can be directly merged and managed by git but for this example treat `ODT` as a binary) we may want to extend our workflow to provide file locking.  

If you noticed there was a some output in the block above telling us that the external repository supports `LFS` locking and how to switch it on.

```
Locking support detected on remote "origin". Consider enabling it with:
  $ git config lfs.https://git.acolvin.me.uk/andrew/test-lfs.git/info/lfs.locksverify true
```

Running the command gives no output but tells git to check files are not locked before allowing editing/pushing.  The command `git lfs locks` shows us which files are locked.  

Now let us add the `ODT` files to the `LFS` workflow and define them as lockable

```shell
andrew@host:~/git/test-lfs> git lfs track --lockable "*.odt"
Tracking "*.odt"

andrew@host:~/git/test-lfs> more .gitattributes 
go_blog4.pdf filter=lfs diff=lfs merge=lfs -text
*.pdf filter=lfs diff=lfs merge=lfs -text
*.odt filter=lfs diff=lfs merge=lfs -text lockable
```  

You can see now the `.gitattributes` states that `*.odt` is tracked for `lfs` and is lockable.

We need to commit and push the changes

```shell
andrew@host:~/git/test-lfs> git add .
andrew@host:~/git/test-lfs> git status
On branch master
Your branch is up to date with 'origin/master'.

Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

        modified:   .gitattributes
        modified:   go_blog4.odt

andrew@host:~/git/test-lfs> git commit -m "lockable odt"
[master 0bff72b] lockable odt
 2 files changed, 1 insertion(+)
 rewrite go_blog4.odt (100%)

andrew@host:~/git/test-lfs> git push
Uploading LFS objects: 100% (1/1), 30 KB | 0 B/s, done                                                                                                                                                                                                
Enumerating objects: 7, done.
Counting objects: 100% (7/7), done.
Delta compression using up to 8 threads
Compressing objects: 100% (4/4), done.
Writing objects: 100% (4/4), 462 bytes | 462.00 KiB/s, done.
Total 4 (delta 1), reused 0 (delta 0)
To git.acolvin.me.uk:andrew/test-lfs.git
   24db550..0bff72b  master -> master


```



#### Show Lock and Unlock

Now we have lockable files we need to understand how to lock and unlock files.

Let's start by looking at what the current lock status is for the files in the repository

```
andrew@host:~/git/test-lfs> git lfs locks
andrew@host:~/git/test-lfs>

```

None as expected.  This means that the file should be read only because we haven't acquired the lock.

```
andrew@host:~/git/test-lfs> ls -l *.odt
-r--r--r-- 1 andrew users 29823 May  2 08:02 go_blog4.odt


```

The file permissions demonstrate that the `odt` file is read only.  To take out the lock use 

```
andrew@host:~/git/test-lfs> git lfs lock go_blog4.odt
Locked go_blog4.odt

andrew@host:~/git/test-lfs> git lfs locks
go_blog4.odt    Andrew Colvin   ID:12

andrew@host:~/git/test-lfs> ls -l *.odt
-rw-r--r-- 1 andrew users 29823 May  2 08:02 go_blog4.odt


```

So we have grabbed the lock and the file now has read and write permissions and `git lfs locks` show I have the lock as expected.

To release the lock us `git lfs unlock go_blog4.odt`

#### Migrate to and from LFS

If we wish to remove tracking for a file then we can use `git lfs untrack`

```
andrew@host:~/git/test-lfs> git lfs untrack "*.odt"
Untracking "*.odt"

andrew@host:~/git/test-lfs> more .gitattributes 
go_blog4.pdf filter=lfs diff=lfs merge=lfs -text
*.pdf filter=lfs diff=lfs merge=lfs -text

andrew@host:~/git/test-lfs> git status .
On branch master
Your branch is up to date with 'origin/master'.

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

        modified:   .gitattributes
        modified:   go_blog4.odt

no changes added to commit (use "git add" and/or "git commit -a")
andrew@host:~/git/test-lfs> git add .
andrew@host:~/git/test-lfs> git commit -m "untrack odt"
[master b19fd69] untrack odt
 2 files changed, 1 deletion(-)
 rewrite go_blog4.odt (100%)
andrew@host:~/git/test-lfs> git push
Enumerating objects: 7, done.
Counting objects: 100% (7/7), done.
Delta compression using up to 8 threads
Compressing objects: 100% (4/4), done.
Writing objects: 100% (4/4), 27.30 KiB | 27.30 MiB/s, done.
Total 4 (delta 2), reused 0 (delta 0)
To git.acolvin.me.uk:andrew/test-lfs.git
   0bff72b..b19fd69  master -> master
andrew@host:~/git/test-lfs> git lfs ls-files
bb893bd217 * go_blog3.pdf
69e316d68f * go_blog4.pdf



```


Not all files will be converted straight off so it is often useful to run `git add --renormalize .` to get the repository in the state required.

To remove `LFS` from the repository and set it back is not as straight forward as setting it up.  The process is basically to untrack all files.  Add with a `--renormalize` and commit and push and then you can run `git lfs uninstall (--local)`  

```
andrew@host:~/git/test-lfs> git lfs untrack "*.pdf"
Untracking "*.pdf"

andrew@host:~/git/test-lfs> git status
On branch master
Your branch is up to date with 'origin/master'.

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

        modified:   .gitattributes

no changes added to commit (use "git add" and/or "git commit -a")

andrew@host:~/git/test-lfs> git add --renormalize .

andrew@host:~/git/test-lfs> git status
On branch master
Your branch is up to date with 'origin/master'.

Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

        modified:   .gitattributes
        modified:   go_blog3.pdf
        modified:   go_blog4.pdf

andrew@host:~/git/test-lfs> git lfs uninstall --local


```

## Conclusion  

`git-lfs` is an extremely useful tool to add to your git armoury.  It provides the administrators to have large binary files stored on servers that are high throughput file servers and adds a versioning workflow around them.  `git-lfs` is transparent in use once set up in the repository and makes for a very easy usecase for the end user and speeds up repository interactions for the user as they are not downloading all versions of the large files and therefore also reduces your file server performance requirements. 

The capability to lock files is useful for the repository administrator to lock files that shouldn't be changed (btw non lfs tracked files can also be locked) such as `.gitignore` or `license.md` as well as allowing an author to take temporary ownership of a binary formatted file for editing. 

If you have wordprocessors that are capable of saving the files flat text files such as Word with its open XML Format or libreoffice with its flat ODT format then it is far better to use these formats in git and get full versioning.

I would recommend you turn on `git-lfs` globally in your git's global settings and make use of the functionality whenever you use large binary formatted files in a git repository that is likely to have multiple versions.
