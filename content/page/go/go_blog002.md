---
title: Go Blog Part 2 - Moving Along in Time and by Number
author: Andrew Colvin
date: January 2019
toc: true
weight: 110
tags: ["go", "series", "git"]
layout: article
previous: "go_blog001.md"
index: "readme.md"
next: "go_blog002_1.md"
---

# Go Blog Part 2 – My January Project
by **Andrew Colvin**



In part one of this blog I investigated the Go programming language and
started to convert the data model from a Java application I wrote. This
was undertaken to learn Go and also express its usage from the
perspective of someone that has been programming using object oriented
languages for the last decade and returning to a strongly typed
functional library

In the last part we created a basic library function to manage Blood
Glucose Readings adding them to a set of ordered readings. In this part
I am going to depart from the original desktop approach and build a REST
service wrapper around this library to show how easy it is to produce
REST services using Go. Additionally we explored the Go file system
structure and how it links into git as part of the standard build and
dependency approach.

The layout of this article will be structured as:

  - Change our library to be more suitable for use with a REST services
    
      - Ensure times are minute bounded and are queried to ignore
        timezone
      - Initialise the set on start up 
      - Add Reading Identifiers
      - Create, Find, Update, Delete by ID
      - Return by Copy to protect our library

  - Create Basic REST Service Project with test harness

  - Simple Concurrency

  - Create POST Method to create a BGReading

  - Create Get Method to find one or all Readings by ID

  - Create DELETE Method to delete by ID

  - Create PUT Method to update record by ID

## Restructure the directory area

The first thing to do is to restructure our project to hold the
libraries that we will eventually have, at present this is the blood
library, and create an area for us to build our REST service. 

In part one we added the code directly in the root of our blood git
area. There is a convention that libraries should be added to a pkg
area. 

1.  `mkdir -p ~/go/src/gitlab.acolvin.me.uk/go/blood\_glucose/blood/pkg/blood`

2.  Move the blood package files created in part one into this folder

3.  The library is henceforth imported using the path  
    `gitlab.acolvin.me.uk/go/blood_glucose/blood/pkg/blood`

4.  We now need a directory to hold our REST Service. This is going to
    be a standalone command so I am going to build it in the cmd folder.
    
    `mkdir ~/go/src/gitlab.acolvin.me.uk/go/blood_glucose/blood/cmd`

5.  Now create a directory to hold our Blood Rest Service code. `mkdir
    ~/go/src/gitlab.acolvin.me.uk/go/blood_glucose/blood/cmd/bloodrest`

Let’s commit our changes to git:

Running git status will show a bunch of deleted files as we have moved
our files. 

So lets add the files again: `git add pkg/blood/*`

Commit these changes: `git commit -m "add comment"`

Not that empty directories cannot be pushed to git so add a `README.md`
file to describe the folder purpose in `pkg` and also in `cmd` and `bloodrest`

Note that I have also added my dxc github area as a push remote to my
repository and I am pushing the code there so you can follow and access
it, fork it and run it yourself. Note that if you fork it please change
the import from `glitlab.acolvin.me.uk/go/blood_glucose/blood` to
`github.dxc.com/acolvin/go-blood`. I also had to remove the
group/subgroups as github does not have this capability. The project is
therefore available at
[*https://github.dxc.com/acolvin/go-blood.git*](https://github.dxc.com/acolvin/go-blood.git)
or using ssh *`git@github.dxc.com:acolvin/go-blood.git`* as an remote

## Change the library

The blood package that we created in go blog 1 focussed on the code and
understanding the basics of GO and converting something existing. What
we are about to do is make this so that it can be used as it was or
built into a REST service. 

The current Readings in our set are keyed off of the date value. Passing
the date value as an identifier in a REST call is not the most friendly
position. We are therefore going to add an ID field to each BGReading
and write functions to manage our set through these IDs. 

### Changing BGReading
Whilst we are putting the ID field into the BGReading we will also
change it so that we truncate all times to be at the minute boundary as
we are not interested in seconds nor nano seconds. I am still going to
ignore the timezone. 

The first thing is to add the ID field to the BGReading. I will
add this to the struct as

>`ID string`

As it is capitalized it is public and will be exported as part of the
JSON conversion so it can be used by our REST service.

How to populate this is an interesting question. We could just use a
counter with a mutual exclusivity block that guarantees its atomic
nature. However, this will not help when clients are creating these
objects. Therefore I am going to use a standard UIN pattern for the
identifier.  Looking at the different algorithms I chose the KSUID
algorithm because of its simplicity in design that does not need
synchronisation. It is 20 bytes in length with a time component and a
128bit high random number. Like all of these types of algorithms there
is a chance of duplication but I will accept this chance. 

A quick Duck-Duck-Go search for KSUID and Go returns an implementation
at `github.com/segmentio/ksuid`. Importing this and creating an internal
function that will set the ID is going to be the first step, followed by
adding it to our init function. Below is the code snippet

~~~go

	Import (
	...
	“github.com/segmentio/ksuid”
	)
	
	. . .
	
	func (bg *BGReading) setID() {
	   if bg.ID=="" {
	     bg.ID=ksuid.New().String()
	   }
	}
	
	. . . 
	
	func (bg *BGReading) init() {
	   if !bg.initialised {
	      bg.initialised=true
	      bg.postPrandialMinutes=-1
	      bg.setID()
	   }
	}

~~~

This internal `setID()` function will only create and set a value if ones does not already existing. We create the UID by calling ksuid.New() The
`.String()` call returns a string representation instead of the byte
array. The package being used returns a base 62 string representation.
Base62 encoding converts numbers to ASCII strings (0-9, a-z and A-Z)
which is very simple for url parsing.

>` String: 0ujzPyRiIAffKhBux4PvQdDqMHY`  
>` Raw: 066A029C73FC1AA3B2446246D6E89FCD909E8FE8`

We also need to add the call into the SetPrePrandialMinutes functions as
this will also set the initialise flag and I want an ID set as soon as
the initialise flag is set. The final place to set the ID is in the
FromJSON conversion in case an ID wasn’t sent and in which case it will
be. 

The final change in this file is to find the code that sets the Date
Value to a golang time and to truncate this to the minute boundary. This
is simple in Go and just requires use to call Truncate(time.Minute).
Here is a snippet of code to see how we use it in SetBGReadingTime 

```go
   if date.IsZero() {
      bg.Date=time.Now().Truncate(time.Minute)//todo set to UTC
   } else {
      bg.Date=date.Truncate(time.Minute) //todo set to UTC
   }
```

This function is also added to the FromJSON. However because Date is
exposed publicly we are not guarded against its value being set outside
of our functions

#### Testing the Changes
If you run the current test scripts that were built in the first blog you will find that it throws a fault now

```
	host:~/go/src/blood_glucose/blood/pkg/blood> go test
	bg1.reading = 5.000000 
	bg2.reading = 4.995560 
	JSON Value = {"Reading":5,"Date":"2019-01-25T06:59:00Z","Comment":"test","Food":"","Exercise":"","Fasting":false,"RealFasting":false,"Preprandial":false,"ID":"1GFRt5ojh3H5ObUAZ4cDDcDDgWg"}
	Original Value = {5 2019-01-25 06:59:00 +0000 GMT test   -1 true false false false 1GFRt5ojh3H5ObUAZ4cDDcDDgWg}
	FromJSON Value = {5 2019-01-25 06:59:00 +0000 UTC test   -1 true false false false 1GFRt5ojh3H5ObUAZ4cDDcDDgWg}
	--- FAIL: TestBGValues (0.00s)
	    BGReading_test.go:80: failed to confirm values for BGReading {5 2019-01-25 06:59:00 +0000 GMT test   -1 true false false false 1GFRt5ojh3H5ObUAZ4cDDcDDgWg}
	interface test &{0 0001-01-01 00:00:00 +0000 UTC initialised by New   -1 true false false false 1GFRt8UMqnfy2S5HEHQK2dmPFa7}, type *blood.BGReading

```

The reason for the fail is that we have truncated the time down to the closest minute and therefore the time no longer matches.  We therefore change our test such that we allow our routine to truncate and then we test against the truncated version

```go
 if bg1.Reading!=5.0 || bg1.Comment!="test" || bg1.Date!=date.Truncate(time.Minute) {
     t.Errorf("failed to confirm values for BGReading %v",bg1)
 }
```

If you also look at the printed values  you can see the ID being created automatically for us as part of the initialisation 

> Original Value = {5 2019-01-25 06:59:00 +0000 GMT test   -1 true false false false 1GFRt5ojh3H5ObUAZ4cDDcDDgWg}

### Changing the Collection

The BGSet we developed in the first blog needs to be changed to give us the ability to manage our entries using the ID field as well as by date.  Even though we will have access by ID we still have to maintain the uniqueness of the records entries by date/time which will of course be inforced on us by the Set we have used.

#### Package Initialisation
The first thing I am going to do is create an initialisation function which runs when the file is package is first loaded.  We will use it for now to initialise our set just to give an example

```
func init() {
    _ = getInstance()
}
```

All this function is doing is initialising our singleton package before anything attempts to use it.  Later we may add further capabilities into this.  

There is no corresponding `finalize()` function but there is a `finalizer()` function that can be registered against a pointer that is run when the garbage collector clears up the memory.  There is no guarantee when this runs or the state of the memory at that time or if it runs at all.  

Additionally you can also trap an interruption (ctrl+c) that will terminate the go program and run some clean up such as release/write files.

#### Retrieving a BGReading by the ID

One of the first things we need to decide is how we want to retrieve the value from the set.  The set is not keyed or sorted by the ID so we have 2 options: 
* Walk the set
* Manage an Index of ID to Value in a Hash 

The latter option would provide for a more scalable solution but also carries the complexity that for each operation on the set also has to be atomic with the equivalent on the index.  We may come back to this at a later date just for completeness

For now I am going to do the brute force approach of walking the tree in the Set and finding the record with our ID.  Note as the set is not Keyed from our ID there is no reason that the ID couldn't be included multiple times except we will not allow this programmatically so we will be able to just return the first.

Our Find by ID function is going to be very similar to the Find by Date we defined in part one previously

```go
func FindReadingByID(id string) (BGReading,bool) {
    var reading BGReading
    var preading *BGReading
    var found bool
    preading,found=findReadingByID(id)
    if found {
        reading=*preading
        return reading,true
    } else {
        return *new(BGReading),false
    }
}
```

It takes the ID as a string and returns a copy of the BGReading it finds and the true/false boolean if a record was detected.  I decided that interaction with the set by ID will be by copy and not pointer so that data in our set cannot be manipulated.  

The find by date function in blog one was defined to pass a pointer to the actual object.  As discussed at the end of the first blog this is prone to error so we may well come back and correct this as well

If you had paid attention you will notice that I called a function that doesn't exist and seems to be passing a pointer and boolean. This is the internal function that actually does the tree walk

```go
 func findReadingByID(id string) (*BGReading, bool) {
    set:=getInstance().bgset
    foundIndex, foundValue := set.Find(func(index int, value interface{}) bool {
        t,ok := value.(*BGReading)
        if !ok {
            fmt.Printf("%T is not a BGReading\n", value)
            return false
        }
        return t.ID==id
    })
    if foundIndex != -1 {
        r:=foundValue.(*BGReading)
        return r,true
    } else {
        return nil,false
    }
}
```

This function walks the set and tests for ID equality and is found by brute force (fine for a small number of records).

#### Updating our Reading

As we are passing back a copy of the BGReading we now need to offer out a function to update a record with new values. 

This requires some thought and I wanted to try to make it idempotent in its action so that it could be called multiple times with the same change and we would be left in the same state.

So what are my functional requirements?
- Ensure the ID is valid
- Find the record by ID
- Make sure the time value is canonicalised to the minute and any further rules we may wish to define at a later point
- locate the reading by the ID 
  - if it isn't found by ID then try to locate by date as long as the date isn't zero
  - If a reading with this date isn't found then I can add the reading as a new record
  - When the new reading is successfully added return success
- If the record is found by ID then check if the date is the same.
  - if it isnt check that a different record doesn't exist with this date and fail if it does
  - remove the record from the set update its date and re-add it to the set to maintain set order 
  - update all other fields
  - return success
  

This function is now adding as an add and update and I can use it as is for the REST Post Method and the PUT by ID

```go
func UpdateReadingbyID(reading BGReading) bool {
    var currentReading *BGReading
    var found bool
    if reading.ID=="" {
        return false
    }
    reading.SetBGReadingValues(SetDate(reading.Date.Truncate(time.Minute)))
    currentReading,found = findReadingByID(reading.ID)

    if !found {
        //record with ID doesn't exist
        if reading.Date.IsZero() {
            //date is zero so cannot add
            return false
        }
        currentReading,found= FindReadingByDate(reading.Date)
        if !found {
            // An existing record with this time doesnt exist so can add this as a new record
            AddReading(reading)
            //get the reading that was added to confirm action
            currentReading,found = findReadingByID(reading.ID)
            if !found {
                return false
            } else {
                return true
            }
        }
    }

    //check if the date is changing and remove and readd to set if it is
    if !reading.Date.Equal(currentReading.Date) {
        //check there isnt already a dated entry
        _,oops:=FindReadingByDate(reading.Date)
        if oops {
            return false
        }
        getInstance().bgset.Remove(currentReading)
        currentReading.Date=reading.Date
        getInstance().bgset.Add(currentReading)
    }

    //Just update values
    currentReading.Reading=reading.Reading
    currentReading.Comment=reading.Comment
    currentReading.Food=reading.Food
    currentReading.Exercise=reading.Exercise
    currentReading.Fasting=reading.Fasting
    currentReading.RealFasting=reading.RealFasting
    currentReading.Preprandial=reading.Preprandial


    return true
}

```

#### Fixing AddReading
It may seem that AddReading doesn't need changing   
_current version_   
```go
func AddReading(bg BGReading) {
    i:=getInstance()
    bg.Date=bg.Date.Truncate(time.Minute)
    i.bgset.Add(&bg)
}
```

However, we now need to assure that The ID isnt empty and also that our date isn't zero

```go
func AddReading(bg BGReading) {
    if bg.Date.IsZero() || bg.ID=="" {
        return
    }
    i:=getInstance()
    bg.Date=bg.Date.Truncate(time.Minute)
    i.bgset.Add(&bg)
}
```

Unfortunately this isn't the complete story.  What do we do if the reading already exists with this date and the ID is different or that the ID exists but not at this Date.  We have to make sure we guard against making our set inconsistent. The conditional states are:
+ Reading with Date and ID not present - OK to Add
+ Reading with Date present and ID matches - no need to Add
+ Reading with Date present but ID doesnt Match - can't add
+ Reading with Date doesnt exist but record with ID does - can't add

Given that there are now cases when we can't addwe need to think about an appropriate return that gives the caller information so he can take action instead of being silent.

```go
func AddReading(bg BGReading) error {
    if bg.Date.IsZero() {
        return fmt.Errorf("Cannot add a reading with a zero date")
    }
    if bg.ID=="" {
        return fmt.Errorf("Cannot add a reading without an ID")
    }
    i:=getInstance()
    bg.Date=bg.Date.Truncate(time.Minute)
    
    pbg_date,date_found  := FindReadingByDate(bg.Date)
    pbg_id,id_found := findReadingByID(bg.ID)
    if date_found && id_found && (pbg_date==pbg_id) { //record already exists
        return nil
    }
    if date_found && !id_found { //reading with this date already exists
        return fmt.Errorf("A reading already exists at %s with the ID %s",bg.Date.Format(time.RFC3339), pbg_date.ID)
    }
    if !date_found && id_found { //reading with this ID exists at a different date
        return fmt.Errorf("A reading with ID %s exists with a different time %s", bg.ID, pbg_id.Date.Format(time.RFC3339))
    }
    i.bgset.Add(&bg)
    return nil
}
```

Our test scripts still run and succeed but do not cover the scope of our changes therefore we will come back to the unit test later.


#### Deleting a record by ID

To delete by ID requires that we find the reading with this ID and then remove it from the set.  The caller would like confirmation of success so we will return a true when we have success and false if the reading isn't found

```
func RemoveReadingByID(id string) bool {
    bg,b:=findReadingByID(id)
    if !b {
        return false
    }
    s:=getInstance().bgset
    s.Remove(bg)
    return true
}
```

### Testing the Changes
#### Test the different forms of AddReading
As AddReading is now more complicated we have to test all forms and test routes.  This will also test the FindbyID 

To accommodate this we ceate a new function in the bgset_test.go file that reinitialises the array adds the test records afresh and then adds modified records to test the different states

```go
func TestAddID(t *testing.T) {
    ClearReadings() //reset Set
    fmt.Println("")
    fmt.Println("---testing Add by with IDs---")
    fmt.Println("")
    for _,c :=range bgdataset {
        e:=AddReading(*c)
        if e!=nil {
            t.Errorf("Adding initial record set fails: %s", e.Error())
            return
        }
    }
    
    fmt.Println("------Check Add record again------")
    bg:=*bgdataset[1]
    e := AddReading(bg)
    if e!=nil {
        t.Errorf("Adding duplicate record failed: %s", e.Error())
        return
    }
    
    fmt.Println("------Check Add record New ID existing time------")
    bg=*bgdataset[1]
    bg.ID=""
    bg.setID()
    e = AddReading(bg)
    if e==nil { //should not accept it
        t.Errorf("Adding duplicate time record failed as it didn't throw error")
        return
    } else {
        fmt.Printf("--------- error returned correctly %s\n", e.Error())
    }
    
    
    fmt.Println("------Check Add record Existing ID different time------")
    bg=*bgdataset[2]
    bg.Date=time.Now().Truncate(time.Minute)    
    e = AddReading(bg)
    if e==nil { //should not accept it
        t.Errorf("Adding duplicate ID record failed as it didn't throw error")
        return
    } else {
        fmt.Printf("--------- error returned correctly %s\n", e.Error())
    }
    
    
    fmt.Println("------Check Add record zero time------")
    bg.Date=*new(time.Time)
    e = AddReading(bg)
    if e==nil { //should not accept it
        t.Errorf("Added reading with zero date didnt throw error %s", bg.Date.Format(time.RFC3339))
        return
    } else {
        fmt.Printf("--------- error returned correctly %s\n", e.Error())
    }
    
    
    fmt.Println("------Check Add record no ID------")
    bg=*bgdataset[2]
    bg.ID=""
    e = AddReading(bg)
    if e==nil { //should not accept it
        t.Errorf("Added reading with no ID didnt throw error ")
        return
    } else {
        fmt.Printf("--------- error returned correctly %s\n", e.Error())
    }
}
```

```bash
> go test

... [previous test results cut]

---testing Add by with IDs---

------Check Add record again------
------Check Add record New ID existing time------
--------- error returned correctly A reading already exists at 2018-12-01T10:00:00Z with the ID 1GJvi0WKGMPYzJ8wDPNntJQDgyR
------Check Add record Existing ID different time------
--------- error returned correctly A reading with ID 1GJvhxhEYRlLIisDxlvKDMPNc8j exists with a different time 2018-12-01T11:00:00Z
------Check Add record zero time------
--------- error returned correctly Cannot add a reading with a zero date
------Check Add record no ID------
--------- error returned correctly Cannot add a reading without an ID
PASS
ok      blood_glucose/blood/pkg/blood   0.002s


```

#### Clean up the test scripts

During the course of these blogs we have been writing test functions and also using them as a debugging platform with print statements.  Not technically wrong this does not make for good output and if we want to clean it up we will lose our output if we later come back and do changes.  

What are our options?

The Go testing framework provides many facilities that we haven't even looked at.  If we want to clean up the print statements then the test package provides us with a facility `(*testing.T) func Log(string)` which is the equiv `fmt.Println` and `(*testing.T) func Logf(string,... Interface())` which is the equivalent of `fmt.Printf`

Doing a replacement across the test files and not forgetting to remove the import statements for `fmt` gives a nice clean test run
```
~/go/src/blood_glucose/blood/pkg/blood> go test 
PASS
ok      blood_glucose/blood/pkg/blood   0.003s

```

Where is our output?  Well go writes the logging output to where ever you tell it to and in this case it was the bit bucket.  Let's just send it to `stdout` which we can do with the `-v` verbose flag

```
~/go/src/blood_glucose/blood/pkg/blood> go test 
PASS
ok      blood_glucose/blood/pkg/blood   0.003s
andrew@host:~/go/src/blood_glucose/blood/pkg/blood> go test  -v
=== RUN   TestConvertMMOL
--- PASS: TestConvertMMOL (0.00s)
=== RUN   TestConvertMG
--- PASS: TestConvertMG (0.00s)
=== RUN   TestSetReading
--- PASS: TestSetReading (0.00s)
    BGReading_test.go:47: bg1.reading = 5.000000 
    BGReading_test.go:50: bg2.reading = 4.995560 
=== RUN   TestBGValues
--- PASS: TestBGValues (0.00s)
    BGReading_test.go:86: JSON Value = {"Reading":5,"Date":"2019-01-27T06:46:00Z","Comment":"test","Food":"","Exercise":"","Fasting":false,"RealFasting":false,"Preprandial":false,"ID":"1GL4a1Fc4xX7c0TihNM6hixj57p"}
    BGReading_test.go:92: Original Value = {5 2019-01-27 06:46:00 +0000 GMT test   -1 true false false false 1GL4a1Fc4xX7c0TihNM6hixj57p}
    BGReading_test.go:93: FromJSON Value = {5 2019-01-27 06:46:00 +0000 UTC test   -1 true false false false 1GL4a1Fc4xX7c0TihNM6hixj57p}
=== RUN   TestBRReadingInterface
--- PASS: TestBRReadingInterface (0.00s)
    BGReading_test.go:101: interface test &{0 0001-01-01 00:00:00 +0000 UTC initialised by New   -1 true false false false 1GL4a07vtfSRBzWrPwucGITCHrG}, type *blood.BGReading
=== RUN   TestBGSet
--- PASS: TestBGSet (0.00s)
    BGSet_test.go:37: Testing BGSet
    BGSet_test.go:41: adding 0xc0000d0380:{5 2018-12-01 09:00:00 +0000 UTC test   -1 true false false false 1GL4a49V68GlvZE83TH6xuqaYEZ}
    BGSet_test.go:41: adding 0xc0000d03f0:{7.8 2018-12-01 10:00:00 +0000 UTC test1   -1 true false false false 1GL4a5pwWLGxo1bVs7R733zK16s}
    BGSet_test.go:41: adding 0xc0000d0460:{6.8 2018-12-01 11:00:00 +0000 UTC test2   -1 true false false false 1GL4a3zbpMH72c9fZS60ttc5t7w}
    BGSet_test.go:41: adding 0xc0000d04d0:{4.8 2018-12-01 12:00:00 +0000 UTC test3   -1 true false false false 1GL4a2eHrSwnh9uwBDrLQTjq9m7}
    BGSet_test.go:45: [{"Reading":5,"Date":"2018-12-01T09:00:00Z","Comment":"test","Food":"","Exercise":"","Fasting":false,"RealFasting":false,"Preprandial":false,"ID":"1GL4a49V68GlvZE83TH6xuqaYEZ"},{"Reading":7.8,"Date":"2018-12-01T10:00:00Z","Comment":"test1","Food":"","Exercise":"","Fasting":false,"RealFasting":false,"Preprandial":false,"ID":"1GL4a5pwWLGxo1bVs7R733zK16s"},{"Reading":6.8,"Date":"2018-12-01T11:00:00Z","Comment":"test2","Food":"","Exercise":"","Fasting":false,"RealFasting":false,"Preprandial":false,"ID":"1GL4a3zbpMH72c9fZS60ttc5t7w"},{"Reading":4.8,"Date":"2018-12-01T12:00:00Z","Comment":"test3","Food":"","Exercise":"","Fasting":false,"RealFasting":false,"Preprandial":false,"ID":"1GL4a2eHrSwnh9uwBDrLQTjq9m7"}]
    BGSet_test.go:53: [{"Reading":5,"Date":"2018-12-01T09:00:00Z","Comment":"test","Food":"","Exercise":"","Fasting":false,"RealFasting":false,"Preprandial":false,"ID":"1GL4a49V68GlvZE83TH6xuqaYEZ"},{"Reading":7.8,"Date":"2018-12-01T10:00:00Z","Comment":"test1","Food":"","Exercise":"","Fasting":false,"RealFasting":false,"Preprandial":false,"ID":"1GL4a5pwWLGxo1bVs7R733zK16s"},{"Reading":4.8,"Date":"2018-12-01T12:00:00Z","Comment":"test3","Food":"","Exercise":"","Fasting":false,"RealFasting":false,"Preprandial":false,"ID":"1GL4a2eHrSwnh9uwBDrLQTjq9m7"}]
    BGSet_test.go:69: [{"Reading":5,"Date":"2018-12-01T09:00:00Z","Comment":"test","Food":"","Exercise":"","Fasting":false,"RealFasting":false,"Preprandial":false,"ID":"1GL4a49V68GlvZE83TH6xuqaYEZ"},{"Reading":7.8,"Date":"2018-12-01T10:00:00Z","Comment":"test1","Food":"","Exercise":"","Fasting":false,"RealFasting":false,"Preprandial":false,"ID":"1GL4a5pwWLGxo1bVs7R733zK16s"},{"Reading":4.8,"Date":"2018-12-01T12:00:00Z","Comment":"test3","Food":"","Exercise":"","Fasting":false,"RealFasting":false,"Preprandial":false,"ID":"1GL4a2eHrSwnh9uwBDrLQTjq9m7"}]
        [{"Reading":5,"Date":"2018-12-01T09:00:00Z","Comment":"test","Food":"","Exercise":"","Fasting":false,"RealFasting":false,"Preprandial":false,"ID":"1GL4a49V68GlvZE83TH6xuqaYEZ"},{"Reading":7.8,"Date":"2018-12-01T10:00:00Z","Comment":"changed after find","Food":"","Exercise":"","Fasting":false,"RealFasting":false,"Preprandial":false,"ID":"1GL4a5pwWLGxo1bVs7R733zK16s"},{"Reading":4.8,"Date":"2018-12-01T12:00:00Z","Comment":"test3","Food":"","Exercise":"","Fasting":false,"RealFasting":false,"Preprandial":false,"ID":"1GL4a2eHrSwnh9uwBDrLQTjq9m7"}]
=== RUN   TestAddID
--- PASS: TestAddID (0.00s)
    BGSet_test.go:81: 
    BGSet_test.go:82: ---testing Add by with IDs---
    BGSet_test.go:83: 
    BGSet_test.go:92: ------Check Add record again------
    BGSet_test.go:100: ------Check Add record New ID existing time------
    BGSet_test.go:109: --------- error returned correctly A reading already exists at 2018-12-01T10:00:00Z with the ID 1GL4ZymZPTIVyqKAMYZpeKHlYUn
    BGSet_test.go:111: ------Check Add record Existing ID different time------
    BGSet_test.go:119: --------- error returned correctly A reading with ID 1GL4a1hPud5hxpXxcgu7kcft4ym exists with a different time 2018-12-01T11:00:00Z
    BGSet_test.go:122: ------Check Add record zero time------
    BGSet_test.go:129: --------- error returned correctly Cannot add a reading with a zero date
    BGSet_test.go:131: ------Check Add record no ID------
    BGSet_test.go:139: --------- error returned correctly Cannot add a reading without an ID
PASS
ok      blood_glucose/blood/pkg/blood   0.003s
```

Until now we have run every test every time. The Test platform  allows us to be selective about which test to run.  If I only wish to run the test for Adding Readings then as defined this is the TestAddID function we created.  To restrict the scope of the test we use the `-run=` option with go test.  

Note that at present this will fail because our test isn't stand alone and relies on its data being initialised earlier.  There are ways to fix this using the frameworks sub-test pattern but I am going to change our data `init` routine to ensure it starts afresh with a clean slice and call it from the TestAddID function

```go
func initdata() {
    //make a new zero length slice
    bgdataset=make([]*BGReading,0)
```

```go
func TestAddID(t *testing.T) {
    initdata()
    ClearReadings() //reset Set
```

To run the test with just this function is now just a case of running
```
~/go/src/blood_glucose/blood/pkg/blood> go test  -run=AddID -v
=== RUN   TestAddID
--- PASS: TestAddID (0.00s)
    BGSet_test.go:81: 
    BGSet_test.go:82: ---testing Add by with IDs---
    BGSet_test.go:83: 
    BGSet_test.go:92: ------Check Add record again------
    BGSet_test.go:100: ------Check Add record New ID existing time------
    BGSet_test.go:109: --------- error returned correctly A reading already exists at 2018-12-01T10:00:00Z with the ID 1GL5sveE77SE22lbvwkvsVJc5c7
    BGSet_test.go:111: ------Check Add record Existing ID different time------
    BGSet_test.go:119: --------- error returned correctly A reading with ID 1GL5swP03hyZ08aEaG6QL9vRYaT exists with a different time 2018-12-01T11:00:00Z
    BGSet_test.go:122: ------Check Add record zero time------
    BGSet_test.go:129: --------- error returned correctly Cannot add a reading with a zero date
    BGSet_test.go:131: ------Check Add record no ID------
    BGSet_test.go:139: --------- error returned correctly Cannot add a reading without an ID
PASS
ok      blood_glucose/blood/pkg/blood   0.002s
```

The go test framework has many different capabilities such as Benchmarking, Test Coverage and Sub-Tests

##### Test Coverage
Test coverage is an interesting topic to look into further.  If I want to see just how my test coverage looks then I can use the `-cover` flag to our test routine

```
~/go/src/blood_glucose/blood/pkg/blood> go test -cover
PASS
coverage: 59.6% of statements
ok      blood_glucose/blood/pkg/blood   0.002s
```

As you can see I have a 60% test coverage.  How about a breakdown by function so we can see what we are missing.  Well go will do that for us as well.  First we need to capture the detailed coverage data with the `-coverprofile` option

```
~/go/src/blood_glucose/blood/pkg/blood> go test -coverprofile=coverage.out
PASS
coverage: 59.6% of statements
ok      blood_glucose/blood/pkg/blood   0.003s

```

All that remains is to run the report

```
go tool cover -func=coverage.out
blood_glucose/blood/pkg/blood/BGReading.go:34:  NewBGReading            100.0%
blood_glucose/blood/pkg/blood/BGReading.go:42:  init                    100.0%
blood_glucose/blood/pkg/blood/BGReading.go:50:  setID                   100.0%
blood_glucose/blood/pkg/blood/BGReading.go:56:  SetBGReading            100.0%
blood_glucose/blood/pkg/blood/BGReading.go:66:  ConvertMMOL2MGDL        100.0%
blood_glucose/blood/pkg/blood/BGReading.go:70:  ConvertMGDL2MMOL        100.0%
blood_glucose/blood/pkg/blood/BGReading.go:74:  ConvertMMOLtoHbA1c      0.0%
blood_glucose/blood/pkg/blood/BGReading.go:80:  ConvertHbA1cIFCCtoNGSP  0.0%
blood_glucose/blood/pkg/blood/BGReading.go:84:  ConvertHbA1cNGSPtoIFCC  0.0%
blood_glucose/blood/pkg/blood/BGReading.go:88:  setBGReadingTime        75.0%
blood_glucose/blood/pkg/blood/BGReading.go:103: SetDate                 100.0%
blood_glucose/blood/pkg/blood/BGReading.go:109: SetComment              100.0%
blood_glucose/blood/pkg/blood/BGReading.go:115: SetFood                 0.0%
blood_glucose/blood/pkg/blood/BGReading.go:121: SetExercise             0.0%
blood_glucose/blood/pkg/blood/BGReading.go:127: SetReading              100.0%
blood_glucose/blood/pkg/blood/BGReading.go:133: SetBGReadingValues      100.0%
blood_glucose/blood/pkg/blood/BGReading.go:141: SetPrePrandialMinutes   0.0%
blood_glucose/blood/pkg/blood/BGReading.go:155: SetPostPrandial         0.0%
blood_glucose/blood/pkg/blood/BGReading.go:161: GetPostPrandialMinutes  0.0%
blood_glucose/blood/pkg/blood/BGReading.go:165: ToJSON                  100.0%
blood_glucose/blood/pkg/blood/BGReading.go:170: FromJSON                87.5%
blood_glucose/blood/pkg/blood/BGSet.go:12:      init                    100.0%
blood_glucose/blood/pkg/blood/BGSet.go:17:      byBGReadingDate         84.6%
blood_glucose/blood/pkg/blood/BGSet.go:42:      atsametime              100.0%
blood_glucose/blood/pkg/blood/BGSet.go:63:      getInstance             100.0%
blood_glucose/blood/pkg/blood/BGSet.go:74:      AddReading              100.0%
blood_glucose/blood/pkg/blood/BGSet.go:100:     ClearReadings           100.0%
blood_glucose/blood/pkg/blood/BGSet.go:106:     Size                    100.0%
blood_glucose/blood/pkg/blood/BGSet.go:111:     RemoveReadingByDate     100.0%
blood_glucose/blood/pkg/blood/BGSet.go:119:     RemoveReadingByID       0.0%
blood_glucose/blood/pkg/blood/BGSet.go:130:     FindReadingByDate       84.6%
blood_glucose/blood/pkg/blood/BGSet.go:157:     findReadingByID         81.8%
blood_glucose/blood/pkg/blood/BGSet.go:176:     FindReadingByID         0.0%
blood_glucose/blood/pkg/blood/BGSet.go:190:     UpdateReadingbyID       0.0%
blood_glucose/blood/pkg/blood/BGSet.go:245:     ToJSON                  66.7%
blood_glucose/blood/pkg/blood/BGSet.go:258:     FromJSON                80.0%
total:                                          (statements)            59.6%

```

We can quickly see functions that are completely missing test and those that are not yet fully covered.  What we aren't clear about is exactly which paths aren't covered in those functions.  Well go will show us that too.  Let's produce the output in HTML form so we can view it visually

    go tool cover -html=coverage.out
    

When this is run an html window is opened and a detailed visual report is created

![](coverage.png")

In the pictures folder of the repository is the full [HTML]({{<ref "coverage.html">}} "html") for this coverage picture.  *Note that gitlab and github will not display the page inside their website (security) so please download to your computer and view it locally.*

#### Test Deletion of Records by ID
There are a few tests that we wish to do in the test:
1. Delete a middle record in the set
1. Try to delete using an empty ID and check a false return
1. Try to delete using an ID that isn't found and check a false return
1. Delete The last record in the set
1. Delete the first record in the set
1. Delete the one and only record left in the set and check the size of the set is now zero

This gives us good coverage of the delete routine

```go
func TestRemoveReadingByID(t *testing.T) {
    t.Log("")
    t.Log("testing Remove with IDs")
    
    //initialise the data sets
    initdata()
    ClearReadings() //reset Set
    
    for _,c :=range bgdataset {
        e:=AddReading(*c)
        if e!=nil {
            t.Errorf("Adding initial record set fails: %s", e.Error())
            return
        }
    }
   
   t.Logf("  delete reading with ID %s", bgdataset[2].ID)
   success:=RemoveReadingByID(bgdataset[2].ID)
   if !success {
       t.Errorf("failed to remove reading using ID %s", bgdataset[2].ID)
   }
   
   t.Logf("  delete reading with empty ID")
   failure:=RemoveReadingByID("")
   if failure {
       t.Errorf("success returned trying to pass an empty ID")
   }
   
   t.Logf("  delete reading with Non-Existent ID")
   failure=RemoveReadingByID("cant exist")
   if failure {
       t.Errorf("success returned trying to pass a random ID")
   }
   
   t.Logf("  delete the rest out of order")
   success = true
   t.Logf("  delete reading with ID %s", bgdataset[3].ID)
   success= success && RemoveReadingByID(bgdataset[3].ID)
   if !success {
       t.Errorf("failed to remove (last) reading using ID %s", bgdataset[3].ID)
   }
   t.Logf("  delete reading with ID %s", bgdataset[0].ID)
   success= success && RemoveReadingByID(bgdataset[0].ID)
   if !success {
       t.Errorf("failed to remove (first) reading using ID %s", bgdataset[0].ID)
   }
   t.Logf("  delete reading with ID %s", bgdataset[1].ID)
   success= success && RemoveReadingByID(bgdataset[1].ID)
   if !success && Size()!=0 {
       t.Errorf("failed to remove (the only) reading using ID %s", bgdataset[1].ID)
   }
}
```

The results of the test are

```
~/go/src/blood_glucose/blood/pkg/blood> go test -v -run=RemoveReadingByID
=== RUN   TestRemoveReadingByID
--- PASS: TestRemoveReadingByID (0.00s)
    BGSet_test.go:144: 
    BGSet_test.go:145: testing Remove with IDs
    BGSet_test.go:157:   delete reading with ID 1GLW3hf0lwPThzmWqzPSbkNfXxZ
    BGSet_test.go:163:   delete reading with empty ID
    BGSet_test.go:169:   delete reading with Non-Existent ID
    BGSet_test.go:175:   delete the rest out of order
    BGSet_test.go:177:   delete reading with ID 1GLW3lHBR6t8zRL1vhpbJg3fu0J
    BGSet_test.go:182:   delete reading with ID 1GLW3g10RIBFA15aR2nRuIbGYgC
    BGSet_test.go:187:   delete reading with ID 1GLW3lSktaTZuLPOnwDFg3zgMnX
PASS
ok      blood_glucose/blood/pkg/blood   0.002s

```

A positive for all of our test conditions

#### Test FindReadingByID
We have tested the `findReadingByID` through our other testing and if you run the coverage report you will see this.  However, we have not tested the client facing `FindReadingByID` which returns a copy of the reading and not a pointer to the reading.

To test this routine there is a number of tests I wish to undertake:
1. Retrieving a known item
1. Test this is a copy
1. Try to retrieve a non-existent item

Our test function `TestFindReadingByID` will initialised the data set as we did for the test to Add and then we will find the one of the items using `FindReadingByID`.  If this fails we will raise a failure.  We will then also find using the equivalent `findReadingByID` and having both we will test that the pointers are not equal.  

```go
func TestFindReadingByID(t *testing.T) {
    t.Log("")
    t.Log("testing FindReadingByID")
    
    //initialise the data sets
    initdata()
    ClearReadings() //reset Set
    
    for _,c :=range bgdataset {
        e:=AddReading(*c)
        if e!=nil {
            t.Errorf("Adding initial record set fails: %s", e.Error())
            return
        }
    }
    
    
    bg1,success:=FindReadingByID(bgdataset[2].ID)
    if !success {
        t.Errorf("Could not Find the reading with ID %s",bgdataset[2].ID)
        t.Fail()
    }
    bg2,success:=findReadingByID(bgdataset[2].ID)
    if !success {
        t.Errorf("Could not find the reading with ID %s",bgdataset[2].ID)
        t.Fail()
    }
    if &bg1==bg2 {
        t.Errorf("The pointers are equal %v=%v",&bg1,bg2)
    } else {
        t.Logf("Find has pointer %p", &bg1)
        t.Logf("find has pointer %p", bg2)
    }
    bg3,failure:=FindReadingByID("")
    if failure {
        t.Errorf("Did not receive false and zero Reading with empty ID %v", bg3)
    }
}
```

```
~/go/src/blood_glucose/blood/pkg/blood> go test -v -run=FindReadingByID
=== RUN   TestFindReadingByID
--- PASS: TestFindReadingByID (0.00s)
    BGSet_test.go:198: 
    BGSet_test.go:199: testing FindReadingByID
    BGSet_test.go:227: Find has pointer 0xc0000cc690
    BGSet_test.go:228: find has pointer 0xc0000cc4d0
PASS
ok      blood_glucose/blood/pkg/blood   0.002s

```

#### Test UpdateReadingByID

This complex method is similar to test but there are multiple conditions Let's just do enough  to give us code coverage of the majority of the paths

To facilitate simplification and improvement I have implemented the Stringer interface on bgSet which allows Go to accept `%s` in `printf` etc.  

```go
func (*bgSet) String() string {
    res:=""
    it:=getInstance().bgset.Iterator()
    for it.Next() {
        res=fmt.Sprintf("%s\n%v",res,it.Value())
    }
    return res
}
```

You can use this with code `fmt.Printf("... %s ...",BGSet.getInstance())

```
&{5 2018-12-01 09:00:00 +0000 UTC test   -1 true false false false 1GM1lnt1sOye8MLVGA7ovwcQP7h}
&{7.8 2018-12-01 10:00:00 +0000 UTC test1   -1 true false false false 1GM1lmEMYeQGgYUB7zYeObYmNV6}
&{6.8 2018-12-01 11:00:00 +0000 UTC test2   -1 true false false false new ID Test Change of Date}
&{4.8 2018-12-01 12:00:00 +0000 UTC test3   -1 true false false false 1GM1lmsO0ZHRFT4eBqI1QdHz5it}
&{6.8 2019-01-27 14:53:00 +0000 GMT test2   -1 true false false false 1GM1ljfRwWEOqvCIhCxywZVsmJE} 
```

**Test Code**

We start by initialising our data as previously
```go
func TestUpdateReadingByID(t *testing.T) {
    t.Log("")
    t.Log("testing UpdateReadingByID")
    
    //initialise the data sets
    initdata()
    ClearReadings() //reset Set
    
    for _,c :=range bgdataset {
        e:=AddReading(*c)
        if e!=nil {
            t.Errorf("Adding initial record set fails: %s", e.Error())
            return
        }
    }
```
The first test is to check update with a blank ID
```go
    //test fail if no ID
    bgcopy:= *bgdataset[2]
    bgcopy.ID=""
    b:=UpdateReadingByID(bgcopy)
    if b {
        t.Logf("bgcopy is %T %v",bgcopy,bgcopy)
        t.Errorf("should have received a false for empty ID, received %T:%v",b,b)
        return
    }
```

The first of the straight through tests is to update a reading that is found by ID and doesn't change the time

```go
    //test change of non-structural fields
    bgcopy= *bgdataset[3]
    t.Logf("Record to be updated %v",bgcopy)
    bgcopy.SetBGReadingValues(SetFood("changed in update test"),SetExercise("Changed in update test"))
    b=UpdateReadingByID(bgcopy)
    if !b {
        t.Errorf("update failed for simple case")
        
    }
    t.Logf("%s",getInstance())

``` 

Now we need to test updating a record and changing its `Date` field.  We get a record and change its date.  After successful update we add the original record back in with a new ID `new ID Test Change of Date` to prove that there are no issues and now there is additional reading

```go
    //test update of time
    bgcopy= *bgdataset[2]
    t.Logf("Record to be updated %v",bgcopy)
    bgcopy.Date=time.Now().Truncate(time.Minute)
    t.Logf("                  to %v",bgcopy)
    b=UpdateReadingByID(bgcopy)
    if !b {
        t.Errorf("update failed for date change")
        return
    } else {
        t.Logf("%s",getInstance())
        bgcopy= *bgdataset[2]
        bgcopy.ID="new ID Test Change of Date"
        e:=AddReading(bgcopy)
        if e!=nil && Size()!=5 {
            t.Errorf("Failed to re-add original reading with new ID \n   %s", e.Error())
            return
        } else {
            t.Logf("%s",getInstance())
        }
    }
```

Now that this test is complete we need to check that I cannot update a reading with a new time that has a different reading at this time

```go
    //test change of time to impact another record
    bgcopy= *bgdataset[1]
    t.Logf("Record to be updated %v",bgcopy)
    bgcopy.Date=bgdataset[2].Date
    t.Logf("                  to %v",bgcopy)
    b=UpdateReadingByID(bgcopy)
    if b {
         t.Errorf("Managed to update record to a date of another record")
         return
    } else {
        t.Logf("new state \n%s",getInstance())
    }
    
```

The final test is to check that Update works with a record that has no similar date or ID so will be a simple addition
```go
    //check add if record not found
    bgcopy= *bgdataset[1]
    b=RemoveReadingByID(bgcopy.ID)
    //check it is gone
    if !b {
        t.Errorf("record to test with couldn't be deleted %v",bgcopy)
        t.Logf("%s",getInstance())
        return
    }
    //now update the reading
    b=UpdateReadingByID(bgcopy)
    if !b {
        t.Errorf("record to test with didn't get added %v",bgcopy)
        t.Logf("%s",getInstance())
        return
    }
    t.Logf("New state after add record code branch %s",getInstance())
``` 

Running The test shows success with the detailed logging information
```
~/go/src/blood_glucose/blood/pkg/blood> go test -v -run=UpdateReadingByID
=== RUN   TestUpdateReadingByID
--- PASS: TestUpdateReadingByID (0.00s)
    BGSet_test.go:237: 
    BGSet_test.go:238: testing UpdateReadingByID
    BGSet_test.go:264: Record to be updated {4.8 2018-12-01 12:00:00 +0000 UTC test3   -1 true false false false 1GM6wVrt955Z98NWpiYumJfMc5F}
    BGSet_test.go:271: 
        &{5 2018-12-01 09:00:00 +0000 UTC test   -1 true false false false 1GM6wQdWTrHOKpvrVskUPMXWIEb}
        &{7.8 2018-12-01 10:00:00 +0000 UTC test1   -1 true false false false 1GM6wREi0MEOz5Qs1ubz5paqzEH}
        &{6.8 2018-12-01 11:00:00 +0000 UTC test2   -1 true false false false 1GM6wUvXzjLKlLbfXcqKpB9mGo4}
        &{4.8 2018-12-01 12:00:00 +0000 UTC test3 changed in update test Changed in update test -1 true false false false 1GM6wVrt955Z98NWpiYumJfMc5F}
    BGSet_test.go:274: Record to be updated {6.8 2018-12-01 11:00:00 +0000 UTC test2   -1 true false false false 1GM6wUvXzjLKlLbfXcqKpB9mGo4}
    BGSet_test.go:276:                   to {6.8 2019-01-27 15:35:00 +0000 GMT test2   -1 true false false false 1GM6wUvXzjLKlLbfXcqKpB9mGo4}
    BGSet_test.go:282: 
        &{5 2018-12-01 09:00:00 +0000 UTC test   -1 true false false false 1GM6wQdWTrHOKpvrVskUPMXWIEb}
        &{7.8 2018-12-01 10:00:00 +0000 UTC test1   -1 true false false false 1GM6wREi0MEOz5Qs1ubz5paqzEH}
        &{4.8 2018-12-01 12:00:00 +0000 UTC test3 changed in update test Changed in update test -1 true false false false 1GM6wVrt955Z98NWpiYumJfMc5F}
        &{6.8 2019-01-27 15:35:00 +0000 GMT test2   -1 true false false false 1GM6wUvXzjLKlLbfXcqKpB9mGo4}
    BGSet_test.go:290: 
        &{5 2018-12-01 09:00:00 +0000 UTC test   -1 true false false false 1GM6wQdWTrHOKpvrVskUPMXWIEb}
        &{7.8 2018-12-01 10:00:00 +0000 UTC test1   -1 true false false false 1GM6wREi0MEOz5Qs1ubz5paqzEH}
        &{6.8 2018-12-01 11:00:00 +0000 UTC test2   -1 true false false false new ID Test Change of Date}
        &{4.8 2018-12-01 12:00:00 +0000 UTC test3 changed in update test Changed in update test -1 true false false false 1GM6wVrt955Z98NWpiYumJfMc5F}
        &{6.8 2019-01-27 15:35:00 +0000 GMT test2   -1 true false false false 1GM6wUvXzjLKlLbfXcqKpB9mGo4}
    BGSet_test.go:296: Record to be updated {7.8 2018-12-01 10:00:00 +0000 UTC test1   -1 true false false false 1GM6wREi0MEOz5Qs1ubz5paqzEH}
    BGSet_test.go:298:                   to {7.8 2018-12-01 11:00:00 +0000 UTC test1   -1 true false false false 1GM6wREi0MEOz5Qs1ubz5paqzEH}
    BGSet_test.go:304: new state 
        
        &{5 2018-12-01 09:00:00 +0000 UTC test   -1 true false false false 1GM6wQdWTrHOKpvrVskUPMXWIEb}
        &{7.8 2018-12-01 10:00:00 +0000 UTC test1   -1 true false false false 1GM6wREi0MEOz5Qs1ubz5paqzEH}
        &{6.8 2018-12-01 11:00:00 +0000 UTC test2   -1 true false false false new ID Test Change of Date}
        &{4.8 2018-12-01 12:00:00 +0000 UTC test3 changed in update test Changed in update test -1 true false false false 1GM6wVrt955Z98NWpiYumJfMc5F}
        &{6.8 2019-01-27 15:35:00 +0000 GMT test2   -1 true false false false 1GM6wUvXzjLKlLbfXcqKpB9mGo4}
    BGSet_test.go:323: New state after add record code branch 
        &{5 2018-12-01 09:00:00 +0000 UTC test   -1 true false false false 1GM6wQdWTrHOKpvrVskUPMXWIEb}
        &{7.8 2018-12-01 10:00:00 +0000 UTC test1   -1 true false false false 1GM6wREi0MEOz5Qs1ubz5paqzEH}
        &{6.8 2018-12-01 11:00:00 +0000 UTC test2   -1 true false false false new ID Test Change of Date}
        &{4.8 2018-12-01 12:00:00 +0000 UTC test3 changed in update test Changed in update test -1 true false false false 1GM6wVrt955Z98NWpiYumJfMc5F}
        &{6.8 2019-01-27 15:35:00 +0000 GMT test2   -1 true false false false 1GM6wUvXzjLKlLbfXcqKpB9mGo4}
PASS
ok      blood_glucose/blood/pkg/blood   0.002s
```

If we run the functional coverage report again we can see that we are over 80% coverage with most function now at 100%

```
~/go/src/blood_glucose/blood/pkg/blood> go test -coverprofile=coverage.out
PASS
coverage: 84.3% of statements
ok      blood_glucose/blood/pkg/blood   0.003s
andrew@host:~/go/src/blood_glucose/blood/pkg/blood> go tool cover -func=coverage.out
blood_glucose/blood/pkg/blood/BGReading.go:34:  NewBGReading            100.0%
blood_glucose/blood/pkg/blood/BGReading.go:42:  init                    100.0%
blood_glucose/blood/pkg/blood/BGReading.go:50:  setID                   100.0%
blood_glucose/blood/pkg/blood/BGReading.go:56:  SetBGReading            100.0%
blood_glucose/blood/pkg/blood/BGReading.go:66:  ConvertMMOL2MGDL        100.0%
blood_glucose/blood/pkg/blood/BGReading.go:70:  ConvertMGDL2MMOL        100.0%
blood_glucose/blood/pkg/blood/BGReading.go:74:  ConvertMMOLtoHbA1c      0.0%
blood_glucose/blood/pkg/blood/BGReading.go:80:  ConvertHbA1cIFCCtoNGSP  0.0%
blood_glucose/blood/pkg/blood/BGReading.go:84:  ConvertHbA1cNGSPtoIFCC  0.0%
blood_glucose/blood/pkg/blood/BGReading.go:88:  setBGReadingTime        75.0%
blood_glucose/blood/pkg/blood/BGReading.go:103: SetDate                 100.0%
blood_glucose/blood/pkg/blood/BGReading.go:109: SetComment              100.0%
blood_glucose/blood/pkg/blood/BGReading.go:115: SetFood                 100.0%
blood_glucose/blood/pkg/blood/BGReading.go:121: SetExercise             100.0%
blood_glucose/blood/pkg/blood/BGReading.go:127: SetReading              100.0%
blood_glucose/blood/pkg/blood/BGReading.go:133: SetBGReadingValues      100.0%
blood_glucose/blood/pkg/blood/BGReading.go:141: SetPrePrandialMinutes   0.0%
blood_glucose/blood/pkg/blood/BGReading.go:155: SetPostPrandial         0.0%
blood_glucose/blood/pkg/blood/BGReading.go:161: GetPostPrandialMinutes  0.0%
blood_glucose/blood/pkg/blood/BGReading.go:165: ToJSON                  100.0%
blood_glucose/blood/pkg/blood/BGReading.go:170: FromJSON                87.5%
blood_glucose/blood/pkg/blood/BGSet.go:12:      init                    100.0%
blood_glucose/blood/pkg/blood/BGSet.go:17:      byBGReadingDate         84.6%
blood_glucose/blood/pkg/blood/BGSet.go:42:      atsametime              100.0%
blood_glucose/blood/pkg/blood/BGSet.go:63:      getInstance             100.0%
blood_glucose/blood/pkg/blood/BGSet.go:74:      AddReading              100.0%
blood_glucose/blood/pkg/blood/BGSet.go:100:     ClearReadings           100.0%
blood_glucose/blood/pkg/blood/BGSet.go:106:     Size                    100.0%
blood_glucose/blood/pkg/blood/BGSet.go:111:     RemoveReadingByDate     100.0%
blood_glucose/blood/pkg/blood/BGSet.go:119:     RemoveReadingByID       100.0%
blood_glucose/blood/pkg/blood/BGSet.go:130:     FindReadingByDate       84.6%
blood_glucose/blood/pkg/blood/BGSet.go:157:     findReadingByID         81.8%
blood_glucose/blood/pkg/blood/BGSet.go:176:     FindReadingByID         100.0%
blood_glucose/blood/pkg/blood/BGSet.go:190:     UpdateReadingByID       93.5%
blood_glucose/blood/pkg/blood/BGSet.go:246:     ToJSON                  66.7%
blood_glucose/blood/pkg/blood/BGSet.go:259:     FromJSON                80.0%
blood_glucose/blood/pkg/blood/BGSet.go:283:     String                  100.0%
total:                                          (statements)            84.3%

```

## Re-cap
During this article we investigated the following
- Restructuring the directories to better accommodate the different coding requirements and to make this closer to the recommended *standard*
- Added a standard ID pattern 
- Saw how to truncate to to a unit of time (minute in our case)
- Had a quick look at the `init()` function
- Added code to manipulate the readings by ID
- Built unit test 
- Made use of Logging in the test framework
- Found out how to run test coverage reports

## Next Steps...
As this document is already long I will add the REST discussion into the next blog


