---
title: About me
subtitle: Why you'd want to hang out with me
comments: false
weight: 10
bigimg: [{src: "page/about/bigimg.jpg"}]
logo: "page/about/photobooth-crop.jpg"
---

I am Andrew Colvin therefore I might be me, but my wife reckons I'm a geek.

I was discovered in 1964 so for many of you reading this that makes me vintage.  I like to think it makes me a better story teller if only I could remember them.  Having been dragged up in the East End of London I have since lived in different locations around the UK on my life's journey.  The longest time in one place has been in Hampshire but my favourite was on Dartmoor (such a shame the job market at the time was dead).

I am married with 2 grown up children (two girls) and at present we all live in the United Kingdom (how united it will be after Brexit will be interesting to live through; almost the opposite of the German Unification).  We live in exciting times full of change and opportunity.

I have 3 dogs, whippets and again girls so I am most definitely outnumbered. 

## Education

- I have a PhD in Mathematics from Queen Mary College, University of London; _Using Computer Algebra to find Right Equivalence Mappings for Catastrophe Unfoldings_
- A BSc(Hons) in Mathematics
- GCE A Levels in Maths, Physics and Chemistry
- Various GCE O Levels

I have a natural curiosity to learn things new.  Even today I like to push, dig and delve to increase my knowledge of whatever subject.  This may just be for my own enjoyment at the time (for example installing and building clustering and container solutions with pacemaker, lxc and then docker long before the current drive to containerisation in the industry).  

## Interests

My interests from an early 14 years have been into digital electronics and then computers.  I built my first computer from TTL Gates with a 7 digit LED display at the time and at age 15 it was a pretty good programmable comptometer.

At 16 I purchased a Sinclair ZX80 in kit form and had to make lots of repairs to the printed circuit board to make it work; looking back I can't believe the poor workmanship of the boards dispatched.  My ZX80 still works and I boot it up for nostalgia every now and then.  

I got into computer properly at university and later with the Atari ST helping to drive forward [MiNT](https://freemint.github.io/) a unix like Operating System with eventual multi-tasking UI. During that time I developed a colour animated icon editor for the Atari and was talking to a publisher but felt they wanted to charge too much so eventually placed it in the public domain.  I also created and released an early GEM email client for MiNT which I release into the public domain.

When Linux started I jumped onto this when I got my first 386 and then Pentium PC and for a while was dual booting Windows 98SE and Slackware.

I quickly moved to running SuSE on version 6 exclusively and now [openSUSE](https://www.opensuse.org/) (looking back at the history of SUSE linux this was just around the millenium.   I have many computing interests:

- Computing Technology
- Linux
- Proofs of Concept using New Open Source Projects
- Gaming (Simulations, Building and Management genres mainly).
- Writing Software

### Getting out and about

I have a few canoes and like to go canoeing with my family.  We tend to take the inflatables with us on trips as they pack into the boot of the car.  This is a picture of us paddling across the [Pontcysyllte Aqueduct](https://www.pontcysyllte-aqueduct.co.uk/)

{{< figure src="1505557129708.jpg"  link="https://www.pontcysyllte-aqueduct.co.uk/"  >}}

From the age of eleven I fenced.  My favourite weapon has to be foil.  I find that <!--epee--> &eacute;p&eacute;e and now sabre have both lost the fineese that foil offers.  During University and Naval Career I ran the fencing clubs trying to get more people interested in the sport.  Unfortunately, I now live in a location where the closest club is too far and therefore my fencing days have come to an end.  

I met my wife at a scuba-diving club, another activity I used to do in my early years.  I still take my snorkelling kit on holidays when I go abroad; this is the extend of my diving nowadays (warm and clear tropical waters).

From the age of 13 to 18 I used to sail dinghy boats (Bosuns, Wayfarers, 420s, 470s and Fireballs) using the London Borough provided facilities at a local reservoir.  Unfortunately, for cost reasons my sailing stopped until a few years later I did a little with a friend on his yacht until he decided to just sail himself wherever life took him (real admiration).  I then picked up a little whilst in the navy.  Nowadays, my water activities are getting caught in the Rain whilst walking the dogs or canoeing.

I like my sci-fi and fantasy!  Generally I only read this type of fictional books.  Most of my books are listened to in the car whilst I am driving. I have tried books on superstrings in the past using text to speech but maths doesn't really work so I stick to fiction.  Listening whilst in the car is great; it is generally better than listening to the radio and music or news.  This way I get through lots of books effortlessly and it is like Jackanory ()[if you haven't heard of it click here](https://en.wikipedia.org/wiki/Jackanory)).



## Career

After my PhD (well before I completed writing it up; which delayed me finishing for years) I joined the Royal Navy as an instructor Officer (a branch that no longer exists).

I had various roles in the RN:

- Teaching Radio Electronics
- Writing an HNC course module and teaching electronic engineers digital electronics culminating with us building a simple microprocessor out of gates on breadboards.
- Lecturing Maths at degree level to engineers at the Royal Naval Engineering College, Manadon.  This has now closed and the site turned into housing
- Running a course on [Z-notation](https://en.wikipedia.org/wiki/Z_notation)
- Running and Managing the IT system at RNEC
- Building and Running the IT system at HMS Raleigh with a staff of 2 senior rates and 2 civil servants
- Built a Training Management System in 3 months just before I left the Royal Navy when the Instructor Branch was _extinguished_ as I did not want to become an engineer.

My Path led me to joining CSC as a Consultant when I left the RN.  I have had a varied career through my time at CSC (now after the merger with HPES, DXC) and generally have met some great people and had fun which is all that any of us really want from life.  

I have worked in various industries and performed many roles:

- Design and Development Lead for a Rapid Application Development Project for the Army Logistics Corp where we built a logistics modelling and analytics tool.  I had a team of 13 developers and 2 designers one being from the Army and work alongside two empowered users.  It was my job to lead and plan the teams development activities between each progress workshop and to run and present the changes and capture future requirements coming out of the workshops.  I had the other interesting job of accompanying the army on exercises in the field which included NATO exercises helping the divisions and brigades use the software to better manage their logistic through modelling
This was an Oracle Forms Project which was interesting and as a client/server thick client (the version we used was prior to its web incarnation) suited RAD perfectly.

- Siemens (and then BAe Systems); this was a continuation from the previous MOD project in that the MOD wished to transform the Oracle Forms application with the new _and sexy_ COM/DCOM solution being built for the combat arm.  This was the sister project to the logistic solution we had already built that had got bogged down in requirements capture within the military whilst we had struck out with RAD.  My role on the account was two fold: run the CSC team of designers as the lead consultant and be responsible for designing the UML models and documentation with my team for all the logistic requirements to be built into the new application using its design structures and frameworks.  Later I was asked by both BAe Programme Manager and the Army Colonel to step into the role as their Deliver Manager working closely to specify and agree the target contents of each workstream and the Releases overall in this DSDM Programme.

- Sainsbury's Taste for Life (eventually taste.co.uk) as the technical lead and development manager.
    + This was the social web presence for Sainsbury's and was during the _dotcom_ bubble. We developed, built and hosted the website providing content management processes and staging environments with automatic deployment to production on publishing.  The solution was built around Oracle DB/Weblogic for Persistent EJBs (ye it was the start of the J2EE era) and ATG Dynamo for the Servlet Engine and Personalisation Workflows.
    - During this time we also integrated via lose coupling the Carlton Recipe Site so that the two sites were linked as one host and url with a consistent look and feel and proxying between the sites through a _new and radical_ Alteon AceDirector Layer 5 switch (a rather new concept at the time).

- SITA; I was responsible for the Technical Architecture and the environmental design and delivery of a new Siebel CRM and Order Management System which was hosted in the client's datacentre and delivered worldwide across WAN, ISDN and for some locations analogue modems using CITRIX Metaframe (as it was then) for those low bandwidth locations.  The solution was a standard client server model and I delivered a remote package delivery solution using Windows NT BAT scripts to install the Siebel client application as well as set up the user without having to visit each site around the world to install the solution.  The delivery of the server into the different formal environments was fully automated by a set of scripts that I built. Until I wrote these scripts the process was done manually and it was difficult to predict the outcome due to the risk of missing steps.

- Allders; I completed multiple roles on Allders.  
    - The first was as the senior designer for an integration hub between a new Warehouse Management System and the Legacy IT systems.  On completion of the integration hub functional design and build I took on the responsibility for the non-functional requirements ensuring the solution met the perfrmance characteristics required.  A graduate and I built a menu based failover solution that could be triggered from the active or passive infrastructure and would switch the active nodes automatically backwards and forwards as required without losing data.
    - The second role saw me producing the integration hub business flows to integrate the the merchandising system that was being delivered so that it was integrated into the new WMS and legacy merchandising system.  This included 3rd party applications and over a dozen internal systems with messages having to be restructured, transformed and delivered to multiple systems and maintain transactional integrity or understand when replays were necessary or of course compensating transactions.
    - The third of my roles was an interesting departure in that it found me pick up producing the go live stress test of the new Warehouse Management System working with the warehousing director and her staff to ensure the solution, which included wireless handheld computers could direct the staff to the correct Bins and that the solution was able to manage the throughput especially with additional employees required during peak seasional periods.
    - The fourth and final role found me becoming the client's deployment manager for the final 2 warehouses responsible for the planning of all aspects of delivery into the warehouses.  This included staff training, wireless installation, product purchasing, system enrolment, initial stocktake of the warehouses onto the system and purchasing of handsets for the operatives.

- Vodafone; I was responsible for the investigation and feasibility of delivering changes to the "Pay as you go" system to support a new product enhancement for the product line.  My responsibilities fell into the architectural space requiring me produce the architectural specifications of the changes and high level designs and interface specifications.  I identified that the only solution available within the PAYG systems required 11 separate network calls per transaction for the solution because the back end VAX system was now capable of the changes directly.  I then proved that the network would not be able to cope with this impact without changes to the system to provide a function to merge the majority of the 11 calls into one.  The client team responsible for this system were reluctant to take on this work due to the cost of the implementation but in the end the solution was delivered successfully without failure and the product changes tested using the 11 calls as a proof of concept with controlled numbers and restrictions on the volume of changes per month until the full roll out.

- BT Wholesale;
  - I started with BT Wholesale looking into their tactical highly manual SDSL fulfilment process to automate the IT, ATM Networking Changes as well as DSLAM Port Configuration leaving only the frame and copper implementation remaining as manual. The design of the solution was created meeting BTWholesales requirements, which where somewhat more complex than the tactical product they had live, but the calculated return on investment made further delivery of the product unfeasible. I was, however, ask to design the migration of the tactical scripts, etc., that had been created into their existing ADSL fulfilment system.  I stayed on this project through much of delivery interleaving this with other pieces of work.
  - The next major project BTWholesale asked me to pick up the design for a bulk speed upgrade solution. The reason this was a major departure for the existing solutions is that they were capable of under 1000 fulfilment changes per night.  This system allowed us to process up to 10000 ADSL speed upgrade from 0.5 Mbits/s to 1 Mbits/s each night. This solution was built with me a tester and 3 developers and integrated into the backend ATM fulfilment systems and legacy line management mainframe.  This was delivered in 2 months using a RAD approach with additional requirements and progress demonstrations occuring every week.
  - After this bulk upgrade which removed the 0.5 Mb/s product from BTWholesales portfolio I was asked to design a stop not cease end to end solution. The issue BTWholesale was trying to solve was twofold: engineers making visits to exchanges which was unnecessary and delays for customers receiving broadband during a cease and reprovision at a premises. The stop not cease allowed the fulfilment systems and line mainfram to retain the DSLAM port frame presentation and physical multiplexing to the customers line Until a later date when it was deemed that the port was not going to be provisioned when they were bulk released in one visit. This was a huge saving to BTWholesale n cost and a massive customer improvement as ADSL provisioning became smoother for the majority of customers
  - I was then asked to look into the possibility of uncapping every ADSL line, what we all know nowadays.  The requirement was to uncap 30000 lines per night. I identified a major issue in that the majority of the VPs on the core ATM backbone would become saturated. A two pronged attack was then undertaken: An offline analysis of VPs and therefore lines that were ready to be migrated.  This was fed with Broadband Supplier migration dates so that we had a pool of lines to work with each night. These lines where then fed into a beefed up and slightly tweaked bulk upgrade solution (now coined FastPeter) so that we migrated 30000 lines every night.  There we few failures from this process and most were caused by records not matching between the line and Broadband record systems and as each line was being upgraded meant the databases got corrected as a by product.
  - I next help BTWholesale product team design the requirements for their early 21CN solutions. This was pure product developments and requirement specification.  Later I went on to design a tactical fulfilment workflow using their current 20CN fulfilment solution so that could successfully trial prior to a new fulfilment solution was delivered (some years after I ended my consulting time with BTWholesale).
  - The final major programme I undertook was the enforced separation of BTWholesale from Exchange and Line Engineers (the birth of OpenReach). My role was the lead architect for BTWholesale working alongside the CTO from BTGroup to deliver the split of sysem and data. To achieve the migration on the legal data I again turned to FastPeter and the team to provide me with changes on data separation. The more important part of my work was designing the changes to the systems and processes, working with the BT Retail and Openreach Solution Leads, so that these systems did not see other entities data on any shared systems and to use the publiclly exposed APIs for Openreach and for BT Retail to use the same public interfaces as other BB Supplier. This separation [now history](https://en.wikipedia.org/wiki/Openreach) was a massive undertaking for BT and at the end I was producing reports for Ofcom on progress and estimations to complete. I was asked by BTGroup to present to Ofcom on their behalf but unfortunately my time with BTWholesale ended just prior to this date because my skills were needed on another account and CSC did not want to extend me further beyond the end of the contracted time.

- Zurich Insurance;
  - I joined Zurich Insurance for a 4 week period to facilitate a handover between an outgoing change manager and an incoming one where their programme would have a gap in role.  What I discovered was that the change manager was more like an integration lead taking the code delivery from offshore repackaging and configuring for all environments and then raising the paper work and attend change calls to get the delivery deployed into the integration environment.  This should have been a simple enough task but the packaging of the code was all completed by hand!  After my handover by the out-gong contractor and attempting to package the first delivery I decided this is stupid.  I therefore wrote a set of Bash scripts that took the code base and built the packages for environments making all of the configuration changes and structural changes required for the selected environments or all environments.  This complete repackage for all environments picked its configurations up from reference files so the data and scripts were managed separately.  This build then ran without error and created all artefacts for all environments in under 5 minutes saving over  2 days effort for each environment.  
  - Once I handed over the new change process to the replacement contractor I was asked to stay on and take the role of the Programme Release Manager.  I was responsible for letting all work to the different 3rd parties and managing their deliveries as well as managing the integration into each of the environments up to production.  I was working alongside the Project Manager (both of us answerable to the director) to ensure we had successful delivery of the solution so that the programme met its timescales.   
  - I stayed in this post getting the first release live when I was called up for Jury Service which was forecast to run for between 4 and 6 months.  

- Department of Health; Following on from Jury Service I was asked to go into the Department of Health to rescue a failing project.  This was the DoH new intranet solution that was having issues with its single sign on solution as well as its clustering.  I started with a full drains up of the architecture and found that there where network paths missing in the solution.  The filesystem clustering needed for the back end content management was missing network cables at build.  I then moved onto the single sign on.  This all seemed to be correct and I struggled to understand why this was not working. It was eventually tracked down to a multiple of reasons.  The kerberos principle name was deployed multiple times but the standard AD GUI only shows the last but the implementation used the first! The AD structure was poorly maintained and users and their machines were not in the groups as they should be.  This facilitated a clean up process with only the Accessiblity Requirements needing a separate solution as the standard Policies stopped the specialised equipment.    

- IPS/HMPO/HO; I have completed multiple roles on this acount and a few are listed below.
  - I joined the IPS account as an  architect who was responsible designing the checking service which would allow calling systems to verify identities and check for eligibility and entitlement to obtain products. This was designed as a generic framework with pluggable modules that would provide for future products, rule sets and data sources. The workflow was exploded into many independent subprocesses and control was then built into the rules set which would decide on the next subprocess based on pevious results and the state of the data retrieved for this request. This rules set also had calls to other services built in that had a standard interface to make even this extendable. The first version of this was produced that could deliver the eligibility and entitlement for Passport requests. This solution never sure production because the incoming government cancelled the identity card programme. The client and the account both renamed ourselves as HMPO and HMPO account and our requeirements changed to deliver a replacement passport production system. Our responsibilities covered the online presence, application processing, long term  data storage and desktop support; everything up to the actual production of the physical book.
  - The new requirements removed the necessity for the checking service so I stepped up and took the role of solution architect responsible for the design and build of the production environments, which had to meet very high security constraints as well as four 9's availability and 24 hour disaster recovery (we looked into active-active but the security requirements blocked this solution).  The solution was highly compartmentalised and connected from the internet with its customer portal, through the business to process the application which included scanning application forms and a web application for all user processing, to the backend storage and delivery systems. This solution had to allow many integration points with other systems in a secure way such that we were protected and we also met their security requirements.  I was also responsible for working with the client security team, our security team and also CESG to get architectural sign-off for the solution which meant every data flow had to be documented through the system and what protections we intended to put in place, including all of the system and service management data pulls and monitoring and feeds to the Security Operations Centre. During this process I was also responsible for design and delivery of the System Test Environments that would model the new production environments so all aspects of the system could be tested before delivery.
  - Once these environments were delivered and ready for the application to be deployed I picked up the role of non-functional architect with a team of developers and performance engineers and performance testers to get the solution working to meet the non-functional requirements of the solution. This was a big ask for a number of reasons but the key ones were: over complicated environment driven by security requirements, developers ignoring non-functionals and mainly too many read the book purist architects that thought every function could be delivered as a soap service! This meant I had to migrate many of the services into libraries, repackage and restructure others and just rewrite most to make them meet performance characteristics. I also had to look at the system and service management to prove that the solution was "supportable" as the developers had typically ignored the supportability of their solutions in production. A whole logging and error handling framework was designed and built so that this was all captured and presented and errors coule be replayed with or without data correction. This had to, of course, be built with role based access allowing different access to data and function. 
  - When we went live I picked up a temporay role to assist the transition period into full application support. This temporary role was very hectic as real life legacy data threw up many issues along with the elderly systems we had to integrate into. I also had to deal with instability of the new system which I eventually tracked down to be a few COTS product issues that would get triggered at different times. Once these where fixed, which took a year to fully resolve, the system was capable of handling the business requirements and increased the business throughput but also allowed them to deliver more checks in the same processing time. 
  - At the end of this period I was asked to stay on as the solution director's deputy and work as the Delivery and Service Architect closely with the Service Team into the account management. 
  - I moved to the client side after about a year as a consultant working into the client's Director and Programme Leads for Strategy and Change within HMPO as well as their Technical Design Authority and Business Development Group. I took on many tasks here and help define improvements and how we as a joint team could deliver change that would benefit the business. I also wrote the requirements for a large change that had to go out to tender. 
  - After 9 months I returned to the account and took on the role as Account Solution Director when my predecessor moved to a new account. This meant I now had management responsibility as part of the Account Team for delivery of account objectives as well as servicing the customer needs and ensuring the governance of all architectural aspects of the account.
  - On the birth of DXC (the merger of CSC and HPES) the HMPO account and the multiple Home Office services delivered by HPES were merged into the Home Office Account and I picked up role of Chief Technology Officer with the architectural governance responsibility across all solutions. This is the role I perform today working alongside the rest of the account governance team and working with the Account General Manager for new growth opportunities and to the Service Delivery Lead who has delivery responsibility into the wider DXC. One of the main deliveries recently has been the negotiation of a further year of operation of the HMPO contract and structuring it such that parts of the contract can be terminated as and when the client's replacements are ready as our solutions are now 10 years old. 

## Technology, Toys and just Plain Interest

I have put this section here so that the reader can get an idea of the different technology areas I like to play with. Those areas where my knowledge has become stagnant are in italics.  I have only listed the main 

### Programming Languages


These are the languages I have used in anger:
*Fortran*, *LISP*, *Forth*, C, *C++*, *Oracle Forms*, Java, Go, SQL \

### Design

UML, *BPMN*, DB Models, SOAP, REST, platform design \
CI/CD pipelines \


### Platforms & COTS

Oracle, MySQL, Postgres \
J2EE, Weblogic, *Websphere*, *Dynamo* \
LDAP \
Pacemaker, KVM, DRBD, Gluster, LVM, Linux (RHEL and openSUSE), containerisation (docker, containerd, cri, k8s) \
git, gitlab, github, jenkins \
doc as code (latex, markdown, mkdocs, hugo, mermaid, plantuml) \
ansible

### Toys

Raspberry PIs \
KVM Farm with hand crafted clustering (just because I can it is good for learning) \
Scripted backup of all running VMs without downtime.
Synology NAS, FreeNAS,  My latest storage project has been to use a dedicated USB 3.1 Gen 2 (10GB/s) card and Storage array to provide a fast raided disk array - faster than SATA and SASS

