---
title: Andrew's System Analysis Program (ASAP)
subtitle: Analysis and Monitoring of Instrumented Systems
author: Andrew Colvin
date: 2019-06-18 
toc: false
#weight: 120
comments: true
tags: ["ASAP", "Performance", "java", "Monitoring", "Analysis", "Real Time"]
bigimg: [{src: "page/asap/asap-overview/bigimg.png"}]
layout: article
previous: ""
index: ""
next: ""
logo: "page/asap/asap_start/splash.png"
--- 

Andrew's System Analysis Program (ASAP) was born out of a need to analyse data from performance test runs and to determine which companents of a system was performing below par and which parts should be adjusted, scaled, re-coded or just plain re-architected. ASAP additionally provides a real time monitoring capability to provide all of its analysis running in a live system.  ASAP has been running in a highly complex system in live for the last 6 years providing real time feedback of the running system to the engineers and service managers on the ground and has allowed us to identify root cause of failures fairly fast by pin pinting components as they went under stress.

<!---more-->

# Andrew's System Analysis Program (ASAP)

ASAP Provides the following capabilities:

- Load log data for performance information
- Monitor log data in real time for performance data
- Transfer statistical to single collection points for ASAP
- Analyse
  - Slowest Service
  - CPU Hoggers
  - Statistic distributions
  - Dwell times of components
  - Build a component/Service Call Map (if the data is available)
  - Provide real time performance graphs down to individual components on individual servers
  - Provide minute by minute aggregated time graphs (ie showing peak hours etc).
- Simulate Expected Business mprovements by making changes to individual components and thereby estimate the cost benefit of proposed changes

There are a few additional tools provided in the tool bag:

- A System Simulator that can mimic component botlenecks a resource pools usage to allow the engineer to tweak their resouce constraints to provide enough but not over compensate.
- A loader to provide bulk loading of directories worth of data into the database
- A Collector that can transmit incoming data to multiple ASAP end points and also on request replay data to an individual ASAP either in slow motion and up to as fast as it can go.
- The ASAP Jar File also has a test generator packaged into it to generate either randomised normally distributed data points or log normally distributed data points as specified on the command line

## License 

This software is currently licensed under the GNU Public License V2

## Getting the Software

The latest release is available from the [project's release folder](https://git.acolvin.me.uk/andrew/ServiceAnalysis/releases).  This software is now compiled for JDK 1.7 and above and has extensive use on 7 and 8, moderate usage on 9 and light touch usage on 11 and 12 by me.

## Thanks...

I would like to thank the authors of the following products without which the production ASAP would have been much harder than it could have been:

- Oracle (and as was Sun) and the whole openJDK community for the awesome Java Platform
- Apache for the [Batik SVG Framework](https://xmlgraphics.apache.org/batik/)
- Apache for the [commons-io library](https://commons.apache.org/proper/commons-io/) 
- Apache for the [Derby Database](https://db.apache.org/derby/)
- iText for PDF generation (1.4 version which was released under LGPL)
- JFree.org for [JFreeChart](http://www.jfree.org/jfreechart/) which is used for all the graphs.
- Mongo for the mongoDB nosql database (used as part of the external collector)
- The SLF4J organisation for [slf4j](https://www.slf4j.org/)
- SwingX very old library which I started using when this project started on JDK 1.5 and some functions are still used.
- Oracle again for their [free to use Spring Utilities Code](https://git.acolvin.me.uk/andrew/ServiceAnalysis/raw/master/SystemSimulator/src/systemSimulator/SpringUtilities.java) where it is used to build the dialog windows inside the simulator with the newer spring layout manager
- Finally, I would like to thank all the people on StackOverflow for take those difficult steps before me and I am able to pick up understanding to get me through a sticky patch...


and of course my family who only ever saw the back of my head for most evenings and weekends whilst I was working on this.

## Source 

This is now hosted publically in my gitlab service at [https://git.acolvin.me.uk/andrew/ServiceAnalysis](https://git.acolvin.me.uk/andrew/ServiceAnalysis), see more about my service [under resources]({{< ref "resource" >}})

Note that the code for this is fairly old and was built as need drove. See the `build manual` (change this to a reference when section is written) 

## Found an Issue?

Why not raise it [here](https://git.acolvin.me.uk/andrew/ServiceAnalysis/boards).  You do not need to log on for this.

## Manual 

Currently there is an [oldish manual](https://git.acolvin.me.uk/andrew/ServiceAnalysis/tree/master/manuals) available in my gitlab repository in ODT format.  As I produce a new version this will move to markdown and be linked in this section.

The product suite provides multiple tools in its arsenal and the index of manuals can be reached by pressing this button:
{{< button href="manuals.md" text="manuals">}} 

### Overview of Suite 

1. The ASAP Client 
    - This is packaged as TibcoServiceDependency.jar 
    - Acts as a gui or headless
    - Has a random event generator with produces a distribution that is either normal or log-normal with a given mean and standard deviation.
2. UDPAgent which collects data from a sngle file and teansmits on a UDP port
2. JMSAgent which does the same as the UDPAgent but adds its events onto a JMS queue
3  UDPDirAgent which collects data from all files matching the given suffix under a directory and transmits all events to a host/port combination using UDP
4. TCPDirAgent which watches a directory for all files with a name with the given suffix and waits for connections from ASAP clients
5. The Collector receives data from multiple agents simulataneously and can feed multiple clients with this data and thereby behaving as a broker.
  - The collector is capable of storing all of the events and can play these back to a client on request.
6. Simulator which provides a graphical simulation to model a systems bottlenecks and processes.
7. A Database Loader, DBLoader, which can bulk load data into the collector
8. PasswordHash which generates hases for passwords to bulk set accounts

The following diagram shows how the different agents and files support the functionality in the ecosystem

{{< mermaid >}}
graph TD
subgraph Collector
T1>TCPDirAgent] -->|feeds| C((Collector))
T2>TCPDirAgent] -->|feeds| C
T3>TCPDirAgent] -.->|feeds| C
C -.-> A4(ASAP)
C -.-> A2(ASAP)
C -.-> A3(ASAP)
style C fill:#ffffff
end
subgraph Agents
UA[UDP Agent] ---|tails| LF(Log File)
UA -->|feeds|A
A[ASAP] -->|Read File| B(Log Files)
A -->|Tail|B
A -->|pops| Q[JMS Queue]
J(JMS Agent) -->|posts|Q
J -->|tails|f(Log File)
tt1>TCPDirAgent] -.-> A
tt2>TCPDirAgent] -.-> A
style A fill:#f9f
style A2 fill:#f9f
style A3 fill:#f9f
style A4 fill:#f9f
style T fill:#ffbf80
style T1 fill:#ffbf80
style T2 fill:#ffbf80
style T3 fill:#ffbf80
style tt1 fill:#ffbf80
style tt2 fill:#ffbf80
style dir fill:#80ffff
style sub fill:#b3ffff
style B fill:#ccffff
style f fill:#ccffff
style LF fill:#ccffff
style B1 fill:#ccffff
style UA stroke-dasharray: 5, 5
style J stroke-dasharray: 5, 5
style U stroke-dasharray: 5, 5
end
subgraph Directory Agents
dir{directory} ---|contains| sub{subdirectories}
sub ---|contains|B1(Log Files)
T>TCP Dir Agent] ---|watches|dir
U>UDP Dir Agent]---|watches|dir 
T --> A
U --> A
end
{{< /mermaid >}}




## Videos and Presentations

There are a number of instructional videos I made on an old version of the software showing its operation. These can be found in on the playlist below and present a 5 minute view around each topic. As you can see the product started with the name of Service Performance Analysis Tool but its acronym meant people `spat` everywhere and hence the rename. Note that the jar file is built as `TibcoServiceDependency.jar` and denotes the original function which was to analyse TiBCO Business Works statistics and show this on the dependency view.

<iframe width="640" height="360"  src="https://www.youtube.com/embed/videoseries?list=PL58ECF02147409C08" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>




### Early Presentations

The presentation below is from an [early "What and How" presentation]({{< ref "sa-pres.md">}}).  The link presents a gallery of the individual slides so that they can be studied. 

{{<revealjs theme="black" progress="true" controls="true">}}

![](../sa-pres/sapres1.png)

---

![](../sa-pres/sapres2.png)

---

![](../sa-pres/sapres3.png)

---

![](../sa-pres/sapres4.png)

---

![](../sa-pres/sapres5.png)

---

![](../sa-pres/sapres6.png)

---

![](../sa-pres/sapres7.png)

---

![](../sa-pres/sapres8.png)

---

![](../sa-pres/sapres9.png)

---

![](../sa-pres/sapres10.png)

---

![](../sa-pres/sapres11.png)

---

![](../sa-pres/sapres12.png)

---

![](../sa-pres/sapres13.png)

---

![](../sa-pres/sapres14.png)

---

![](../sa-pres/sapres15.png)

---

![](../sa-pres/sapres16.png)

---

![](../sa-pres/sapres17.png)

---

![](../sa-pres/sapres18.png)

---

![](../sa-pres/sapres19.png)

---

![](../sa-pres/sapres20.png)

---

![](../sa-pres/sapres21.png)

---

![](../sa-pres/sapres22.png)

---

![](../sa-pres/sapres23.png)

---

![](../sa-pres/sapres24.png)

---

![](../sa-pres/sapres25.png)

---

![](../sa-pres/sapres26.png)

---

![](../sa-pres/sapres27.png)

---

![](../sa-pres/sapres28.png)

---

![](../sa-pres/sapres29.png)

---

![](../sa-pres/sapres30.png)

{{</revealjs>}}

