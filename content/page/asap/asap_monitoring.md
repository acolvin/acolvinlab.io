---
title: ASAP Monitoring
subtitle: Live Monitoring of a System's Performance using ASAP
author: Andrew Colvin
date: 2019-07-04
toc: true
tags: [ASAP]
layout: article
previous: "asap_analysis.md"
index: "manuals.md"
next: ""
#draft: true
logo: "page/asap/asap_start/splash.png"
---

This manual will describe the capabilities of ASAP monitoring of currently running systems so that adjustments and issues can be identified before they become business and customer impacting.  

<!--more-->

# {{% param "title" %}}
**{{< param subtitle >}}** \
by **{{< param author>}}** \

## Introduction

This manual will walk through the abilities of ASAP to provide live monitoring of a running system so the engineers can spot when things are becoming out of the norm and the system could be heading for catastrophic failure or start to improve. This may appear in many different patterns such as complete and instant lose of all transactions or a gradual increase in response times from one component which impacts upstream until transactions start to time out or users stat retrying transactions.

To achieve these monitoring functions ASAP needs to read log files (those that contain the performance data) in real time.  This is achieved either by pointing ASAP directly at the log file to monitor or feeding it with data from agents across the systems ecosystem either directly or through a series of collector agents. This article will start with the simple tailing of logs and discuss the functions and then have a brief look at the agents and collector.

### Simulating a System for Testing

ASAP comes with a system simulator that allows you to simulate the performance of a system including potential bottlenecks and monitor improvements as you tweak the model of a live system. This article is going to use this simulator to generate test data.  The figure below shows the simulator window with a running simulation

{{< figure src="simulator.png" caption="Simulating a System of Web Calls" class="no-photoswipe">}}

There is also a separate ability to generate randomised data that fits either a normal or log-normal distribution with a defined mean and standard deviation. This version is packaged directly in the main jar file `ServiceAnalysis.jar` or `TibcoDependencyAnalysis.jar` and run using:

```
java -cp TibcoServiceDependencyAnalysis.jar TibcoAnalysis.RealTimeReader.Test.BWStatsGenerator <options>
```

The options are:

Option | Description
-------|-------------
`--mean n`| Sets the mean of the random set of data
`--stddev n`|Sets the standard deviation the the randomised data sample
`--service s`|Sets the service name for the output log data
`--operation o`|Sets the operation name for the output log data
`--file f` | The file to write the log data in to
`--normal` | Use this option to force a normally distributed sample set instead of a log-normal set 
`--delay n`| The delay in milliseconds between each generated sample

## Monitoring a File Directly

When ASAP is running directly on the server the log files are generate or has direct access to the live log it can be set to tail the log files to monitor in real time.  This is achieved by selecting the menu entry *[Real Time | Monitor File]* and selecting the file to monitor on the filesystem. Just like the *[File | Load File]* seen in the Analysis section. The simulator is generating output in a file called `timings.csv` in the directory that the simulator was started in. This file looks like this 

```
MODEL,BW,Database,Proc1,,,,,,
MODELEVENT,BW,Database,Proc1,2019-07-04 18:49:09:569,66,0,sim::4--sim::5--sim::10
MODEL,BW,WebServer,Button,,,,,,
MODELEVENT,BW,WebServer,Button,2019-07-04 18:49:09:560,144,76,sim::4--sim::5
MODEL,BW,Database,Proc1,,,,,,
MODELEVENT,BW,Database,Proc1,2019-07-04 18:49:09:704,39,0,sim::2--sim::23--sim::24
MODEL,BW,WebServer,Button,,,,,,
MODELEVENT,BW,WebServer,Button,2019-07-04 18:49:09:704,134,39,sim::2--sim::23
MODEL,BW,Database,Proc1,,,,,,
MODELEVENT,BW,Database,Proc1,2019-07-04 18:49:09:839,38,0,sim::1--sim::25--sim::26
MODEL,BW,WebServer,Button,,,,,,
MODELEVENT,BW,WebServer,Button,2019-07-04 18:49:09:839,138,38,sim::1--sim::25
MODEL,BW,Database,Proc1,,,,,,
MODELEVENT,BW,Database,Proc1,2019-07-04 18:49:09:977,12,0,sim::6--sim::27--sim::28
```
It carries timing and structural data as we will see.  Start the monitoring... This image shows the file being monitored, there will be a list and clicking an item in this list will terminate the monitoring of the file, and the data being loaded behind in the table.

{{< figure src="sim2.png" caption="List of Monitors" class="no-photoswipe">}}

From the table we can see the mean and other values in real time such as the wait times.

### Additional File Monitoring

Above the document described how a single file can be monitored, however, we often have  whole directories, list of files, and files that roll there filenames. To handle these situations ASAP offers the ability to load a file containing a list of filenames to monitor (one filename per line). This list is loaded using the **[Real Time | Load Monitor File List]** menu entry. 

To get dynamic directory monitoring the engineer has to use the command interface. The other functions are also available from the command interface

Command | Description
---|---
Monitor <file> | Start monitoring the specified file
Monitor Remove <file> | Stop monitoring the specified file
Directory Watch <Directory> <extension> | Watch a directory for all changes to files with the specified extension
Directory Remove <Directory> <extension> | Remove the specified Directory Watcher
Directory clear watchers | Remove all Directory Watchers

Therefore the following `COMMAND: Directory Watch "the directory the simulator was started in" csv` would have automatically detected the log file as soon as it changes and set a monitor process to start tailing it.  If any other csv files are being written to in this directory (or its sub directories) they also would be monitored for recognised performance log data.

## Monitoring with Graphs

The table carries lots of data but it is just a table. Lets view the data as a time graph using *[Real Time | Real Time Graph]* with the UI.Button row selected (note the graphs can be started in a right click menu or the windows menu)

{{< figure src="sim3.png" caption="Time Graph for UI Button Press Response" class="no-photoswipe">}}

The x-axis of these graphs show time while the y-axis displays the response time on the left and rate on the right (transactions per second).  The graph travels with time as new events come into ASAP. If no data arrives the graph will be static. To have a rolling graph at all times select the *Active Axis* check box.  The range value is the time window in seconds that the x-axis display. 

The Graph has 5 separate plots:

- Service Time
  - Displays each transaction as time against response time 
  - This is displayed in red
- The yellow line is the wait time for each event. 
- The blue dots show the current overall average for this service since ASAP started to monitor
- The green triangles display the running average for the last 100 samples so show how the responses are varying and short term trends
- The final black line if the rate of calls for this function.

There is a drop down at the bottom right that shows all of the servers that are feeding this data. This allows the engineer to filter data from just that server.  Note when monitoring from a file it only shows `localhost` unless the log data can cnvey they host information and the reader conveys it.

In the next figure we can see the server response for this button press

{{< figure src="sim4.png" caption="Time Graph for the Server function for the Button Press" class="no-photoswipe">}}

and this graph showing a failure/timeout/stuck thread with the UI graph zoomed in showing stopped data flow at the end of its axis with the thread being timed out in the server a minute later.

{{< figure src="sim5.png" caption="Time Graph time out" class="no-photoswipe">}}

The final figure in this section shows all 6 services being monitored simultaneously

{{< figure src="sims-all.png" caption="All Time Graphs" class="no-photoswipe">}}

Overall the system looks to be running well, however, it is interesting that the *UI.Button* response time seems to be more than the sum of the others. This is something that would need investigating during your performance tests prior to go live but may of course only appear in live and during a heavy load. This short of behaviour can normally be put down to queueing if the wait time is excessive and in this case the wait time is over 2 seconds.

Looking back at the simulator it is easy to find the bottleneck (well the first) and that looks to be *Webserver.Button* as it is defined as single threaded. Raising the threads, i.e. the parallelism, will remove a clear block.  This component is attached to a *Router* which has a setting of 10.  This forces all components connected to it to obey the maximum number of processes that can run across all of those components.  

{{< figure src="sim6.png" caption="Blocking Component" class="no-photoswipe">}}

Tweaking the simulation shows a number od improvements 

- Running average over compared to the overall average has reduced
- Throughput has risen from 9 to 12.5 (we are processing more for the user providing a better experience or able to process an increased number of user requests)

{{< figure src="sim7.png" caption="Immediate Improvements" class="no-photoswipe">}}

Whilst ASAP is monitoring all the graphs are functional. This means we can look at the frequency distribution 

{{< figure src="sim8.png" caption="Simulation's Distribution" class="no-photoswipe">}}

The frequency graph has no consumer or dependent elements shown and this can be confirmed by the service table which shows zero in all the comments; this will be corrected later.  The quantised view can also be used

{{< figure src="sim9.png" caption="Quantised View" class="no-photoswipe">}}

This view is updated every minute with new data so the peak hour slowly moves through the day.  The above screeshot shows the reduced response times including min and max. The following image shows this graph zoomed in so that the minute event rate is clearly visible. 

{{< figure src="sim10.png" caption="Quantised View - Minute by Minute Volumes" class="no-photoswipe">}}

This shows events/minute change over time from 550 to zero then 750 and corresponds to the changing of the simulator above.  When the simulation is left to run for longer this wierd pyramid on pyramid changes shape as more samples fill the hour at each minute plot pointing

{{< figure src="sim11.png" caption="Quantised View - Minute by Minute Volumes as the Simulation Runs" class="no-photoswipe">}}


## Service Table and Monitoring

During live monitoring the service take updates in real time.  By default the table is in delayed mode which means it collates changes to fields and then draws them all every set period.  This refresh period can be changed from *very fast* to *Very Slow*; the default is *Slow* which is every 5 seconds. The fastest *very fast* is about 4 times per second and can put a fair load on the hardware running ASAP if there are hundreds of rows being monitored as in a real life system.  This setting is in the bottom left of Service Table Window. There is another option which is with every individual field being udated in real time. Because of the java technology and the fact this is doing field by field redraws this can be slower than *very fast* in some situations. To turn on real time updates right click on the table headers to bring up the table menu 

{{< figure src="st-rt.png" caption="Turn on Real Time Updates" class="no-photoswipe">}}

The option below this entry in the menu will turn the updates back to delayed.  Occasionally the table can do with being reset in its entirety and this is what *Reload Rows* does in the menu.

What can be seen from the above table view is that we have UI.Button cell coloured green. If a row in the table is clicked a different context menu appears with an item called highlight row. This provides two functions:

<html />1. A visual tracker for running average changes. It will go through Green to Amber then Red based on how far the short term running average (last 10 events) is from the current mean value. This gives a very fast visual indication of systems starting to get into trouble. 

 {{< figure src="highlight-amber.png" caption="Amber Alert" class="no-photoswipe">}}
 
<html />2. Filter the table view to only show the highlighted row. This is achieved through the menu entry **[Filter | Show Highlighted]**. Selecting the menu entry again will turn off the filtered view.

 {{< figure src="hl-filter.png" caption="Filtered View" class="no-photoswipe">}}
 
Other fields that can have highlights set are the count field and the average field. To do this requires the use of ASAP's command interface.  One way of using this is to open the *System.Out* window from the *window* menu and use the bottom input box.

The commands available are :

Command | Description
---|---
Highlight <field name> <layer.service.operation> <green> <orange> <red> | colour fields based on threshold values.  Fields can be "Rate", "Average", "Service.Operation" or "Layer". The colours are: uncoloured < Green Threshold < Green < Amber Threshold < Amber < Red threshold < red 

## Transaction Monitoring

Up to now the discussion around the monitoring has looked at graphs and tables around the statistics. This section is going to capture transactions and plot each transaction against the others. The test data being generated contrains transaction information

```
sim::4--sim::5--sim::10
```

This transaction data allows ASAP to structure the events records into parent and children. The transaction value doesn't need to be in this format and this can be seen in the [analysis discussion]({{< relref "asap_analysis.md" >}}). 

This structure is defined as: `[<event name>::<event number>]--[child event]--...` 

To start recording events we use the **[Real Time | Capture Events]**.  The storage of the events is very memory intensive and ASAP will monitor the memory footprint and stop capturing when a critical threshold is reach to not impact its function. Captured transactions can be removed when the engineer has completed their analysis by selecting **[Real Time| Clear Transactions]**. Captured Transaction can be viewed by selecting **[Real Time | View Transactions]**

{{< figure src="transaction.png" caption="Transaction View" class="no-photoswipe">}}

The list on the right is listing all of the captured transactions. This list can be filtered by the use of the 3 dropdown boxes above:

- top; Server
- middle; Log File
- bottom; Service

Selecting one of the transactions in the list shows all of the details for that transaction including sub-transactions if the can be linked together:

{{< figure src="transaction2.png" caption="Specific Transaction" class="no-photoswipe">}}

The transaction view shows that we have quite a lot of delay between the child service completing and the consumer picking up this message and processing the rest of the call.  This shows we are likely still resource constrained.  ASAP will allow the display of many transactions together as in this next view

The last function of this window is to use the dependencies built from the transactions to populate the service dependency matrix.  This is achieved using the Create Dependencies Button.  Under the button is a progress bar that shows how far through the process it is.  The ASAP suite comes with a library that generates the transaction information if you would like to create the structured details.  This isn't strictly necessary if you have other solutions in place.


{{< gallery >}}
{{< figure link="transaction3.png" caption="Multiple Transactions" >}}
{{< figure link="transaction4.png" caption="All Transactions" >}}
{{< figure link="transaction5.png" caption="Consumers and Dependencies Populated" >}}
{{< figure link="transaction6.png" caption="Service Dependency View after Population from Transactions" >}}
{{< /gallery >}}


## Alerts

ASAP has a rudimentary alert process that can watch for certain conditions and raise a text alert that can be monitored by other systems ie to generate sounds to draw attention. All alerts are managed through the command interface:

Command | Description
---|---
Alert add <layer> <service> <operation> <threshold>| Set an event alert to a service at specified threshold
Alert del <layer> <service> <operation>| Remove any event alert against the specified service
Alert clear| Clear all alert rules

These alerts are very simple in that events that are above the threshold generate an alert
Here is an example alert generated by the command `COMMAND: Alert add BW UI Button 2300`:

```
ALERT:Fri Jul 05 17:49:32 BST 2019:UI.Button:null:2581.0
```

## Tracing User Interactions

As each event into ASAP can carry user information it is possible to get ASAP to track the statistical data for the specified users. This is again completed through the command interface. The commands that support this function are:

Command | Description
---|---
UserWatch <name>| Output all user tagged actions <name> is a regexp so .* means everyone
UserWatchOff| Turn off all user watching
ListCapturedUsers| Lists the captured users
PrintUserEvents <user>| Prints the events collected for the specified user
SaveUserEvents <user regex pattern> <file>| Save the users events to a file

The command `COMMAND: UserWatch BEL_CASH.*` will start to watch all user transactions identified by user's with a username starting BEL_CASH.  The captured users can be listed 

```
COMMAND: ListCapturedUsers
BEL_CASH3
BEL_CASH10
BEL_CASH13
BEL_CASH1
BEL_CASH12
BEL_CASH7
BEL_CASH8
BEL_CASH5
BEL_CASH6
BEL_CASH9
```

We can now print transactions for a user

```
COMMAND: PrintUserEvents BEL_CASH8

BEL_CASH8 | 1331654942348 | 1331654942955 | 13/03/2012 16:09:02.348 | 13/03/2012 16:09:02.955 | 607.0 | UI | PORTAL | home/begin | aeportal1_svr_1 | /home2/andrew/Documents/SA/aeportal1_svr_1.log00015
BEL_CASH8 | 1331654942356 | 1331654942472 | 13/03/2012 16:09:02.356 | 13/03/2012 16:09:02.472 | 116.0 | UIBW | STAFF | findteamlocations | aeportal1_svr_1 | /home2/andrew/Documents/SA/aeportal1_svr_1.log00015
BEL_CASH8 | 1331654942479 | 1331654942722 | 13/03/2012 16:09:02.479 | 13/03/2012 16:09:02.722 | 243.0 | UIBW | CORRESPONDENCE | getdefaultprinter | aeportal1_svr_1 | /home2/andrew/Documents/SA/aeportal1_svr_1.log00015
BEL_CASH8 | 1331654942632 | 1331654943003 | 13/03/2012 16:09:02.632 | 13/03/2012 16:09:03.003 | 371.0 | UIBW | AUDIT | startsession | aeportal1_svr_1 | /home2/andrew/Documents/SA/aeportal1_svr_1.log00015
BEL_CASH8 | 1331654942793 | 1331654943316 | 13/03/2012 16:09:02.793 | 13/03/2012 16:09:03.316 | 523.0 | UIBW | MOTD | findmessages | aeportal1_svr_1 | /home2/andrew/Documents/SA/aeportal1_svr_1.log00015
BEL_CASH8 | 1331654945206 | 1331654945357 | 13/03/2012 16:09:05.206 | 13/03/2012 16:09:05.357 | 151.0 | UI | PORTAL | home/Profile | aeportal1_svr_1 | /home2/andrew/Documents/SA/aeportal1_svr_1.log00015
BEL_CASH8 | 1331654945227 | 1331654945349 | 13/03/2012 16:09:05.227 | 13/03/2012 16:09:05.349 | 122.0 | UIBW | STAFF | findteamlocations | aeportal1_svr_1 | /home2/andrew/Documents/SA/aeportal1_svr_1.log00015
BEL_CASH8 | 1331654946393 | 1331654946402 | 13/03/2012 16:09:06.393 | 13/03/2012 16:09:06.402 | 9.0 | UI | PORTAL | profile/updateProfile | aeportal1_svr_1 | /home2/andrew/Documents/SA/aeportal1_svr_1.log00015
BEL_CASH8 | 1331654946420 | 1331654946574 | 13/03/2012 16:09:06.420 | 13/03/2012 16:09:06.574 | 154.0 | UI | PORTAL | home/begin | aeportal1_svr_1 | /home2/andrew/Documents/SA/aeportal1_svr_1.log00015
BEL_CASH8 | 1331654946429 | 1331654946566 | 13/03/2012 16:09:06.429 | 13/03/2012 16:09:06.566 | 137.0 | UIBW | MOTD | findmessages | aeportal1_svr_1 | /home2/andrew/Documents/SA/aeportal1_svr_1.log00015
BEL_CASH8 | 1331654946591 | 1331654946599 | 13/03/2012 16:09:06.591 | 13/03/2012 16:09:06.599 | 8.0 | UI | PORTAL | box/receiveBoxInit | aeportal1_svr_1 | /home2/andrew/Documents/SA/aeportal1_svr_1.log00015
BEL_CASH8 | 1331654947628 | 1331654951705 | 13/03/2012 16:09:07.628 | 13/03/2012 16:09:11.705 | 4077.0 | UI | PORTAL | box/receiveBoxes | aeportal1_svr_1 | /home2/andrew/Documents/SA/aeportal1_svr_1.log00015
BEL_CASH8 | 1331654947636 | 1331654951701 | 13/03/2012 16:09:07.636 | 13/03/2012 16:09:11.701 | 4065.0 | UIBW | HANDLEBOX | validatereceivework | aeportal1_svr_1 | /home2/andrew/Documents/SA/aeportal1_svr_1.log00015
BEL_CASH8 | 1331654952738 | 1331654956572 | 13/03/2012 16:09:12.738 | 13/03/2012 16:09:16.572 | 3834.0 | UI | PORTAL | box/receiveBoxes | aeportal1_svr_1 | /home2/andrew/Documents/SA/aeportal1_svr_1.log00015
BEL_CASH8 | 1331654952746 | 1331654956567 | 13/03/2012 16:09:12.746 | 13/03/2012 16:09:16.567 | 3821.0 | UIBW | HANDLEBOX | receivework | aeportal1_svr_1 | /home2/andrew/Documents/SA/aeportal1_svr_1.log00015


```

The structure of this file is pipe delimited 

- username
- start time (milliseconds from epoch)
- end time (milliseconds from epoch)
- start time in human readable format
- end time in human readable format
- response time
- layer
- service
- operation
- server
- log file

With this capability when users complain of performance issues the engineer to track a session for a users and then analyse what happened to that user on the system.  There is no need to ask what functions the user took as it is listed and the engineer can easily see exactly when this issue occurred. Additionally the user can determine if issues are for users on particular servers and link with the information from the other functions above can track down live issues fast, efficiently and in real time.


