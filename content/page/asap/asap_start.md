﻿---
title: Starting ASAP
subtitle: Starting and Communicating with ASAP and Configuring Agents
author: Andrew Colvin
date: 2019-07-07
toc: true
tags: [ASAP]
layout: article
previous: ""
index: "manuals.md"
next: "asap_analysis.md"
#draft: true
logo: "page/asap/asap_start/splash.png"
---

This manual describes how to run ASAP and the different modes of operation and communication mechanisms.  It will then look at each of the different types of agents and describe how they function and how to configure them to support an ASAP ecosystem.

<!--more-->

# {{% param "title" %}}
**{{< param subtitle >}}** \
by **{{< param author>}}** \

## Introduction

Most times ASAP is used will be through its User Interface. However, it can also be started completely headless; but for what purpose? We will investigate using headless mode in more detail later.  First it seems sensible to look at the different components that come with ASAP. The ASAP Release is available from the [release folder of my Gitlab Repository](https://git.acolvin.me.uk/andrew/ServiceAnalysis/releases).  The package distribution is available with the release. Click the build link under the release and download the archive and extract it in your chosen location.  This will extract the following files

File | Purpose
---|---
TibcoServiceDependencyAnalysis.jar | The main ASAP user application can be packaged as asap.jar or even ServiceAnalysis.jar
collector.jar | The [TCP Collector]({{< ref "tcpcollector.md" >}})
DBLoader.jar | [Bulk data loader]({{< ref "tcpcollector.md#bulk-loading-data" >}}) for the TCP Collector
extension.jar | Base file holding pre-packaged log processing modules.
JMSAgent.jar | A log processor that load extensions and sends processed events to ASAP via JMS Queues
PasswordHash.jar | Password hashing tool for the [TCPCollector Password Generator]({{< ref "tcpcollector.md#security-and-communication" >}})
SAClient.jar | Provided for your own programs to create ASAP structured timing events without having to write new file reader extensions
SAExtInc.jar | Library for building file reader extensions
simulator.jar | A system simulator which models queueing and pool starvation using real message and queueing and not emulations.
TCPDirAgent.jar | An agent that monitors whole directors for log files and processes each change to those log files to produce events for ASAP and provides a TCP port for different ASAP clients to connect to them
UDPAgent.jar | Monitors a single file for events to send to a single ASAP and send this out asyncronously via UDP to a single port and IP
UDPDirAgent.jar | Monitors a whole directory for log file changes and sends the events to a single connectionless UDP port on a server


## Starting ASAP

If your java environment (you require a minimum of Java 1.7 with 1.8 recommended presently) then you should be able to just doubleclick the main asap jar `TibcoServiceDependencyAnalysis.jar` file and ASAP will open showing its splash screen before the main UI appears

![](splash.png)

The yellow strip shows what ASAP is doing, which in the above image is starting its internal database.  When it is complete the main UI appears

![](asapui.png)

The asap application can also be started using `java -jar TibcoServiceDependencyAnalysis.jar` on the command line in which case the console will also show output during start

```
starting derby
Wed Jul 10 06:58:21 BST 2019 : Could not connect to Derby Network Server on host localhost, port 1527: Connection refused (Connection refused)
Db still not up
Wed Jul 10 06:58:21 BST 2019 : Apache Derby Network Server - 10.10.1.1 - (1458268) started and ready to accept connections on port 1527
Database up
tables created
Service.operation, Original Time, Adjusted Time
creating controller - bwr
im here
Started UDP on port 5555
creating jms listener
No JNDI available
no receiver
creating memory monitor
getting memory monitor
setting up highlight fields
end of setting up highlight fields
Status Bar: Ready and listening on UDP port 5555
can't find file /home2/andrew/Documents/SA/0.99.51/autorun.asap

```

This provides information on the start of its database and its successful internal structures being started what port it is listening on for UDP events its JMS connector (or not in this case as there is no `JNDI` information) and the success of any autostart commands it finds in the autorun.asap file.  The start up messages will vary depending on whether ASAP has to start the database; if you run multiple asap clients on the same `host` asap will connect to the already running instance therefore allow you to load the same snapshots across instances. This is mainly useful for snapshotting a running system , then load the snapshot into another instance that is not getting live updates and processing any analysis on this static data set.



### command line args

ASAP allows the user to use a few commandline arguments to affect its running.  There are many that are depprecated that can be found in the code. There successful use is undocumented.

Option | Use
---|---
--headless | Starts ASAP without its UI
-- | listen on standard in for commands
--noauto | turn off processing of the `autorun.asap` file
--DBEvents | record every incoming event into the internal database. Recommended to use the TCPCollector instead but can be useful for connecting the data to a spreadsheet. This has major impacts on ASAP performance so use at your own risk



### Menus
The menu structure of ASAP is:

> File

Menu Entry | Description
---|---
Reset Timings| resets all timings and samples
Clear Services|Clears the complete model from memory
________________|
Save Snapshot|Saves the model and timings to the embedded database
Load Snapshot|Load a snapshot previously saved from the embedded database
Compare Snapshot|Compares the current timings against those saved in a previous snapshot
Remove Snapshot|Delete a snapshot from the database
________________|
Load File |Reads a file from the beginning to the end and processes each line through the internal readers and then the extensions
Load Matches |Reads a file containing regular expression matches and replacements
________________|
Exit | Exit the ASAP client

\

> Filter

Menu Entry | Description
---|---
Layer | Filter services to a particular layer String, UI, BW,DB or all
Service|Allows the user to search for a service.operation.  Matches the first part exactly as a string
Show Highlighted|Show highlighted rows only [see here]({{<ref "page/asap/asap_monitoring.md#service-table-and-monitoring">}})
Filter File Events|Filter events to those within the specified time range
Manage Matches| Create, Edit and Delete Matches to change incoming *service.operation* values

\

> Windows

Menu Entry | Description
---|---
Volumetric Model| Opens the Volumetric Model Cost and Improvement Window
Top Service Times| Opens a [Window]({{< ref "page/asap/asap_analysis.md#viewing-worst-performing-services" >}}) Showing the worst responders
Transaction View| Opens a Window Showing Individual Transactions (see [here]({{< ref "page/asap/asap_monitoring.md#transaction-monitoring" >}}) )
________________|
Service Dependencies| Opens a [window]({{< ref "page/asap/asap_analysis.md#dependency-view" >}}) showing the dependency matix for the selected *service.operation*
Service Frequency| Opens a [window]({{< ref "page/asap/asap_analysis.md#non-time-related-analysis" >}}) showing the frequency distribution for a *service.operation*
Real Time Graph| Opens a [window]({{< ref "page/asap/asap_monitoring.md#monitoring-with-graphs" >}}) showing real response times per transaction for a *service.operation*
Quantised View| Opens a [window]({{< ref "page/asap/asap_analysis.md#reviewing-performance-over-time" >}}) showing aggregated data over rolling 60 minute periods.
________________|
Open System.output| Shows the system out messages and allows the user to enter commands
Save Windows Settings| Saves out the position and size of all open windows so they can be loaded

\

> Real Time

Menu Entry | Description
---|---
Monitor File| Allow the user to select a file that ASAP will tail for performance events
Load Monitor File List| Allows the user to load multiple files to tail
Monitoring -> <files> | Shows the files being tailed and allows releasing the file from monitoring
________________|
Reset Rate Meters| Resets all rate meters to zero 
________________|
Real Time Graph| See Real Time Graph above
Quantised View| See Quantised View above
________________|
Transaction View| Opens a [window]({{< ref "page/asap/asap_monitoring.md#transaction-monitoring" >}}) to visualise each transaction and sub-transactions
Capture Transactions| Turns on the capture of individual transactions
Clear Transactions| Clears the memory of individual transactions

\

> Reports

Menu Entry | Description
---|---
Text Report|Opens a window that allows the selection of various text reports
PDF Service Report|Writes a PDF report to disk of the selected *service.operation*


\

> TCP Collector

Menu Entry | Description
---|---
Connect|Provides the ability to connect to a TCP Collector
Select Timings|Provides the capability to query the TCP Collector
Manage Collector|Provides basic control of the TCP Collector connection



## Command Language

ASAP has a set of commands (a command language) that can be used to control what it does.  This can be delivered through the *System.out* window or from the standard in using `--` as a command line argument or through commands loaded via a file. Note that the TCP Collector will not pass Commands to ASAP clients.

All commands follow a standard pattern `COMMAND: <command> <mandatory args> [optional args]`; importantly there is a `help` command. Note that the System.out will prepend `COMMAND:` if it doesn't exist.

```
COMMAND: help
StartJMS: Start JMS Listener
TCPConnect [STOP] <host> <port>: connect or disconnect to a TCP Directory Agent
Password <password>: set password to authenticate against collector
Reset Services: clear timings of all services
Reset meters: reset all rate meters
Reset filters: reset all time filters
Reset: clear all services
Help: this help message
Read <file>: load file
Monitor [Remove] <file>: tail file for events or stop tailing
Save <name>: save data to snapshot
List: list snapshot descriptions to standard out
Load <name>: load snapshot
Exit|Quit|Shutdown: close application
UserCount [clear|zero]: clear all user login counts or set to zero at this time
Directory Watch <Directory> <extension>: watch directory for files with set extension and tail those that are changing
Directory Remove <Directory> <extension>: remove the specified directory watcher
Directory clear watchers: remove all watchers
Window dump: dump details of the windows currently open
Window Monitor <layer> <service> <operation> <x> <y> <w> <h> [period] [show title]: open the specified real time window at a location and dimension and optionally set the timespan and the title on/off
Window ServiceTable column "<column name>" <on|off>: add or remove the specified column to the table
Window ServiceTable highlight <on|off>: turn on or off the highlight filter
Window ServiceTable position <x> <y> <w> <h>: set the position and size of the service table
Window ServiceTable columnwidth "<column name>" <width>: set the width of the specified column in the service table
Window Count "<Layer>" "<Service>" "<Operation>" <x> <y> <w> <h> <period>: open the count graph for the specified service with specified dimension and location and set the average aggregation to period
Print Report <name>: print the report with the specified name
Print Services: print list of services
Output <file>: send reports to this file
Close: close the output file and reset it to standard out
Filter From <yyyy-MM-dd HH:mm:ss>: only load events that are after this date/time
Filter To <yyyy-MM-dd HH:mm:ss>: only load events up to this date/time
Report Add <report Name> <Column> [value]: create report and add the column to it
Report Rates Service Operation Period: output the fixed rate report
Report Counts Service Operation Period: output the fixed  count report
Report RollingCount Service Operation Period: output the fixedrolling count report
TCPCommand read <server> <port> <file>: read a file on the server and send it to the client
TCPCommand read <server> <port> <directory> <extension>: monitor another directory on the server and send file updates to all clients
TCPCommand monitor <server> <port> <file>: monitor file on server and send all changes to all clients
TCPCommand stopall <server> <port>: stop monitoring all files and directories
TCPCommand chpasswd <host> <port> <current password> <new password>: change collector password once authenticated
TCPCommand resetpasswd <host> <port> <user> <admin password> <new user password>: change collector password for a user
TCPCommand disableuser <host> <port> <user>: delete a user from the collector (must be an admin user)
TCPCommand adduser <host> <port> <admin password> <user> <user password>: add a user to the collector (must be an admin)
TCPCommand addadmin|deladmin <host> <port> <admin password> <user>: add or remove an admin role to a user (must be an admin to run this command)
TCPCommand live <host> <port> on|off: turn on or off live updates from the TCPCollector
TCPCommand select <host> <port> timings from "<dd/MM/yyyy HH:MM>" to "<dd/MM/yyyy HH:MM>": retrieve records between the given dates
TCPCommand select <host> <port> timings day <dd/MM/yyyy>: select all records on a given day
TCPCommand select <host> <port> stop: stop selection of records
Delay <number>: change delay between updates to number milliseconds
Highlight <field name> <layer.service.operation> <green> <orange> <red>: colour fields based on threshold values.  Fields can be "Rate" or "Average"
Alert add <layer> <service> <operation> <threshold>: Set an event alert to a service at specified threshold
Alert del <layer> <service> <operation>: Remove any event alert against the specified service
Alert clear: clear all alert rules
OutlierFactor <value>: Count graph outlier calculation scale factor.  Should be greater than 1 (default 1.5)
UserWatch <name>: Output all user tagged actions
UserWatchOff: Turn off all user watching
ListCapturedUsers: Lists the captured users
PrintUserEvents <user>: Prints the events collected for the specified user
SaveUserEvents <user regex pattern> <file>
SetInactivityThreshold <minutes>: Sets the period after which a user is considered to be logged out (default 10)

Allowed Report Tags
	Min
	Max
	Mean
	Percentile
	Service
	Operation
	StdDev
	Count
	HMean
	GMean
	LogMode
	ConsumersCount
	DependantCount
	Dependants
	Wait
```


## Agents

### UDP Agents

### TCP Agents

### JMS Agents



