﻿---
title: ASAP Analysis
subtitle: Analysing Performance using ASAP
author: Andrew Colvin
date: 2019-06-25
toc: true
tags: [ASAP]
layout: article
#previous: "manuals.md"
index: "manuals.md"
next: "asap_monitoring.md"
#draft: true
logo: "page/asap/asap_start/splash.png"
---

This article is a section of the ASAP manual which has been broken into separate section for ease of use.  The section will look at how ASAP will help you, the performance analyst, to process the performance data of the system and relate this to real business outcomes using the model of the business processes built using ASAP.

<!--more-->

# {{% param "title" %}}
**{{< param subtitle >}}** \
by **{{< param author>}}** \

## Introduction

The analyst often needs to model the business performance to work out how to improve the performance of either the business or the systems used by the business.  ASAP provides the capability to produce a simple model of the business processes and link this into system processes so that the timings from the system drive the operational time used for processing the different steps of the process. This is then multiplied up by the quantity of times this is completed in a standard unit of business time (say a day or an hour).

There are a number of concepts that will be discussed throughout this document and these are listed in the table below:

| Concept  | Description|
|----------|------------|
|Model     |This is built using simple text formatting to represent the processes within the business that need to be analysed|
|Workstream|The is a step in a process that is generally designed to be a subprocess of the business to produce a product|
|Volumetric|This is the product that is being produced in volume.  It can be physical or ephemeral depending on how you wish to build the model of the business. |
|Service.Operation| A system function that is being called as part of the business model|
|Mean| The average time a step in the process or system function takes to complete|
|Adjustment| The change that the analyst wants to model to see how it impact the business and therefore help with a return on investment|
|Dependencies| Those components that execute other processes and how often|
|Frequency Distribution| The count of the number of times a function completed in specified periods of time, e.g., (0--1] second, (1--2] seconds, (2--3] seconds, (3--4] seconds,...|
|Instrumented System| A term used to define a system that generates relevant statistical data either by the underlying products being used or by the developers introducing logging of data carrying performance information preferably at a transaction level. |
|Reader| A specific dynamically loaded library that is designed to read log data in a specified format and is not built into the core ASAP program|
| Extension| A external reader developed by you or a third to extend the log file reading capability of ASAP

The rest of this document will start by looking at the static analysis of the functional performance of a system.  It will then look at the business modeling and performance analysis and finally the language used to build the model.

## Static Analysis of a System's Performance

ASAP works on being able to read the log data from an _Instrumented System_; without this log data ASAP will not be able to help you. ASAP does provide a library of logging code (written in java) that will generate instrumentation that is compatible with the _readers_ packaged with ASAP as part of the delivered client library used to build _extensions_. An example of a _reader_ is the Portal Reader that was built for a specific project that had _instrumented_ its portal performance such that it recorded the start of a transaction and the end of the transaction and each service call inside that transaction and the time taken for the service to return.

```
####<13-Mar-2012 10:21:00 o'clock GMT> <Info> <AMS_PERF> <vioappcdc196129.blue.net> <aeportal1_svr_1> <[ACTIVE] ExecuteThread: '61' for queue: 'weblogic.kernel.Default (self-tuning)'> <DUR_CH128> <> <> <1331634060722> <BEA-000000> <Request #287468, unknown, request start: /AMSPortalWebProject/com/ams/home/begin.do>
####<13-Mar-2012 10:21:00 o'clock GMT> <Info> <AMS_PERF> <vioappcdc196129.blue.net> <aeportal1_svr_1> <[ACTIVE] ExecuteThread: '61' for queue: 'weblogic.kernel.Default (self-tuning)'> <DUR_CH128> <> <> <1331634060730> <BEA-000000> <Request #287468, DUR_CH128, service start: STAFF.findTeamLocations>
####<13-Mar-2012 10:21:00 o'clock GMT> <Info> <AMS_PERF> <vioappcdc196129.blue.net> <aeportal1_svr_1> <[ACTIVE] ExecuteThread: '61' for queue: 'weblogic.kernel.Default (self-tuning)'> <DUR_CH128> <> <> <1331634060847> <BEA-000000> <Request #287468, DUR_CH128, service end: STAFF.findTeamLocations, duration(ms): 117>
####<13-Mar-2012 10:21:00 o'clock GMT> <Info> <AMS_PERF> <vioappcdc196129.blue.net> <aeportal1_svr_1> <[ACTIVE] ExecuteThread: '61' for queue: 'weblogic.kernel.Default (self-tuning)'> <DUR_CH128> <> <> <1331634060854> <BEA-000000> <Request #287468, DUR_CH128, service start: CORRESPONDENCE.getDefaultPrinter>
####<13-Mar-2012 10:21:01 o'clock GMT> <Info> <AMS_PERF> <vioappcdc196129.blue.net> <aeportal1_svr_1> <[ACTIVE] ExecuteThread: '61' for queue: 'weblogic.kernel.Default (self-tuning)'> <DUR_CH128> <> <> <1331634061021> <BEA-000000> <Request #287468, DUR_CH128, service start: AUDIT.startSession>
####<13-Mar-2012 10:21:01 o'clock GMT> <Info> <AMS_PERF> <vioappcdc196129.blue.net> <aeportal1_svr_1> <[ACTIVE] ExecuteThread: '61' for queue: 'weblogic.kernel.Default (self-tuning)'> <DUR_CH128> <> <> <1331634061192> <BEA-000000> <Request #287468, DUR_CH128, service end: AUDIT.startSession, duration(ms): 171>
####<13-Mar-2012 10:21:01 o'clock GMT> <Info> <AMS_PERF> <vioappcdc196129.blue.net> <aeportal1_svr_1> <[ACTIVE] ExecuteThread: '61' for queue: 'weblogic.kernel.Default (self-tuning)'> <DUR_CH128> <> <> <1331634061214> <BEA-000000> <Request #287468, DUR_CH128, service start: MOTD.findMessages>
####<13-Mar-2012 10:21:01 o'clock GMT> <Info> <AMS_PERF> <vioappcdc196129.blue.net> <aeportal1_svr_1> <[ACTIVE] ExecuteThread: '61' for queue: 'weblogic.kernel.Default (self-tuning)'> <DUR_CH128> <> <> <1331634061369> <BEA-000000> <Request #287468, DUR_CH128, service end: MOTD.findMessages, duration(ms): 155>
####<13-Mar-2012 10:21:01 o'clock GMT> <Info> <AMS_PERF> <vioappcdc196129.blue.net> <aeportal1_svr_1> <[ACTIVE] ExecuteThread: '61' for queue: 'weblogic.kernel.Default (self-tuning)'> <DUR_CH128> <> <> <1331634061379> <BEA-000000> <Request #287468, DUR_CH128, request end: /AMSPortalWebProject/com/ams/home/begin.do, duration(ms): 657>
```

With this data the analyst was able to view the speed of the server transactions and what time the different components responded within.  This could also be compared with the times from the underlying service which would then expose delays in transport of asynchronous waits such as not enough threads in a pool etc.

## ASAP's Window onto the world

ASAP has multiple windows that will be discussed in this section. These windows are all linked together and are normally related to the service selected in its main window.

The main window displays data about the performance characteristics of each service. The following pictures shows the main window with all of its fields displayed. Note that fields can be added and removed from the display dynamically through the right click menu on the table header as shown:

{{<figure src="fields.png" caption="ASAP's Main Window and its Fields" >}}

The meaning of the different fields:

| Field | Description|
|---|---|
|Layer|A descriptor specifying the system layer, e.g. UI or DB|
|Service Operation|A dotted name of the component.  The two part name allows the timings to be at a functional level if required.|
|Count|The number of timing events that have been loaded against the service.operation|
|Rate|The rate at which the last 100 samples arrived
|Cs|The number of consuming `service.operation` recorded
|Ds|The number of `service.operation` this called
|Average|The mean of all the samples for this _service.operation_
|Std Dev|The population standard deviation of all of the samples for this _service.operation_
|Avg Wait Time|The mean of the time this _service.operation_ had to wait on it called _service.operations_
|CPU Time|The mean of the time this _service.operation_ spent processing internally <br />`Mean = Avg Wait Time + CPU Time`
|Explicit Adjustment| An analysis tool to model improvements
|Adjustment -/+| The amount of savings made from the adjustment modeling
|Adjusted Average| The new average if the adjustment is made
|Log Median| The calculated median if the _service.operation_ fir=tted a perfect log-normal distribution
|Log Mode| The calculated mode if the _service.operation_ fir=tted a perfect log-normal distribution
|Harmonic Mean| The harmonic mean of the distribution displayed by the _service.operation_
|Median| The median (50<sup>th</sup> percentile) of the distribution displayed by the _service.operation_
|Min| The minimum value received from the events relating to this _service.operation_
|Max| The maximum value received from the events relating to this _service.operation_
|Running Average| The average of the last 100 events received

The following image shows the main window with data loaded into it sorted on the `count` field

{{< figure src="maindata.png" caption="Main Window with Data Loaded" >}}

### Different Averages

There is a good description about different averages [here]({{< ref "post/lognormal" >}}). The theoretical perfect distribution for a single path function is log-normal as discussed in the linked article. This means that the log of the samples are distributed normally. There are some conceptual drivers for this such as a system cannot respond faster than zero (or even at zero) so all the values become bunched and the tail of response time also becomes more prominent and has a larger impact on the mean as it cannot be balanced by a single event on the opposite side of the mean because it cannot go faster than the minimum time the system can respond. 

The median of a log normal, \\( \tilde x \\), is the calculated as
<html>$$ 
\begin{aligned}
\tilde{x} &= e^{\mu}, \text{\ \ where  } \\\ 
\mu &= \frac{1}{n} \displaystyle\sum_{i=1}^n \log {x_i}
\end{aligned} 
$$</html>

The modal value, \\( \hat x \\), called the mode and is the most common value, is evaluated as:
<html>\\[
\begin{aligned}
\hat x &= e^{\mu - \sigma^2}, \text{\ where } \\\ 
\sigma^2 &\approx \sum_{i=1}^n (\log x_i )^2 - \mu^2
\end{aligned}
\\]</html>

The mean of a log-normal distribution, \\[ \bar x \approx e^{\mu+\sigma^2}\\] 

It is possible to calculate the mean of the samples exactly and this is what ASAP does.  Comparing the values gives a measure of confidence of how close the samples are to a theoretical log-normal distribution.
Note that theoretically the distribution should be log-normal this may not be the case as a service may have many paths through it each producing a different distribution.  These distributions will be overlaid over each other and the distributions will be multi-peaked.  


## Viewing Worst Performing Services

When data is loaded into ASAP the first and simplest view is to list each service against each other in terms of their performance. This is achieved using the `[windows/Top Service Times]` menu which brings up two separate windows. The first is simple and just displays the average performance of the _service.operations_ sorted with the worst at the top. Note it will only show up to a maximum of 10 seconds and anything above this level is excluded from the graph

{{< figure src="servicetimes.png" caption="Worst Performers" >}}

There is a drop down box at the top of the window which allows the analyst to focus on the layer they are interested in.  On the left and right are sliders that change the threshold of the lower and upper performance values and thereby providing a zoom capability

{{< figure src="servicetimeszoom.png" caption="Worst Performers Zoomed Out" >}}

Just viewing the worst performers based on the average response time tells us little relative about the performance of the whole solution. For example there may be a process that takes one minute to complete but if it is completed once per day this is likely not an issue. What is a different view and arguably a better value is the amount of "_virtual cpu time_" being consumed by each _service.operation_ and comparing these values together.

The total _virtual cpu time_ a _service.operation_ uses is defined as the average time multiplied by the number of calls.  Therefore the percentage of _total virtual cpu_ any particular _service.operation_  is defined using the following formula:



\\( \mu_i \text{ is the algebraic mean of the } i^{\text{th}} \\) _service.operation_ \
\\( c_i \text{ is the sample count of the } i^{\text{th}} \\) _service.operation_ \
\\( i \in \\{ 1,\...,n \\}, n \\) is the total number of _service.operations_ in the data set \
Percentage of total virtual CPU for a _service.operation_,
\\[ p_i = 100.\frac{\mu_i c_i}{\sum^n_1{\mu_i c_i}} \\]

{{< figure src="percentagetimes.png" caption="CPU Hoggers" >}}

On this view the second worst performer is a process that by looking at the pure average figure is actually not a bad performer.  This is because it is called a large number of times. Therefore improving this will have a big improvement on the performance of the business.

This bar chart only shows those _service.operations_ that utilise more than 2% of the total _virtual cpu_.

## Reviewing Performance Over Time

The next function to look at is how a _service.operation_ performs across a long period of time.  To do this it is necessary to collect responses in tme periods and not look at each transaction. The graph created when the analyst uses **[right click | Quantised View]** or **[windows menu entry | Quantised View ]** in the ASAP window is shown below and is focused on a single _service.operation_; multiple quantised view windows can be opened simultaneously.

{{< figure src="quant1.png" caption="Quantised Performance Graph for a service operation" >}}

This graph is carrying a lot of information:

- The x-axis is time
- The left hand y-axis is quantity
- The right hand y-axis shows response time in milliseconds
- The blue line is the quantised hourly rate and th quantise width can be changed with the entry field at the bottom.  What is meant by this is that each minute has the "recording" of each event that happened within that minute and this. The line then shows the number of events that occured in the last hour and rolls for each quantised size i.e if the quantisation is set at one minute I get the hourly rate each minute plotted against time thereby showing the peak hour simply.
- The green line is the raw number of events received for this _service.operation_ within each given period.
- The red dots show the average response time of all the samples in that quantised period and a representation of the amount of variance (the line attached) denoted by the length of the line.
- The maximun and minimum values are also shown with outliers of the graphs axis shown as a red triangle (the number above being the maximum and if a number below the triangle is shown that is the average for that period.

There is one more element to the graph which is to show the number of logged on users to the sysem against time; this is only available when data is provided.  The majority of the different functions can be controlled from the options menu.  The following version of the same graph now has the user count switched on:

{{< figure src="quant2.png" caption="Quantised Performance Graph for a service.operation" >}}

## Non-Time Related Analysis

Having a time based view of the system is not always the best option to review the performance. The ASAP client will present the frequency and cumulative frequency graphs for each _service.operation_.  It can be opened using the **[ right click | Frequency Graph ]** menu entry or the **[ Windows |  Service Frequency ]** menu entry. 

{{< figure src="frequency.png" caption="Frequency Graph for a service operation" height="200" width="200">}}

This graph displays the number of events that occurred at a given response; or falls within a given response range set using the slider at the top which defaults to one.  The cumulative frequency is the sum of all of the samples up to a given response displayed as a percentage of all the samples received.

The graph is very jaged and would smooth out the more samples received. However, this could require many millions to give a really smooth view (this is a plot of about 4000 samples). To get around this sharpness we can smooth the distribution by pooling samples into ranges. The width of the range can be set using the slider along the top right of the window. What we then plot is the centre of the sample range as the as the ordinate. For example a sample width of 20 would create partitions [0,20), [20,40), [40,60), ... with the following as plot values: 10, 30, 50, ...  Further as we gather more samples and represent them as a single value the sample count at the plot values increases thereby giving us a smoothed view. The follwoing two graphs show the same data plotted with the sampling set at 20 and 60 so you can see this effect in action

{{< figure src="frequency1.png" caption="Frequency Graph for a service operation; sample size = 20" >}}

{{< figure src="frequency2.png" caption="Frequency Graph for a service operation; sample size = 60" >}}

On the right hand side of the window is a white box with a listed _service.operation_ listed. This _service.operation_ is a known dependent function, i.e., it is called as part of the calculation. This function cal be plotted overlayed on the same axis so that appropriate comparison can be taken.  Note if the data loaded conveys multiple dependents they would all be listed and the analyst can select and plot only those that are interesting. To deselect one of these additional plots use `ctrl+click` to deselect and click the **[Plot]** button. Additionally, but not shown on this diagram because the data does not convey any data, is a list above the dependent function for consuming _service.operations_. 

The next graph provides a view of the two _service.operation_ plots together showing great correlation between them. Clearly if the dependent performance can be improved this will carried with a 1:1 relationship onto the `PORTAL` function which is the focus of the graph. 

{{< figure src="frequency3.png" caption="Frequency Graph for a service operation; sample size = 60; with dependent " >}}

The other key feature of the graph is that it shows two modes of function (2 humps) which is likely in this case to be 2 different paths through the code. As this is a `find` function the difference is possibly search by key and search with a wildcard say. The cumulative frequency graph shows a knee at about the 85 percentile so this more complex path is clearly called about 15% of the time by the business. If this was significantly slower we could then target these searches with better indexation or some other mechanism as appropriate to the solution.

The graph also shows a consistent delay between the dependent's values and the _service.operation_ being investigated. The following figure shows this delay zoomed in on the graph ![](frequencydelay.png) This demonstrates that there is a consistent 30 ms delay which is the duration required to return the result between the dependent and its consumer. Reducing this to a much lower value of 1 ms by removing this remote call to a library call is a clear example of of saving time spent by the business waiting for results. Thirty milliseconds may not sound a lot but when multiplied by the number of calls throughout the day can become a substantial saving to the business in that the users' perception improves and their thought processes are not interspersed by delays thereby keeping them more focused and productive.

From the graph the analyst can also see how spread the _service.operation_ response is and is often a measure of complex processes and internal delays caused by contention in the underlying platform, an example could be: waits caused whilst other threads or processes are accessing shared objects, this can arise from unexpected sources (including outside influences) for example creating temporary files in a filesystem will normally lock on the directory to ensure uniqueness or I/O delays, system wait of all sorts. 

## Dependency View

It is often important to understand what _service.operations_ call which other _service.operations_.  ASAP is able to show this given appropriate data being loaded that provides this information.  Here is an example of the output for the `PORTAL` service's `home/begin` operation. We can clearly see that it calls multiple functions to deliver it offering:

{{< figure src="PORTAL.home-begin.svg" caption="Dependency View" >}}

The meaning of the numbers is shown in the following diagram

{{< figure src="dep-findapplication2.svg" caption="Dependency View Explained" >}}

From this diagram we can see some fairly interesting facts that we may want to investigate:

- There were 4312 request made to the PORTAL but only 4305 reached the `GATEKEEPER.findApplication`
- The mean of `searchApplication2` is outside even the 98.5^th percentil of `GATEKEEPER.findApplication`

### Configuration of Dependencies

The easiest way to get dependencies loaded is directly from your performance log files. This is the case with the file loaded for these graphs which also included the sample of logs from above.  Looking at a snippet from the log `<Request #287468, DUR_CH128, service end: MOTD.findMessages, duration(ms): 155>` tells me that each line has request number `287468` and therefore ASAP is able to tie all of these parts tgether as a single transaction and work out dependencies.

Another way to to define them in the ASAP main window. This is achieve by selecting a row and then right clickig another row and in the menu selecting **[Set as Dependent]**  You can also remove a depency this way.

For a complex model where the data does not convey the structure the analyst can create a model that can be loaded separately. This model file is defined as a simple text file and has the following structure

Define a _service.operation_...

```bash
#define the services
#MODEL,layer,service,operation,,,,,,comment

MODEL,UI,PORTAL,SCREEN1,,,,,,PAP=hhhrrr
MODEL,UI,PORTAL,SCREEN2,,,,,,PAP=fgfgfgf
MODEL,BW,PROCESS1,OP1,,,,,,
MODEL,BW,PROCESS1,OP2,,,,,,
MODEL,BW,PROCESS2,OP1,,,,,,
MODEL,BW,PROCESS3,OP1,,,,,,
MODEL,DB,PACKAGE1,PROCEDURE1,,,,,,
MODEL,DB,PACKAGE1,PROCEDURE2,,,,,,
MODEL,DB,PACKAGE1,PROCEDURE3,,,,,,
```

and now the dependencies...

```bash
#define dependencies
#MODEL,consumer layer,consumer service,consumer operation,dependent layer,dependent service,dependent operation,(protocol),(synchronicity),(tags)


MODEL,BW,PROCESS1,OP1,BW,PROCESS2,OP1,JMS,SYNC,queue 1
MODEL,BW,PROCESS1,OP2,BW,PROCESS3,OP1,JMS,SYNC, queue 2
MODEL,BW,PROCESS2,OP1,BW,PROCESS3,OP1,JMS,SYNC

MODEL,BW,PROCESS2,OP1,BW,PACKAGE1,PROCEDURE1,SQL,SYNC, DB a
MODEL,BW,PROCESS2,OP1,BW,PACKAGE1,PROCEDURE3,SQL,SYNC, DB b
MODEL,BW,PROCESS3,OP1,BW,PACKAGE1,PROCEDURE3,SQL,SYNC

MODEL,UI,PORTAL,SCREEN1,BW,PROCESS1,OP1,HTTP,SYNC
MODEL,UI,PORTAL,SCREEN2,BW,PROCESS1,OP2,HTTP,SYNC
```

This model can be seen in the following dependeny view

{{< figure src="dependency-model.svg" caption="Example Model" >}}

## Comparing _service.operations_ against Saved Versions

ASAP allows the analyst to save the performance results to its database and then at a later date compare those against the currently loaded values. This give the ability to see how much better or worse the current values are relative to a reference set.

To save values to the database select **[ File | Save Snapshot]** and add a description of the save in the dialogue box.

To do a comparison against this snapshot select **[ File | Compare Snapshot ]** select the snapshot from the list presented and then click **[ Compare Snapshot ]**.

At this point a table appears with 5 columns:

| Column | Description |
|---|---|
| Service | Arranged as < *layer* >:< *service* >:< *operation* > 
| Snapshot Mean | The mean value of the _service.operation_ in the saved set of data
| Mean % Difference | The percentage difference between the mean values \\( 100( \bar x\_{snapshot}   - \bar x\_{current} ) / \bar x_{snapshot} \\)
| Snap Shot Count | The number of samples for _service.operation_ in the snapshot set of data
| Current Count |The number of samples for _service.operation_ in the current set

The percentage difference provides the  following information

negative (-&#8734;,0) | zero | positive (0,1] 
----|----|----
|The current set has a mean that is higher and the more negative the greater the difference | The means are the equal| The current data set has an improved value. The closer to one implies better improvement|

{{< figure src="compare.png" caption="Comparison window comparing the same data" >}}



