﻿---
title: TCPCollector Manual
subtitle: What is the Collector and how it Works
author: Andrew Colvin
date: 2019-06-22
toc: true
#weight: 120
tags: [ASAP]
layout: article
previous: "manuals.md"
index: "asap-overview.md"
next: ""
logo: "page/asap/asap_start/splash.png"
---

The TCP Collector is a part of the ASAP suite of tools that assists the analyst to monitor and tune a system.  It collects data from other agents through different source agents and transmits all of the collected data to any number of ASAP clients that have connected to it.  The TCPCollector is able to record timing events into a datastore for retrieval by ASAP clients at a later date and replay these events in slow motion or fast forward.  The TCP Collector is not a required component and ASAP can be used to monitor the source system directly but it provides additional degrees of freedom and controls.

<!--more-->

# {{% param "title" %}}
**{{< param subtitle >}}** \
by **{{< param author>}}** \

## Introduction

The TCPCollector is a component of the ASAP Performance Analysis and Monitoring Tool (ASAP).  Its purpose is to collect events from the TCPAgents that are monitoring log files for events.  Once an event has been collected it then forwards the event on to either other collectors or ASAP clients; providing a hub and spoke collection mechanism.

{{< figure src="collector1.png" caption="Block Diagram showing TCPCollector Usage" >}}

The TCPCollector connects to TCPDirAgents that are capturing events from log files they are monitoring. These events are then sent to the TCPCollector as they occur. ASAP clients can connect to the TCPCollector and will receive these events. The ASAP clients also have some degree of control over their sesion or if logged in with admin role the collector in its entirety. The collector is capable of recording each event into a datastore for playback to an ASAP client on request.


The TCPCollector has two modes of operation: unmanaged and managed.  In unmanaged mode it listens on a port for incoming ASAP connections and connects to a series of TCPDirAgent components to proxy on the events transparently.  In this mode any  user can connect and collect the event information.  In some environments this may not be desirable and the managed mode is more appropriate. In managed mode a user is requested to authenticate themselves using a username and password.  Once logged in events will start to flow to the client.  A user can be given an admin role which provides that user with additional functionality to manage user and admin rights.

When a TCPCollector is to be used to proxy events to a further TCPCollector (a mode used to move events around a complex zoned environment) the first ones that are not connected to by ASAP clients) must be in unmanaged mode.

## Configuring and Running the TCPCollector

The TCPCollector, like ASAP, is a program written in java 1.7 (tested from 1.6 to 1.9) and is packaged as a self contained jar file that can be run with the command:
```
java -jar collector.jar <config file> [log level]

```

On start it looks for the configuration file passed on the command line and parses the instruction held within.  The log level is optional and can be one of: -debug, -trace, -info (from high logging to low logging.  Without a log level then the TCPCollector is silent.  All logging output is sent to the standard out.

### Configuration File Format

The collector requires a mandatory configuration file to be passed on start up.  This configuration file is used to provide the collector with details of how it should be started.  The following table provides a list of values that can or may be set.

|Command |Use    |Description|
|-------|------|------|
|port=\<integer\> | Mandatory and once only | This defines the port that the TCPCollector will listen on for incoming connections.|
|auth=\<true\|false\>|Optional but once only|Set this to true for authentication required else the mode will be set to unmanaged (the default)|
|remote=\<true\|false>|Optional but once only|This enables remote management when authentication is required.  The default is off and any changes have to be made to the config file and not through ASAP|
|user=\<username\>,\<password\>|Optional Multiple lines|This creates a user with a name username.  The password will be hashed and written back to the file if it is plain text.|
|admin=\<username\>|Optional Multiple lines|Sets the specified user as an administrator. |
|connection=\<host\>:\<port\>|Optional Multiple lines|These lines specify which TCPAgents the collector will connect to|
|store=true|Optional once only|Turns on event storage to the mongo database.|
|mongoURL=\<connection string\>|Optional once only|Changes the connection string to the mongo database.  The default is localhost on port 27017|
|purgePeriod=\<+minutes\>|Optional once only|Set the period at which the database purge process runs.  Setting it to zero turns off purge.  Only accepts positive number and zero.  Default value is zero i.e. no purge|
|purgeDelay=\<+hours\>|Optional once only|The number of hours for a record to age before it is available to be purged.  Default is 7 days (168)|

The TCPCollector will periodically attempt to reconnect to any connection specified in the configuraton file that is currently disconnected.  This provides the mechanism for an agent being started after the collector or restarted if necessary without impact on the TCPCollector or its clients.

Any change to the configuration file will be processed within 10 seconds of each save of the configuration file.

#### Example Configuration File

Here is an example configuration file that starts the Collector listening for connections on port 5556, attempts to connect to a TCP agent on port 5555 on localhost, records all events, requires authorisation, has one user defined that is an admin, does not purge old records and allows remote management.

```
port=5556
auth=true
remote=true
user=andrew,Hashed:1000:xxxxxxxxx:xxxxxxxxxxx
admin=andrew
connection=localhost:5555
#connection=localhost:5554
store=true
purgeDelay=720
purgePeriod=0
```

### Managing the TCPCollector

The TCPCollector can be managed using either of the following two methods:

- Changing its configuration file which will be processed dynamically
- Enabling remote administration and sending commands through from the ASAP command system.  Note that only a limited subset of configuration settings are exposed through remote management
- Using the ASAP UI

#### ASAP Commands

The following ASAP Commands can be used to connect, disconnect and remote manage a TCPCollector.  Note that you cannot dynamically connect to other TCP Agents using remote commands.  Remote commands provide a subset of administration function for day to day user support only.

All commands that communicate with a TCPCollector require the host and port that the collector is listening to and the issuing client must be logged into this TCPCollector.

**User Commands**

The following commands are available to a normal user from within the ASAP client to control the user's session.  As all of ASAPs command language these are entered with a preceding `COMMAND:`, for example `COMMAND: TCPConnect localhost 10000`.

|Command|Description|
|---|---|
|TCPConnect [STOP] &lt;host&gt; &lt;port&gt;|Connect (disconnect) to a TCPCollector that is listening on the specified host (IP address or hostname) and port. If authentication is configured in the TCPCollector then the collector will request the username and password to be sent along with an encryption certificate that the client should use. On successful authentication the connection is confirmed to the client and performance events will be sent to the client as they arrive.|
|Password &lt;password&gt;|Set the password to authenticate against the TCPCollector. The username is taken from your log on details to the desktop running ASAP. This can be overridden by setting the USERNAME environment variable to the requested username string or the system property user.name|
|TCPCommand chpasswd &lt;host&gt; &lt;port&gt; &lt;current password&gt; &lt;new password&gt;|Allows a user to change their current password to a new password on the specified TCPCollector listening at host:port|
|TCPCommand select &lt;host&gt; &lt;port&gt; timings from “dd/MM/yyyy HH:mm” [to “dd/MM/yyyy HH:mm”] [layer regexp] [service regexp] [operation regexp] [user regexp] [rate value]|Retrieve timings from the TCPCollector storage between the given dates. When the “to” date/time is not provided it will default to now. The layer/service/operation allow additional filtering with an implied logical &amp; between all the filters. The optional rate allows playback at a specified relationship to real time i.e, a value of one replays the events queried back in normal time a value of 2 replays them back at double speen and a value of 0.5 replays them back at ½ rate (ie slower than normal time). The value can be anything above 0.|
TCPCommand select &lt;host&gt; &lt;port&gt; timings day dd/MM/yyy [layer regexp] [service regexp] [operation regexp] [user regexp] [rate value]|Retrieve timings from the TCPCollector that occurred on the specified date. The layer/service/operation allow addition filtering with an implied logical &amp; between all the filters|
|TCPCommand live &lt;host&gt; &lt;port&gt; on\|off|Turns on/off live receiving for this ASAP client (default on)|
|TCPCommand select &lt;host&gt; &lt;port&gt; stop|Stops retrieval of all queries made to the TCPCollector from this client|

**Administration Commands**

Admin users must be connected and authenticated to the collector to utilise these commands

|Command|Description|
|---|---|
|TCPCommand resetpasswd &lt;host&gt; &lt;port&gt; &lt;user&gt; &lt;admin password&gt; &lt;new user password&gt;|Allows an admin to reset the password for another user. The admin must provide their password to re-authenticate themselves|
|TCPCommand disableuser &lt;host&gt; &lt;port&gt; &lt;user&gt;|Deletes a user from the TCPCollector. Note that any existing session for this user is immediately disconnected.|
|TCPCommand adduser &lt;host&gt; &lt;port&gt; &lt;admin password&gt; &lt;user&gt; &lt;user password&gt;|Adds a new user to the collector with no admin rights. The admin users password must be resubmitted to authenticate the command|
|TCPCommand addadmin &lt;host&gt; &lt;port&gt; &lt;admin password&gt; &lt;user&gt;|Gives the specified user admin rights to the specified TCPCollector.|
|TCPCommand deladmin &lt;host&gt; &lt;port&gt; &lt;admin password&gt; &lt;user&gt;|Removes admin rights from the specified user. Note that you can remove your own admin rights in which case you will need another admin user to reapply them.|

### ASAP UI and the Collector

The user can do several important actions through the UI to use the collector.  This starts with connecting to a collector

{{< figure src="asapconnect.png" caption="Connecting ASAP to a Collector" >}}

ASAP can connect to many different collectors at the same time if required. Once ASAP is connected the **[TCP Collector | Manage Collector]** menu provides the capability for some simple management commands

{{< figure src="managecollector.png" caption="Managing Collectors from ASAP" >}}

Use the drop down to select the collector to manage and then chose `Close Connection` to disconnect or `Stop Query` to cancel any running queries with this collector.

To start a query select **[TCP Collector | Select Timings]**

{{< figure src="select.png" caption="Querying the Collector from ASAP" >}}

Select the time range, service, operation and layer to filter further as required.  If only a set of users are to be queried enter the user filter and then add the replay rate.  Leaving any field blank means not filtered. All filters are regular expressions so are very powerful.  Leaving the rate field empty allows the data to be replayed as fast as possible in time sequence. Using a rate of 1 will replay in real time while a replay rate less than one implies in slow motion and greater than one means fast foward.

When a rated query is requested the ASAP displays the percentage through the requested filtered data as it replays whereas an unrated query displays the number of records delivered and the total amount to be sent.  This is because they are sent in batches for unrated.

A big query across a large datasets can take some time to start delivering due to the performance of the underlying mongo database configuration, or, it may be because you have set a rated query which starts before any recorded data. This blank space is still part of the replay as it may be important to the analysis.

## Managing The Datastore

The TCPCollector has the ability to record all timing events received
into a `mongo` database. These events can then be replayed to clients on
request using the commands specified above. The mongo database version
that this collector has been tested against is 3.0.1 but unless there are future breaking changes this should be compatible with future versions as it uses basic features only.

To turn on storage to the mongo database add the following property to
the configuration file:

```
store=true
```

The mongo database is connected to on start up and if it isn't available
the connection will be retried in the background until successful. Note
that the TCPCollector does not expect to have to use a username/password
as part of the connection. No provision is made to set one unless it is
provided in the connection string.

The connection string can be specified in the configuration file,
however, if one is not specified a default is used:
`mongodb://localhost:27017/` Please see the mongodb documentation for the
format of the connection string.

The data is stored in a database called `ASAP` and all timing events are
recorded in a collection called `timings`. Each timing record is stored
in the following document format:

```
{“service”, “operation”, “layer”, “time”, “response”, “wait”, “filename”, “location”, “transaction”, “user”, “login”, “logout”}

```

This timing collection has a number of indexes created on it to improve
the speed of the selection queries with large record sets. The defined
queries that are created when the database and collection are first
created are:

  - an ascending index on “time”
  - an ascending index on “layer”
  - an ascending index on “service”
  - an ascending index on “operation”
  - an index on “user”

### Record Management

The TCPCollector will manage its data collection if required to. It will
purge records that have reached a defined age (from the “time” of the
event not the time of recording the event) and runs the purge process every configurable number of minutes.
This purge process is controlled through the TCPCollector configuration file using the following parameters:

- `purgePeriod=<period>` number of minutes to delay between running purge processes
- `purgeDelay=<delay>` the age of the records before they are considered for purging in hours

The default values are `purgePeriod=0` (equivalent to not purging) and `purgeDelay=168` (7 days)

### Bulk Loading Data

A separate program is available to load events in bulk directly into the
mongo database. This program is a command line executable and can be
provided with either a list of files to process or can read from
standard input (useful for piped commands).

**Usage**

The format of the command is:

```
java -jar DBLoader.jar [-d] <mongo connection string> <file> [<file> …]
```

or for reading from standard in replace the list of files with `--`

```
java -jar DBLoader.jar [-d] <mongo connection string> --
```

The optional argument `-d` (duplicate) causes the loader to query the
database for each record to be written and check it is not already
present. The check for duplicity is exact match against the layer, the
service name, the operation name, the transaction id, the response time
and the wait time. It does not look at the filename or server as loading
records may alter these in the stored, or to be store, records. Note
that this can become an expensive operation and should be used with
caution.

**Output**

When the program is run it will check the command parameters and print a
usage message if it is incorrect. When it starts to run it will list the
file it is processing and then a dot for each 1000 lines it processes
from the file. At the end of the file it will output
- the number of lines read
- the number of events this generated
- the number of events saved in the database
- the number of duplicate records it found

At the end of reading all the files
(when there is more than one) it will output a combined set of
statistics from all of the files.

The output will list all of the extensions read from the classpath that
are used to process the lines from the files. It is possible to add your
own extensions to support additional log file structures.  This mechanism is the same as that utilised throughout all of the ASAP tools.

## Security and Communication

The TCPCollector utilises several security measures to support a level
of protection of the user management when authorisation is required.

1.  Authentication can be enabled as a required option to connect
2.  Remote management can be enabled optionally if required

All user details are held in the configuration file and the TCPCollector
needs to be able to read (as a minimum) this file and write to the file
when remote administration is required.

All passwords in the file will be hashed using a salted PBKDF2
algorithm. Upon reading a plain text password this hashing is undertaken
and the hash held in memory only and the plain text form of the password
replaced in the configuration file. This hash algorithm is designed
specifically for password hashing and is necessarily slow to restrict
brute force dictionary attacks.

If the file is to be set up so that the TCPCollector has no write access
then the password needs to be added to the file in hash form. A separate
program is provided to generate a hash for a given password and is run
using the following syntax:

```
java -jar passwordHash <password>
```

Note that each time a hash is generated it will be different for the
same pass phrase

### Communication

Command communications between ASAP and the TCPCollector are transmitted
using public/private key encryption mechanism. Each client that connects
to the TCPCollector is provided with a separate public key and the
TCPCollector holds a different (& dynamically generated) private key per
connection. This implies that a message sent from one client to another
port (other than the one originally connected to) will not be able to
send commands as the private/public key combination will not match. The
private/public key uses the following cyphers/encoding
RSA/ECB/PKCS1PADDING using 512 byte RSA key, but there is no block
chaining even though it is specified in the encryption scheme.

Each command is split into separate pieces: a plain text piece and an
encrypted piece. Commands can carry a unique random number in both parts
and these must match to confirm the message has not been altered.
Additionally, this message identity is checked to determine that it has
not been used before and if it has it is rejected as a
duplicate/potential fraud.

The encrypted part of the message is sent as a base64 encoded string for
simplicity of handling the message.

Failures of command messages are logged as SEVERE and output
irrespective of the logging level.

#### Login Process with Authentication

1.  Client connects to the TCPCollector from an unknown host:port
2.  TCPCollector generates a new private key and public key per connection
3.  TCPCollector base64 encodes public key
4.  TCPCollector tells client to “start authenticate” and provides public key to use.
5.  Client extracts public key from message and sends username and encrypted password (using the furnished public key) to the TCPCollector.
6.  TCPCollector decrypts password and checks this against the hash held for the user.
7.  Failure causes an immediate disconnect of the port and therefore a new private/public key combination on next connection from the same
    public/private key combination.
8.  Success passed back to client.

From this point the performance data will be sent to the client
unencrypted. With keep alive messages sent in both directions to
maintain network connections through firewalls.

#### TCPCollector Network Commands

Note that the following details are what is sent across the connection between ASAP and the client.  These are not user commands that were detailed above.

---

**Closing a Connection**

To close a connection the ASAP client sends the string

```
quit
```

When a connection is lost due to a network disconnect the connection is
automatically closed.

---

**Keep Alive**

The client sends the following string as a keepalive message

```
Hello
```

This same keepalive message is sent in the other direction every 30
seconds of no traffic to ensure TCP connections remain open through
firewalls.

---

**Receiving User Details**

The TCPCollector receives a message with the following contents to
specify the user details on request:

```
user <username>,<encrypted string>
```

Where `<encrypted string>` is the base64 encoded encrypted password
using the connections public key.

---

**Changing a Password**

The following message is sent to change a user password by the same
user:

```
chpasswd <username>,<encrypted string>
```

Where `<encrypted string>` is the base64 encoded encrypted string of
`<original password string>::<new password string>`

---

**Resetting User Password as Admin**

The following message is sent to the TCPCollector to reset a user
passwords

```
resetpasswd <username> <encrypted string>
```

The encrypted string is of the form
`<message id>::<admin password>::<new password>`

---

**Disabling a User**

```
disableuser <user>,<unique id>,<encrypted string>
```

where `<encrypted string>` contents are `<unique id>`

---

**Adding a User**

```
adduser <message id>,<encrypted string>
```

where `<encrypted string>` contents are `<message id>::<admin user password>::<username>::<password for new username>`

---

**Adding and Deleting Admin Rights**

```
addadmin <message id>,<encrypted string>
deladmin <message id>,<encrypted string>
```

where `<encrypted string>` contents are `<message id>::<admin's password>::<username of user to change admin rights>`

---

**Changing the Live Broadcast Status**

```
live off
live on
```

Turns off or on the live broadcast to the issuing client. Note that
requests for historic records are still sent.

---

**Request Timing Events Between Specified Times**

```
select timings <start time> <end time> [<layer regexp> <service regexp> <operation regexp> <user regexp> [<rate>]]
```

The times are standard java system times in milliseconds from 1970. This
will query the database and retrieve all records that match and send
these through to the client in the standard communication form. The
records can be further filtered by having 4 regular expression strings
passed through which are applied to the query with and operators, ie a
timing event must be after the start time and before the end time and it
must be from the specified layer, service, operation and the event user
must match the regexp.

A request could generate many thousands of events to be sent to the
client. A count of the number of events is sent to the client as a
message and also a running count for each thousand that has been
processed. For rate restricted playbacks a percentage value is returned
to ASAP and not the count.

---

**Pausing a Query Event Streaming**

A steam of events being sent to the requesting system could easily
overload the receiving system. The client can therefore request that the
stream of events is paused or un-paused using the network message

```
select pause
select unpause
```
The ASAP client does this automatically when its internal queue builds
up to 100,000 messages and un-pauses the query when we the queue reduces passed a threshold below this level.

---

**Stopping a Query Event Stream**

The stream of events requested in the above command can be interrupted
at any time by receipt of the following command:

```
select stop
```

This will stop all running streams if more than one is currently being delivered on a connection.

---

**Event Streaming**

All events are sent to the connected client using a standard data object
definition. This object holds data used by ASAP for communication of all
of its components not just the collector and is therefore somewhat more
complex than that necessary for the TCPCollector. The object is defined
below. The `ServiceEntry` object is sent to the client via java
serialisation and communicated through TCP as a byte stream and reformed
at the client and then processed. This is not pparticularly transferable but results in the fastest deserialisation and minimal network traffic.

Note that I have trimmed the `ServiceEntry` class to only those parts that are relevant to the communications required for the TCPCollector Event Streaming.

```java
public class ServiceEntry implements Serializable
{

  private static final long serialVersionUID = 1302857275965139627L;

  // The service
  public Service service = null;

  // The date/time the event occurred
  public Date when;

  // The response time of the events and how long it waited for is children to respond
  public double response, wait = 0;

  // specifies the type of event (listed below)
  public int type = -1;

  // The transaction id for the timing event. Used to trace through calls through different layers and services
  public String txn = null;

  // The location the event occurred and usually specifies the machine name
  public String location=null;

  // The log file the event was generated from
  public String filename="";

  // whether this is considered as a login event (not login to the collector but the system generating the log file)

  public boolean loginEvent=false;

  // whether this is cnsidered a log out event
  public boolean logoutEvent=false;

  // a string to represent the specified user for any event
  public String user=null;

  public static final int SE_TYPE_EVENT = 1;

}
```

Any events that arrive at the TCPCollector that are command events are
logged to info logging and dropped. This stops commands from being sent
to all ASAP clients from a malicious actor by crafting a file with commands in it.



## High Level Design of the Collector

The following simplified sequence diagrams show the main functions
of the TCPcollector at the component level and not the internals of the
TCPCollector.

This diagram shows the process of starting the TCPCollector and an ASAP client connecting to it.

{{< mermaid >}}
sequenceDiagram
  participant A as ASAP Client
  participant C as TCPCollector
  participant T as TCPDirAgent
  participant D as MongoDB
  activate C
  C->>+D: Connect
  opt No Database Exists
    C->>D: Create Collections
  end
  deactivate D
  loop for each TCPDirAgent

    C-xT: Connect

    Note right of T: This is retried<br />whenever the<br />connection is not<br />available
  end
  deactivate C
  activate A
  activate C
  A->>+C: connect
  opt login required
    C-->>A: demand login
    A->>+C: send login details
    alt success
      C->>C: register connection
      C-->>A: send success
    end
    alt failure
      C->>-A: disconnect
    end
  end
  deactivate C
  deactivate A
{{< /mermaid >}}

The next diagram shows the interactions when an event is available
{{< mermaid >}}
sequenceDiagram
  participant A as ASAP Client
  participant C as TCPCollector
  participant T as TCPDirAgent
  participant D as MongoDB
  T-x+C: send event
  activate C
  C->>+D: store
  D-->>-C: result
  loop connections
    C-xA: send event
  end
  deactivate C
{{< /mermaid >}}

The next sequence diagram shows the interactions when a series of events is requested from the TCPCollector


{{< mermaid >}}
sequenceDiagram
  participant A as ASAP Client
  participant C as TCPCollector
  participant D as MongoDB

  A--xC: Request Records
  activate C
  C->>D: Get Count
  C-xA: Send Count
  loop records
    C->>D: Get Record
    C->C: Convert record to event
    Note right of C: records may be<br />bundled into a<br />group of events
    C-xA: Send event
    opt 1000 records
    C-xA: send progress
    end
  end
  deactivate C
{{< /mermaid >}}


### Functional Design

The TCPCollector has a number of major internal components that operate
independently. These are:

| Component        | Description           |
| -----------------|---------------------- |
| SocketListener   | A thread that listens on the specified port for connections from clients. Note that it opens a port on all available network addresses|
| TCPSender         | A thread per client that is used to send data from the TCPcollector to a client. Each sender has a queue of data that needs sending so a slow response from one individual client will not cause a bottleneck in the TCPcollector|
| ListenerTCP       | A thread that listens to a port for data from a TCPAgent that the TCPcollector has connected to. All received data is added to a central queue for deliver to clients asynchronously |
| Poller            | A thread that pulls a message from the central queue and sends the event to each TCPSender and the DBWriter |
| DBWriter          | A singleton object with an internal queue for data to be written to the database a self managing connection pool to the database and each connection running as a separate thread. Note that this queue can hold up to 1,000,000 records to handle a database slowdown if necessary before database writing is paused for a period of time. |
| UserConnection    | An object that manages a user connection and encryption for each client  |
| ConnectionMonitor | A thread that runs and recognises when a TCPAgent connection has died and reconnects when it becomes available |
| MongoConnect      | A thread that runs and connects to the Mongo database and retries until a connection is established. On connection it checks that the ASAP database and timings collection are created and if not creates them |
| DisconnectMongo   | A threaded object that is registered as a shutdown hook, within the JVM, to close the connections cleanly on exit of the TCPCollector. Note that it will not clear ensure that the DBWrite queue is clear before closing the connections. |
| PurgeTimer        | A timer thread that runs and requests a purge to be run |
| MongoPurgeThread  | A thread that is responsible for the complete purge  |
| MongoWriteThread  | A thread that is a connection to the mongodb and selects records from the db queue and writes them to the database. The thread will only live for a number of writes prior to closing.    |

The class diagram below shows how each object is related to the others.
The centre portion is related to managing the data inside mongo. The righthand side of the diagram shows the classes that are involved in a user connection.  The lefthand side is related to recieving data from the Agents.

{{< figure src="./class.svg" caption="TCPCollector Class Diagram (plantUML)" >}}

{{% notice info "fa-comment-alt" %}}
Please see the [PlantUML](http://plantuml.com/) site for details of the tool that generated the above diagram.   The source of the diagram is available [here](./class.puml) and is generated using `java -jar plantuml.jar class.puml`

{{% /notice %}}
