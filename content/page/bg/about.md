---
title: Blood Glucose Monitor
subtitle: Java Application to Provide analysis of Blood Glucose Readings
author: Andrew Colvin
date: 2019-06-12 11:53:00.831395616 +0100 BST m=+0.038027864
toc: false
#weight: 120
comments: false
tags: ["Blood Glucose", "maven", "java"]
bigimg: [{src: "img/bg-img.png"}]
layout: article
previous: ""
index: ""
next: ""
---

This is the project page for my blood glucose monitoring application.  It provides the following capability:

- Adding, loading, saving, exporting and importing your BG readings, 
- Estimation of HbA1c values, 
- Providing Insulin On Board calculations 
- Providing a real time estimate of IOB. 
- Graphs containing different types of reading such as comparing weight reduction against weekly BG reading
- Tracking of Weight
- Tracking of Blood Pressure

<!--more-->

# Blood Glucose Monitor Application

To read the detail and history of the changes to the application you can read the [diabetes.co.uk forum topic](https://www.diabetes.co.uk/forum/threads/writing-bg-management-app.56644/).

The code repository is available [here](https://gitlab.acolvin.me.uk/andrew/blood-glucose); you will need my CA certificate which is available under the [resources section]({{< ref "resource.md" >}})

There is a very quick and rough video (no sound) to illustrate how to use the BG application.  This is based on a rather old version but gives a view of the basics:

{{< youtube "oO4KhOxXiAs" >}} 

## Downloading the Application

The application is built as a fully self contained executable jar and is available [here](https://drive.google.com/open?id=0B-z_UPs-Jl8abTBvSVM5MnluV1k) along with some of my personal data in the folder for you to play with.

## Manual

Presently there is a partial manual available in [ODT format](https://git.acolvin.me.uk/andrew/blood-glucose/blob/master/manual.odt) in the repository. This is not complete but is useful to get you going alongside the video. The manual will be added as a document to this site in due course.


## Building the Application

To build the application you will need maven and java 6 to 8 installed (may work on newer version but this is not tested). clone the repository to your local machine and execute `mvn package`. 

As an alternative the whole project can be built using  the command `mvn scm:bootstrap` with just the [pom.xml](https://git.acolvin.me.uk/andrew/blood-glucose/raw/master/pom.xml) in the directory. 

Here is an example from an empty directory

```bash

andrew@host:~> mkdir bg-test
andrew@host:~> cd bg-test
andrew@host:~/bg-test> wget https://git.acolvin.me.uk/andrew/blood-glucose/raw/master/pom.xml
andrew@host:~/bg-test> ls
pom.xml
andrew@host:~/bg-test> mvn scm:bootstrap
[INFO] Scanning for projects...
[WARNING] 
[WARNING] Some problems were encountered while building the effective model for uk.me.acolvin:blood.glucose:jar:0.5.7
[WARNING] 'build.plugins.plugin.version' for org.apache.maven.plugins:maven-surefire-plugin is missing. @ line 99, column 13
[WARNING] 
[WARNING] It is highly recommended to fix these problems because they threaten the stability of your build.
[WARNING] 
[WARNING] For this reason, future Maven versions might no longer support building such malformed projects.
[WARNING] 
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/plugins/maven-archetype-plugin/2.2/maven-archetype-plugin-2.2.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/plugins/maven-archetype-plugin/2.2/maven-archetype-plugin-2.2.pom (7.3 kB at 14 kB/s)

-----   snipped out lots of downloads that just happen for brevity ------

Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/wagon/wagon-provider-api/1.0-alpha-6/wagon-provider-api-1.0-alpha-6.jar (43 kB at 32 kB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/com/mks/api/mksapi-jar/4.10.9049/mksapi-jar-4.10.9049.jar (645 kB at 401 kB/s)
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/groovy/groovy-all/1.7.6/groovy-all-1.7.6.jar (5.7 MB at 1.8 MB/s)
[INFO] Removing /home/andrew/bg-test/target/checkout
[INFO] Executing: /bin/sh -c cd /home/andrew/bg-test/target && git clone https://gitlab.acolvin.me.uk/andrew/blood-glucose.git /home/andrew/bg-test/target/checkout
[INFO] Working directory: /home/andrew/bg-test/target
[INFO] Executing: /bin/sh -c cd /tmp && git ls-remote https://gitlab.acolvin.me.uk/andrew/blood-glucose.git
[INFO] Working directory: /tmp
[INFO] Executing: /bin/sh -c cd /home/andrew/bg-test/target/checkout && git pull https://gitlab.acolvin.me.uk/andrew/blood-glucose.git master
[INFO] Working directory: /home/andrew/bg-test/target/checkout
[INFO] Executing: /bin/sh -c cd /home/andrew/bg-test/target/checkout && git checkout
[INFO] Working directory: /home/andrew/bg-test/target/checkout
[INFO] Executing: /bin/sh -c cd /home/andrew/bg-test/target/checkout && git ls-files
[INFO] Working directory: /home/andrew/bg-test/target/checkout
[INFO] Scanning for projects...
[WARNING] 
[WARNING] Some problems were encountered while building the effective model for uk.me.acolvin:blood.glucose:jar:0.5.7
[WARNING] 'build.plugins.plugin.version' for org.apache.maven.plugins:maven-surefire-plugin is missing. @ line 99, column 13
[WARNING] 
[WARNING] It is highly recommended to fix these problems because they threaten the stability of your build.
[WARNING] 
[WARNING] For this reason, future Maven versions might no longer support building such malformed projects.
[WARNING] 
[INFO] 
[INFO] --------------------< uk.me.acolvin:blood.glucose >---------------------
[INFO] Building blood.glucose 0.5.7
[INFO] --------------------------------[ jar ]---------------------------------
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/plugins/maven-install-plugin/2.4/maven-install-plugin-2.4.pom
Progress (1): 2.2/6.4 kB
Progress (1): 5.0/6.4 kB
Progress (1): 6.4 kB    
                    
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/plugins/maven-install-plugin/2.4/maven-install-plugin-2.4.pom (6.4 kB at 15 kB/s)
[INFO] 
[INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ blood.glucose ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources
[INFO] 
[INFO] --- maven-compiler-plugin:2.3.2:compile (default-compile) @ blood.glucose ---
[INFO] Compiling 82 source files to /home/andrew/bg-test/target/checkout/target/classes
[INFO] 
[INFO] --- maven-resources-plugin:2.6:testResources (default-testResources) @ blood.glucose ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /home/andrew/bg-test/target/checkout/src/test/resources
[INFO] 
[INFO] --- maven-compiler-plugin:2.3.2:testCompile (default-testCompile) @ blood.glucose ---
[INFO] Compiling 7 source files to /home/andrew/bg-test/target/checkout/target/test-classes
[INFO] 
[INFO] --- maven-surefire-plugin:2.12.4:test (default-test) @ blood.glucose ---
[INFO] Surefire report directory: /home/andrew/bg-test/target/checkout/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
Running uk.me.acolvin.blood.glucose.model.BloodGlucoseSetTest
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.016 sec
Running uk.me.acolvin.blood.glucose.model.IOBProfileTest
Tests run: 3, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.001 sec
Running uk.me.acolvin.blood.glucose.model.InsulinSetTest
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0 sec
Running uk.me.acolvin.blood.glucose.AppTest
Tests run: 1, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0 sec
Running uk.me.acolvin.blood.glucose.BGReadingTest
Tests run: 17, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.004 sec
Running uk.me.acolvin.blood.glucose.loader.CSVImporterTest
Tests run: 1, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.001 sec

Results :

Tests run: 26, Failures: 0, Errors: 0, Skipped: 0

[INFO] 
[INFO] --- maven-jar-plugin:2.4:jar (default-jar) @ blood.glucose ---
[INFO] Building jar: /home/andrew/bg-test/target/checkout/target/blood.glucose-0.5.7.jar
[INFO] 
[INFO] --- maven-shade-plugin:2.2:shade (default) @ blood.glucose ---
[INFO] Including org.apache.commons:commons-math3:jar:3.2 in the shaded jar.
[INFO] Including commons-cli:commons-cli:jar:20040117.000000 in the shaded jar.
[INFO] Including org.jfree:jfreechart:jar:1.0.17 in the shaded jar.
[INFO] Including org.jfree:jcommon:jar:1.0.21 in the shaded jar.
[INFO] Including xml-apis:xml-apis:jar:1.3.04 in the shaded jar.
[INFO] Including org.swinglabs:swingx:jar:1.6.1 in the shaded jar.
[INFO] Including com.jhlabs:filters:jar:2.0.235 in the shaded jar.
[INFO] Including org.swinglabs:swing-worker:jar:1.1 in the shaded jar.
[INFO] Including org.apache.derby:derbyclient:jar:10.10.2.0 in the shaded jar.
[INFO] Including org.apache.derby:derbynet:jar:10.10.2.0 in the shaded jar.
[INFO] Including org.apache.derby:derby:jar:10.10.2.0 in the shaded jar.
[WARNING] derbyclient-10.10.2.0.jar, derby-10.10.2.0.jar define 15 overlappping classes: 
[WARNING]   - org.apache.derby.iapi.services.info.ProductGenusNames
[WARNING]   - org.apache.derby.shared.common.error.ExceptionSeverity
[WARNING]   - org.apache.derby.iapi.reference.Attribute
[WARNING]   - org.apache.derby.iapi.services.info.JVMInfo
[WARNING]   - org.apache.derby.iapi.services.info.JVMInfo$1
[WARNING]   - org.apache.derby.shared.common.reference.JDBC30Translation
[WARNING]   - org.apache.derby.iapi.tools.i18n.LocalizedResource
[WARNING]   - org.apache.derby.shared.common.reference.SQLState
[WARNING]   - org.apache.derby.iapi.tools.i18n.LocalizedInput
[WARNING]   - org.apache.derby.iapi.services.info.ProductVersionHolder
[WARNING]   - 5 more...
[WARNING] derbynet-10.10.2.0.jar, derbyclient-10.10.2.0.jar, derby-10.10.2.0.jar define 13 overlappping classes: 
[WARNING]   - org.apache.derby.impl.tools.sysinfo.Main$8
[WARNING]   - org.apache.derby.impl.tools.sysinfo.Main
[WARNING]   - org.apache.derby.impl.tools.sysinfo.Main$2
[WARNING]   - org.apache.derby.impl.tools.sysinfo.Main$9
[WARNING]   - org.apache.derby.impl.tools.sysinfo.Main$6
[WARNING]   - org.apache.derby.impl.tools.sysinfo.Main$1
[WARNING]   - org.apache.derby.impl.tools.sysinfo.Main$5
[WARNING]   - org.apache.derby.impl.tools.sysinfo.ZipInfoProperties
[WARNING]   - org.apache.derby.impl.tools.sysinfo.Main$4
[WARNING]   - org.apache.derby.tools.sysinfo
[WARNING]   - 3 more...
[WARNING] maven-shade-plugin has detected that some .class files
[WARNING] are present in two or more JARs. When this happens, only
[WARNING] one single version of the class is copied in the uberjar.
[WARNING] Usually this is not harmful and you can skeep these
[WARNING] warnings, otherwise try to manually exclude artifacts
[WARNING] based on mvn dependency:tree -Ddetail=true and the above
[WARNING] output
[WARNING] See http://docs.codehaus.org/display/MAVENUSER/Shade+Plugin
[INFO] Replacing original artifact with shaded artifact.
[INFO] Replacing /home/andrew/bg-test/target/checkout/target/blood.glucose-0.5.7.jar with /home/andrew/bg-test/target/checkout/target/blood.glucose-0.5.7-shaded.jar
[INFO] Dependency-reduced POM written at: /home/andrew/bg-test/target/checkout/dependency-reduced-pom.xml
[INFO] 
[INFO] --- maven-install-plugin:2.4:install (default-install) @ blood.glucose ---
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-utils/3.0.5/plexus-utils-3.0.5.pom
Progress (1): 2.2/2.5 kB
Progress (1): 2.5 kB    
                    
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-utils/3.0.5/plexus-utils-3.0.5.pom (2.5 kB at 61 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus/3.1/plexus-3.1.pom
Progress (1): 2.2/19 kB
Progress (1): 5.0/19 kB
Progress (1): 7.8/19 kB
Progress (1): 11/19 kB 
Progress (1): 13/19 kB
Progress (1): 16/19 kB
Progress (1): 19 kB   
                   
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus/3.1/plexus-3.1.pom (19 kB at 332 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-digest/1.0/plexus-digest-1.0.pom
Progress (1): 1.1 kB
                    
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-digest/1.0/plexus-digest-1.0.pom (1.1 kB at 28 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-components/1.1.7/plexus-components-1.1.7.pom
Progress (1): 2.2/5.0 kB
Progress (1): 5.0 kB    
                    
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-components/1.1.7/plexus-components-1.1.7.pom (5.0 kB at 121 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus/1.0.8/plexus-1.0.8.pom
Progress (1): 2.2/7.2 kB
Progress (1): 5.0/7.2 kB
Progress (1): 7.2 kB    
                    
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus/1.0.8/plexus-1.0.8.pom (7.2 kB at 185 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-container-default/1.0-alpha-8/plexus-container-default-1.0-alpha-8.pom
Progress (1): 2.8/7.3 kB
Progress (1): 5.5/7.3 kB
Progress (1): 7.3 kB    
                    
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-container-default/1.0-alpha-8/plexus-container-default-1.0-alpha-8.pom (7.3 kB at 182 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-utils/3.0.5/plexus-utils-3.0.5.jar
Downloading from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-digest/1.0/plexus-digest-1.0.jar
Progress (1): 2.2/230 kB
Progress (1): 5.0/230 kB
Progress (1): 7.7/230 kB

... snipped again ...

Progress (2): 188/230 kB | 12 kB
Progress (2): 192/230 kB | 12 kB
Progress (2): 196/230 kB | 12 kB
Progress (2): 200/230 kB | 12 kB
                                
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-digest/1.0/plexus-digest-1.0.jar (12 kB at 119 kB/s)
Progress (1): 204/230 kB
Progress (1): 208/230 kB
Progress (1): 212/230 kB
Progress (1): 217/230 kB
Progress (1): 221/230 kB
Progress (1): 225/230 kB
Progress (1): 229/230 kB
Progress (1): 230 kB    
                    
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-utils/3.0.5/plexus-utils-3.0.5.jar (230 kB at 1.9 MB/s)
[INFO] Installing /home/andrew/bg-test/target/checkout/target/blood.glucose-0.5.7.jar to /home/andrew/.m2/repository/uk/me/acolvin/blood.glucose/0.5.7/blood.glucose-0.5.7.jar
[INFO] Installing /home/andrew/bg-test/target/checkout/dependency-reduced-pom.xml to /home/andrew/.m2/repository/uk/me/acolvin/blood.glucose/0.5.7/blood.glucose-0.5.7.pom
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 8.251 s
[INFO] Finished at: 2019-06-12T15:13:54+01:00
[INFO] ------------------------------------------------------------------------
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 19.092 s
[INFO] Finished at: 2019-06-12T15:13:54+01:00
[INFO] ------------------------------------------------------------------------

```

The executable jar can be seen below

```bash
andrew@host:~/bg-test> ls -l target/checkout/target/
total 9716
-rw-r--r-- 1 andrew users 9584250 Jun 12 15:13 blood.glucose-0.5.7.jar
drwxr-xr-x 1 andrew users      40 Jun 12 15:13 classes
drwxr-xr-x 1 andrew users      54 Jun 12 15:13 generated-sources
drwxr-xr-x 1 andrew users      28 Jun 12 15:13 maven-archiver
-rw-r--r-- 1 andrew users  362103 Jun 12 15:13 original-blood.glucose-0.5.7.jar
drwxr-xr-x 1 andrew users    1256 Jun 12 15:13 surefire-reports
drwxr-xr-x 1 andrew users       4 Jun 12 15:13 test-classes

```

The `original-blood.glucose-0.5.7.jar` is the version prior to all of the libraries being packaged into a single jar file.

## Copyright 

Copyright Andrew Colvin 2014 - 2019\
This software is licensed under the GNU General Public License Version 3.
Please feel free to use for your own purposes.

## Libraries

The following libraries are used and licensed respectively:

__Apache License 2.0__

- Apache Commons Math Library
  - [https://commons.apache.org/proper/commons-math/](https://commons.apache.org/proper/commons-math/)
  - Used for statistical calculations
- Apache Commons CLI
  - [https://commons.apache.org/proper/commons-cli/](https://commons.apache.org/proper/commons-cli/)
  - Used to process command line arguments
- Apache Derby
  - [https://db.apache.org/derby/](https://db.apache.org/derby/)
  - The database backend

__GNU Lesser General Public License (LGPL)__

- JFree Chart
  - [http://www.jfree.org/jfreechart/](http://www.jfree.org/jfreechart/)
  - Provides the graph functionality
- SwingX
  - [https://mvnrepository.com/artifact/org.swinglabs/swingx/1.6.1](https://mvnrepository.com/artifact/org.swinglabs/swingx/1.6.1)
  - Adds helpers for managing the GUI

