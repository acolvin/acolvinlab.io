+++
title = "revealjs"
slug = "revealjs"
layout="slide"
description = "present content as a reveal.js slide"
theme="moon"
[RevealOptions]
controls= true
transition="zoom"
center=true
+++

This shortcode will format the enclosed markdow to render it with [reveal.js](http://lab.hakim.se/reveal-js/) at runtime (client-side)

Read more on [revealjs github repo](https://github.com/hakimel/reveal.js/#markdown).

---

## Embedded Slide

`revealjs shortcode` can use the following named parameters :

* theme      <html><!-- .element: class="fragment" --></html>
* transition <html><!-- .element: class="fragment" --></html>
* controls   <html><!-- .element: class="fragment" --></html>
* progress   <html><!-- .element: class="fragment" --></html>
* history    <html><!-- .element: class="fragment" --></html>
* center     <html><!-- .element: class="fragment" --></html>

*Even if the enclosed content is a mardown, use `<` shortcode notation instead of the `%` notation*<html><!-- .element: class="fragment" --></html>

___

## Slide Page

Use slide layout in frontmatter. Presentation retains the Top Nav bar of site

Use RevealOptions in front matter
___

## Fragments

Define fragments in a slide using   
`* theme      <html><!-- .element: class="fragment" --></html>`
___

### Content formating and slide delimiters

[read more on this here]( "page-slide.md")

---

## Demo




# In the morning

___


## Getting up

- Turn off alarm
- Get out of bed

___

## Breakfast

- Eat eggs
- Drink coffee

---

# In the evening

___

## Dinner

- Eat spaghetti
- Drink wine

___

## Going to sleep

- Get in bed
- Count sheep

---

## Source :



```
{{</*revealjs theme="moon" progress="true"*/>}}

# In the morning

 ___


## Getting up

- Turn off alarm
- Get out of bed

 ___

## Breakfast

- Eat eggs
- Drink coffee

 ---

# In the evening

 ___

## Dinner

- Eat spaghetti
- Drink wine

 ___

## Going to sleep

- Get in bed
- Count sheep

{{</*revealjs*/>}}

```

___

* [click here to view raw content](https://raw.githubusercontent.com/vjeantet/hugo-theme-docdock/master/exampleSite/content/shortcodes/revealjs.md)
